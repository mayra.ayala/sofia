//***************************************************** Setup de eventos a escuchar
require('events').EventEmitter.defaultMaxListeners = 20
//***************************************************** HTTPS server setup
//-----* Express inicia servidor / carpeta raiz
//------------------------------------Express inicia servidor 
const express = require('express')
const app = express()
const fs = require('fs')
let clientesConectados = [];
let isConnected = true
let disconnected = null
//const ImageDataURI = require('image-data-uri')
app.use(express.static(__dirname))//Carpeta de donde sirve / carpeta raiz public

const server = app.listen(8888, () => {
    console.log('Servidor web iniciado')
})

//-----* Filesystem module object
var fss = require('fs')
//-----* https module object
var https = require('https')

//***************************************************** Seccion Socket 
var io = require('socket.io')(server)

io.on('connection', (socket) => {

    socket.on('ejemplo', async function () { await socketfunction() })

})//close io.on

//***************************************************** Postgres module object
const { Pool } = require('pg')
postgresdb = new Pool({
    host: 'localhost',
    user: 'ialab',
    password: 'ialab',
    database: 'sofia',
})
let client


// release the client


//***************************************************** Files Handler
io.on('connection', (socket) => {
    socket.on('deletefile', function (path) {
        deletef(path);
    }); //Close socket
    socket.on('datargb', async function (tax,valorminRFinal,valorMaximoRFinal,valorMinGFinal,valorMaximoGFinal,valorMinBFinal,valorMaximoBFinal) {
        await datargb(tax,valorminRFinal,valorMaximoRFinal,valorMinGFinal,valorMaximoGFinal,valorMinBFinal,valorMaximoBFinal);
    });
    socket.on('picsaving', async function (datauri, serial, sqty) {
        await savingpic(datauri, serial, sqty);
       // console.log("recibe", serial, sqty);
    });

    socket.on('pictasaving', async function (datauri, tea, snfile,identifica) {
        await savingpicTa(datauri, tea, snfile,identifica);
       // console.log("recibe", tea, snfile);
    });

    socket.on('logsaving', async function (sn, logsaving,logsave) { // Funcion de ejemplo borrar no importante
        savinglog(sn, logsaving,logsave)
      
    });
    socket.on('renombrasnr', function (snr) { // conexion con main_script
        renombraF(snr);
    });
    //Socket BD insert data
    socket.on('socketconection', async function (turno, status,day,fecha,semana,serial) { //datos del front
        await partn(turno, status,day,fecha,semana,serial)
        //console.log("entre al socketconection")
    }); //Close socket
    //Socket BD insert data each TA 
    socket.on('ubicaciones', async function(ta1,ta2,ta11,ta12,tb1,ta3,ta4,ta9,ta10,tb2,ta5,ta6,ta7,ta8,tb3,ta17,ta18,ta15,ta16,ta19,ta20,tb4,ta13,ta14,ta21,ta22,seri,fecha,day,semana){
        //console.log(ta1)
        await tadata(ta1,ta2,ta11,ta12,tb1,ta3,ta4,ta9,ta10,tb2,ta5,ta6,ta7,ta8,tb3,ta17,ta18,ta15,ta16,ta19,ta20,tb4,ta13,ta14,ta21,ta22,seri,fecha,day,semana)
    })
    //agrupapass t2 se agrego
    socket.on('agrupapasst2', async function (datospf) {
        agrupapasst2(datospf.turno, datospf.status, datospf.day,datospf.fecha,datospf.semana)
        //console.log("entre a agrupapass t2")
    });

    socket.on('agrupardias', async function (datospfd) {
        agrupardias(datospfd.status, datospfd.day,datospfd.semana)
       // console.log("entre a agrupar dias")
    });
    socket.on('countas', async function (ta1,ta2,ta3,ta4,ta5,ta6,ta7,ta8,ta9,ta10,ta11,ta12,ta13,ta14,ta15,ta16,ta17,ta18,ta19,ta20,ta21,ta22,tb1,tb2,tb3,tb4,day,semana) {
        countas(ta1,ta2,ta3,ta4,ta5,ta6,ta7,ta8,ta9,ta10,ta11,ta12,ta13,ta14,ta15,ta16,ta17,ta18,ta19,ta20,ta21,ta22,tb1,tb2,tb3,tb4,day,semana)
        //console.log("estoy en la funcion countas del backend")
    });

    //agrupa fail se agrego
    socket.on('agrupafailt1', async function () {
        await agrupafailt1()
       // console.log("entre a agrupafail t1")
    });
    //agrupa fail se agrego
    socket.on('agrupafailt2', async function () {
        await agrupafailt2()
        //console.log("entre a agrupafail t2")
    });

    socket.on('abrir', async function () {
        await abrir()
        //console.log("conectado")
    });

    socket.on('cerrar', async function () {
        await cerrar()
        //console.log("desconectado")
    })

    socket.on('exp', async function () {
        await experiment()
       // console.log("Experiment ...")
    })
})

async function deletef(path) {
    await deletefile(path);
}

async function deletefile(path) {

    return new Promise(async resolve => {

        netpath = path;
        fs.unlinkSync(netpath)
        console.log("Archivo borrado: " + netpath);

    });//Cierra Promise
}

//-----* Guarda imagen desde URI
async function savingpic(datauri, serial, sqty) {

    let filePath;
    const ImageDataURI = require('image-data-uri');
    return new Promise(async resolve => {
        //console.log("Variables:"+serial+' - '+sqty+'');// temporal para ver que esta rebiendo 

        //C:/Users/mayra_ayala/Documents/Aquiles/img/
        //C:/Users/gdl3_mds/myapp/timsamples/
        let filePath = 'C:/Users/gdl3_mds/myapp/projects/timsamples/' + serial + '';//Ruta de las carpetas por serial
        let filevalidation = fs.existsSync(filePath);

        if (filevalidation) {

            filePath = '' + filePath + '/' + sqty + '';
            ImageDataURI.outputFile(datauri, filePath).then(res => console.log(res));
        }
        else {
            fs.mkdir(filePath, (error) => {
                if (error) {
                    console.log(error.message);//en caso de que el folder ya exista manda un error y evita hacer otro folder con el mismo nombre.
                }
                filePath = '' + filePath + '/' + sqty + '';
                ImageDataURI.outputFile(datauri, filePath).then(res => console.log(res));
                console.log("Directorio creado");
            });
        }
    });//Cierra Promise
}

async function savingpicTa(datauri, tea, snfile, identifica){
    let filePath;

    //if(identifica == "good"){

    const ImageDataURI = require('image-data-uri');
    return new Promise(async resolve => {
        //console.log("Variables:"+serial+' - '+sqty+'');// temporal para ver que esta rebiendo 

        //C:/Users/mayra_ayala/Documents/Aquiles/img/
        //C:/Users/gdl3_mds/myapp/timsamples/
       
        let filePath = 'C:/Users/gdl3_mds/myapp/projects/recortesTAs/' + tea +'/'+ identifica +'' ;//Ruta de las carpetas por serial
        let filevalidation = fs.existsSync(filePath);
     
        if (filevalidation) {

            filePath = '' + filePath + '/' + snfile + '';
            ImageDataURI.outputFile(datauri, filePath).then(res => console.log(res));
        }
        else {
            fs.mkdir(filePath, (error) => {
                if (error) {
                    console.log(error.message);//en caso de que el folder ya exista manda un error y evita hacer otro folder con el mismo nombre.
                }
                filePath = '' + filePath + '/' + snfile + '';
                ImageDataURI.outputFile(datauri, filePath).then(res => console.log(res));
                console.log("Directorio creado");
            });
        }
    });//Cierra Promise
    //}
}



//Funcion para renombrar carpeta F 
async function renombraF(serial) {
    fs.rename('C:/Users/gdl3_mds/myapp/projects/timsamples/' + serial,
        'C:/Users/gdl3_mds/myapp/projects/timsamples/' + serial + '_F',
        function (err) {
            if (err)
                console.log('Error de renombramiento');
        });
}

function savinglog(sn, logdata,logsave ) {
   // console.log("entre a savinglog",logsave)

    let cadena = logsave.toString()
    //console.log(typeof(cadena))
    let datos = logdata +"\n Cadena a PLC \n"+ cadena 
    //console.log(datos)
    let logpath = 'C:/Users/gdl3_mds/myapp/projects/timsamples/' + sn + '/log.txt';
    fs.writeFile(logpath, datos, function (err) {
        if (err) throw err;
    });
}

//-----------------------------QUERYS BD------------------------//
//agregar t1
async function partn(turno, status, day,fecha,semana,serial) {

    //console.log("entre a insertar")
    let pg = "INSERT INTO unidades VALUES (" + turno + ",'" + status + "','" + day + "','" + fecha + "'," + semana + ",'" + serial + "' )"
   // console.log(pg)
    client.query(pg, (err, result) => {
        if (err) {
            return console.error('Error executing query', err.stack)}
       // console.log(result.rows)
    })
    //})
}
// Insert TA data BD
async function  tadata(ta1,ta2,ta11,ta12,tb1,ta3,ta4,ta9,ta10,tb2,ta5,ta6,ta7,ta8,tb3,ta17,ta18,ta15,ta16,ta19,ta20,tb4,ta13,ta14,ta21,ta22,seri,fecha,day,semana){
    let data= "INSERT INTO ubicaciones VALUES ('" + ta1 + "','" + ta2 +"','" + ta11 +"','"+ ta12 +"','"+ tb1 +"','"+ ta3 +"','"+ ta4 +"','"+ ta9 +"','"+ ta10 +"','"+ tb2 +"','"+ ta5 +"','"+ ta6 +"','"+ ta7 +"','"+ ta8 +"','"+ tb3 +"','"+ ta17 +"','"+ ta18 +"','"+ ta15 +"','"+ ta16 +"','"+ ta19 +"','"+ ta20 +"','"+ tb4 +"','"+ ta13 +"','"+ ta14 +"','"+ ta21 +"','"+ ta22 +"','"+ seri +"','"+ fecha +"','"+ day+"','"+ semana +"')"
    //console.log(data)
    client.query(data, (err, result) => {
        if (err) {
            return console.error('Error executing query', err.stack)}
       // console.log(result.rows)
    })
}

async function datargb(tax,valorminRFinal,valorMaximoRFinal,valorMinGFinal,valorMaximoGFinal,valorMinBFinal,valorMaximoBFinal){
    let datargb= "INSERT INTO rgbdata VALUES ('" + tax + "','" + valorminRFinal + "','" + valorMaximoRFinal + "','" + valorMinGFinal + "','" + valorMaximoGFinal + "','" + valorMinBFinal + "' ,'" +valorMaximoBFinal +"' )"
    client.query(datargb, (err, result) => {
        if (err) {
            return console.error('Error executing query', err.stack)}
       // console.log(result.rows)
    })
   await extractdatargb(tax)
}

async function extractdatargb(tax){
    let extracdata = " SELECT * FROM rgbdata WHERE ta = ('"+tax+"')"
    client
        .query(extracdata)
        .then((result) => { io.emit('rgbdataselect', {result}); }) 
        .catch((err) => console.error('Error executing query', err.stack))
}

async function agrupapasst2(turno, status, day,fecha) {
    //console.log("bd connected")
    let up = "SELECT COUNT (*) FROM unidades WHERE turno = (" + turno + ") AND status = ('" + status + "') AND day = ('" + day + "') AND fecha = ('" + fecha + "') ";
    client
        .query(up)
        .then((result) => { io.emit('qtyP2', { result, turno, status, day,fecha }); }) 
        .catch((err) => console.error('Error executing query', err.stack))

}

async function agrupardias(status, day,semana) {
    //console.log("bd connected")
    let upf = "SELECT COUNT (*) FROM unidades WHERE status = ('" + status + "') AND day = ('" + day + "') AND semana = ('" + semana + "') "; 
    client
        .query(upf)
        .then((result) => { io.emit('qtyD', { result, status, day,semana}); }) 
        .catch((err) => console.error('Error executing query', err.stack))
}
async function countas(ta1,ta2,ta3,ta4,ta5,ta6,ta7,ta8,ta9,ta10,ta11,ta12,ta13,ta14,ta15,ta16,ta17,ta18,ta19,ta20,ta21,ta22,tb1,tb2,tb3,tb4,day,semana){ //"SELECT COUNT (*) FROM ubicaciones WHERE ta1 = ('" + ta1 + "') AND day = ('" + day + "') AND semana = ('" + semana + "')"//OR ta2 = ('"+ ta2 +"')"; //AND day = ('" + day + "')  AND semana =("+semana+") 
    //console.log("entre a countas")
    let alltas = "SELECT COUNT (*) AS conteototal, SUM(CASE WHEN ta1 = ('"+ta1+"') AND semana = ('"+semana+"') THEN 1 ELSE 0 END) AS Conteota1, SUM(CASE WHEN ta2=('"+ ta2 +"') AND semana = ('"+ semana+"') THEN 1 ELSE 0 END) AS Conteota2,SUM(CASE WHEN ta3=('"+ta3 +"') AND semana = ('"+ semana+"') THEN 1 ELSE 0 END)AS Conteota3,SUM(CASE WHEN ta4=('"+ ta4+"') AND semana = ('"+ semana+"') THEN 1 ELSE 0 END) AS Conteota4,SUM(CASE WHEN ta5=('"+ta5+"') AND semana=('"+semana+"') THEN 1 ELSE 0 END) AS Conteota5,SUM(CASE WHEN ta6=('"+ta6+"') AND semana=('"+semana+"') THEN 1 ELSE 0 END)AS Conteota6,SUM(CASE WHEN ta7=('"+ta7+"') AND semana=('"+semana+"') THEN 1 ELSE 0 END)AS Conteota7, SUM(CASE WHEN ta8 = ('"+ta8+"') AND semana = ('"+semana+"') THEN 1 ELSE 0 END) AS Conteota8, SUM(CASE WHEN ta9= ('"+ ta9+"') AND semana = ('"+semana+"') THEN 1 ELSE 0 END) AS Conteota9,SUM(CASE WHEN ta10= ('"+ta10+"') AND semana = ('"+semana+"') THEN 1 ELSE 0 END) AS Conteota10, SUM(CASE WHEN ta11= ('"+ta11+"') AND semana = ('"+semana+"') THEN 1 ELSE 0 END) AS Conteota11,SUM(CASE WHEN ta12= ('"+ta12+"') AND semana = ('"+semana+"') THEN 1 ELSE 0 END) AS Conteota12,SUM(CASE WHEN ta13= ('"+ta13+"') AND semana = ('"+semana+"') THEN 1 ELSE 0 END) AS Conteota13,SUM(CASE WHEN ta14 = ('"+ta14+"') AND semana = ('"+semana+"') THEN 1 ELSE 0 END) AS Conteota14,SUM(CASE WHEN ta15= ('"+ta15+"') AND semana = ('"+semana+"') THEN 1 ELSE 0 END) AS Conteota15,SUM(CASE WHEN ta16= ('"+ta16+"') AND semana = ('"+semana+"') THEN 1 ELSE 0 END) AS Conteota16,SUM(CASE WHEN ta17= ('"+ta17+"') AND semana = ('"+semana+"') THEN 1 ELSE 0 END) AS Conteota17,SUM(CASE WHEN ta18= ('"+ta18+"') AND semana = ('"+semana+"') THEN 1 ELSE 0 END) AS Conteota18,SUM(CASE WHEN ta19= ('"+ta19+"') AND semana = ('"+semana+"') THEN 1 ELSE 0 END) AS Conteota19,SUM(CASE WHEN ta20= ('"+ta20+"') AND semana = ('"+semana+"') THEN 1 ELSE 0 END) AS Conteota20,SUM(CASE WHEN ta21= ('"+ta21+"') AND semana = ('"+semana+"') THEN 1 ELSE 0 END) AS Conteota21,SUM(CASE WHEN ta22 = ('"+ta22+"') AND semana = ('"+semana+"') THEN 1 ELSE 0 END) AS Conteota22,SUM(CASE WHEN tb1= ('"+tb1+"') AND semana = ('"+semana+"') THEN 1 ELSE 0 END) AS Conteota23, SUM(CASE WHEN tb2= ('"+tb2+"') AND semana = ('"+semana+"') THEN 1 ELSE 0 END) AS Conteota24, SUM(CASE WHEN tb3 = ('"+tb3+"') AND semana = ('"+semana+"') THEN 1 ELSE 0 END) AS Conteota25, SUM(CASE WHEN tb4 = ('"+tb4+"') AND semana = ('"+semana+"') THEN 1 ELSE 0 END) AS Conteota26  FROM ubicaciones WHERE ta1= ('"+ ta1+"') OR ta2=('"+ ta2 +"') OR ta3=('"+ta3+"') OR ta4=('"+ta4+"') OR ta5=('"+ta5+"') OR ta6=('"+ta6+"') OR ta7=('"+ta7+"') OR ta8=('"+ta8+"') OR ta9=('"+ta9+"') OR ta10=('"+ta10+"') OR ta11=('"+ta11+"')  OR ta12=('"+ta12+"') OR ta13=('"+ta13+"') OR ta14=('"+ta14+"') OR ta15=('"+ta15+"') OR ta16=('"+ta16+"') OR ta17=('"+ta17+"') OR ta18=('"+ta18+"') OR ta19=('"+ta19+"') OR ta20=('"+ta20+"') OR ta21=('"+ta21+"') OR ta22=('"+ta22+"') OR tb1=('"+tb1+"')OR tb2=('"+tb2+"')OR tb3=('"+tb3+"')OR tb4=('"+tb4+"')" 
    //console.log(tas)
    client
        .query(alltas)
        .then((result) => { io.emit('qtytas', { result,ta1,ta2,ta3,ta4,ta5,ta6,ta7,ta8,ta9,ta10,ta11,ta12,ta13,ta14,ta15,ta16,ta17,ta18,ta19,ta20,ta21,ta22,tb1,tb2,tb3,tb4,day,semana}); }) 
        .catch((err) => console.error('Error executing query', err.stack))
}

async function abrir() {
    // return new Promise(async resolve => {
    client = await postgresdb.connect()
    //resolve('resolved');
    //})
}
async function cerrar() {
    return new Promise(async resolve => {
        client.end()
        client.release()
        //postgresdb.release()
        resolve('resolved');
    })

}
//experimento para saber los
async function experiment() {
    console.log("total", postgresdb.totalCount)
    console.log("idle", postgresdb.idleCount)
    console.log("clientes esperando", postgresdb.waitingCount)

}

//***************************************************** TCP/IP PLC TESLA

//***************************************************** TCP/IP PLC TESLA

let plc_endresponse = 0
io.on('connection', (socket) => {

    socket.on('plc_response', function (result_matrix) {
        plcdatasender(result_matrix)
        console.log(result_matrix)
    })
})

var net = require('net')
var tcpipserver = net.createServer(function (connection) {
 try {  
    console.log('TCP client connected')
    connection.write('Handshake ok!\r\n')
    clientesConectados = []
    clientesConectados.push(connection);

    connection.on('data', function (data) { 
        console.log('Datos recibidos de PLC',  data.toString()) 
        io.emit('Timsequence_start', data.toString()); 
        console.log("Analisis in process...") })

//Responde a PLC cuando termine inspeccion
// Implementación de keep-alive
connection.on('timeout', function () {
    console.log('Keep-alive: Reenviando paquete de ping...');
    connection.write('Ping\r\n');
  });

connection.on('close', function () {
    console.log('Keep-alive: Conexión cerrada.')
    // Reintentar la conexión
  });

} catch (e){
    console.error(e.name + ": " + e.message)
    console.log("PLC NO RESPONDE EN TIEMPO A LA RESPUESTA DE PRUEBA...")
}
})

function plcdatasender(result_matrix) {
    clientesConectados.forEach(socket=>{
    matrixtostring = result_matrix.toString()
    plc_endresponse = matrixtostring
    //console.log("Cadena enviada a PLC",matrixtostring)
    try {
        estadoconexion = socket.readyState
        console.log("Comunicacion con el plc :" + socket.readyState)
        if (estadoconexion == 'closed') {
            console.log("Puerto de PLC cerrado")
        }
        if (estadoconexion == 'open') {
            socket.write(plc_endresponse)
        }
        } catch (e){
        console.error(e.name + ": " + e.message)
        console.log("PLC NO RESPONDE")
        }
    })
    
}


tcpipserver.listen(40000, function () {
    console.log('PLC Port 40000 listening...')
})

/*let plc_endresponse = 0
io.on('connection', (socket) => {

    socket.on('plc_response', function (result_matrix) {
        if (isConnected) {
            tcpWrite(result_matrix)
            console.log("seguimos conectados\n")
            console.log(result_matrix)
        } else {
            console.log("Estamos desconectados del PLC")
        }
    })

})

var net = require('net')
var tcpipserver = net.createServer(function (connection) {
    console.log('TCP client connected') //mensaje para saber que el cliente esta conectado 
    clientesConectados.push(connection) //se guardar al array todas las conexiones 
    connection.write('Handshake ok!\r\n')

    connection.on('data', function (data) {
        console.log("datos recibidos del plc: ", data.toString()) //se imprimen en el servidor los datos que recibe
        if (data.toString() == "hola") {
            setTimeout(() => {
                connection.write("hola")
            }, 100)
        }
        else {
            startSequence=0
            io.emit('Timsequence_start', data.toString());
            
        }
        console.log("Analisis in process...")
    })

    //Responde a PLC cuando termine inspeccion


    connection.on('end', () => {
        if (startSequence==0){
            isConnected = false
            let d = new Date();
            disconnected = d.getHours()
            console.log("PLC desconectado.. " + d)
        }
        
    })
})


tcpipserver.listen(40000, function () {
    console.log('PLC Port 40000 listening...')
})
function tcpWrite(result_matrix) {
    startSequence=1
    clientesConectados.forEach(socket => { //busca la conexion
        matrixtostring = result_matrix.toString()
        console.log('hola estamos en la conexion de nuevo para poder escribir ', startSequence)
        socket.write(matrixtostring)  //envia los datos al plc

    });

}

/*let plc_endresponse = 0
io.on('connection', (socket) => {

    socket.on('plc_response', function (result_matrix) {
        plcdatasender(result_matrix)
        //console.log(result_matrix)
    })
})

var net = require('net')
var tcpipserver = net.createServer(function (connection) {
 try {  
    console.log('TCP client connected')
    connection.write('Handshake ok!\r\n')
    clientesConectados.push(connection);

    connection.on('data', function (data) { 
        console.log('Datos recibidos de PLC',  data.toString())
        io.emit('Timsequence_start', data.toString()); 
        console.log("Analisis in process...") })

//Responde a PLC cuando termine inspeccion
// Implementación de keep-alive
connection.on('timeout', function () {
    console.log('Keep-alive: Reenviando paquete de ping...');
    connection.write('Ping\r\n');
  });

connection.on('close', function () {
    console.log('Keep-alive: Conexión cerrada. Reintentando...');
    // Reintentar la conexión
  });

} catch (e){
    console.error(e.name + ": " + e.message)
    console.log("PLC NO RESPONDE EN TIEMPO A LA RESPUESTA DE PRUEBA...")
}
})

function plcdatasender(result_matrix) {
    clientesConectados.forEach(socket=>{
    matrixtostring = result_matrix.toString()
    plc_endresponse = matrixtostring
    console.log("Cadena enviada a PLC",matrixtostring)
    try {
        estadoconexion = socket.readyState
        console.log("Comunicacion con el plc :" + socket.readyState)
        if (estadoconexion == 'closed') {
            console.log("Puerto de PLC cerrado")
        }
        if (estadoconexion == 'open') {
            socket.write(plc_endresponse)
        }
        } catch (e){
        console.error(e.name + ": " + e.message)
        console.log("PLC NO RESPONDE")
        }
    })
    
}


tcpipserver.listen(40000, function () {
    console.log('PLC Port 40000 listening...')
})*/




/*
let plc_endresponse = 0;
let io = require('socket.io')(server); // Asume que tienes un servidor HTTP creado

io.on('connection', (socket) => {
    console.log('Cliente conectado');

    socket.on('plc_response', function (result_matrix) {
        plcdatasender(result_matrix);
        // Emitir un evento al cliente para indicar que se iniciará la secuencia
        socket.emit('sequence_start', 'Secuencia iniciada');
    });

    socket.on('disconnect', () => {
        console.log('Cliente desconectado');
    });
});

var net = require('net');
var tcpipserver = net.createServer(function (connection) {
    try {
        console.log('TCP client conectado');

        connection.write('Handshake ok!\r\n');

        connection.on('data', function (data) {
            io.emit('Timsequence_start', data.toString());
            console.log("Análisis en proceso...");
        });

        // Responder al PLC cuando termine la inspección
        setTimeout(function respuesta() {
            try {
                estadoconexion = connection.readyState;
                console.log("Comunicación con el PLC: " + connection.readyState);

                if (estadoconexion == 'closed') {
                    console.log("Puerto de PLC cerrado, reintentando en 1 minuto...");
                }
                if (estadoconexion == 'open') {
                    connection.write(plc_endresponse);
                }
            } catch (e) {
                console.error(e.name + ": " + e.message);
                console.log("PLC NO RESPONDE");
            }
        }, 43000); // Tiempo de secuencia completa 40s para responder

        // Implementación de keep-alive
        connection.on('timeout', function () {
            console.log('Keep-alive: Reenviando paquete de ping...');
            connection.write('Ping\r\n');
        });

        connection.on('close', function () {
            console.log('Keep-alive: Conexión cerrada. Reintentando...');
            // Reintentar la conexión
        });

    } catch (e) {
        console.error(e.name + ": " + e.message);
        console.log("PLC NO RESPONDE EN TIEMPO A LA RESPUESTA DE PRUEBA...");
    }
});

let matrixtostring;
function plcdatasender(result_matrix) {
    matrixtostring = result_matrix.toString();
    plc_endresponse = matrixtostring;
    console.log("Cadena enviada a PLC", matrixtostring);
}

tcpipserver.listen(40000, function () {
    console.log('PLC Port 40000 listening...');
});

*/


//*************************  */
/*
let plc_endresponse = 0
io.on('connection', (socket) => {

    socket.on('plc_response', function (result_matrix) {
        tcpWrite(result_matrix)
        console.log(result_matrix)
    })
})

var net = require('net')
var tcpipserver = net.createServer(function (connection) {
    console.log('TCP client connected') //mensaje para saber que el cliente esta conectado 
    clientesConectados.push(connection) //se guardar al array todas las conexiones 
    connection.write('Handshake ok!\r\n') 

    connection.on('data', function (data) {
        console.log("datos recibidos del plc: ", fecha) //se imprimen en el servidor los datos que recibe
        if(data.toString() =="hola"){
            setTimeout(()=>{
                connection.write("hola")
            },100)
        }
        else{
            io.emit('Timsequence_start', data.toString());
        }
        console.log("Analisis in process...") })

    //Responde a PLC cuando termine inspeccion
        

connection.on('end', () =>{
    isConnected = false
    let d = new Date();
   
   disconnected = d.getHours()
   console.log("soy hora "+d)
   
    console.log("PLC desconectado.. "+d)
})
})


tcpipserver.listen(40000, function () {
    console.log('PLC Port 40000 listening...')
})
function tcpWrite(result_matrix){          
    
            clientesConectados.forEach(socket => { //busca la conexion
                matrixtostring = result_matrix.toString()
                console.log('hola estamos en la conexion de nuevo para poder escribir ', result_matrix)
                socket.write(matrixtostring)  //envia los datos al plc
                
            });
        
}

*/