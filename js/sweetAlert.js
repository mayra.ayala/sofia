

function fail(){
 
    Swal.fire({
          
          icon: 'error',
          title: 'Fail',
          //text: 'Something went wrong!',
  });
  }
  
function pass(){
    Swal.fire({
      
      icon: 'success',
      title: 'Pass',
    })
  }

function camedetect(count){
    Swal.fire({
      title: count,
      showClass: {
        popup: `
          animate__animated
          animate__fadeInUp
          animate__faster
        `
      },
      hideClass: {
        popup: `
          animate__animated
          animate__fadeOutDown
          animate__faster
        `
      }
    });
}

function timershow(){ 
let timerInterval;
Swal.fire({
  title: "Auto close alert!",
  html: "I will close in <b></b> milliseconds.",
  timer: 2000,
  timerProgressBar: true,
  didOpen: () => {
    Swal.showLoading();
    const timer = Swal.getPopup().querySelector("b");
    timerInterval = setInterval(() => {
      timer.textContent = `${Swal.getTimerLeft()}`;
    }, 100);
  },
  willClose: () => {
    clearInterval(timerInterval);
  }
}).then((result) => {
  /* Read more about handling dismissals below */
  if (result.dismiss === Swal.DismissReason.timer) {
    console.log("I was closed by the timer");
  }
});
}

function successfuly(){
  const Toast = Swal.mixin({
    toast: true,
    position: "top-end",
    showConfirmButton: false,
    timer: 3000,
    timerProgressBar: true,
    didOpen: (toast) => {
      toast.onmouseenter = Swal.stopTimer;
      toast.onmouseleave = Swal.resumeTimer;
    }
  });
  Toast.fire({
    icon: "success",
    title: "Signed in successfully"
  });
}

//Alertar cambio de calibracion
async function deletedata(){  
Swal.fire({
  position: "top-end",
  icon: "success",
  title: "localStorage Data has been delete",
  showConfirmButton: false,
  timer: 1500
});
}

//Alertar Terminado de calibracion
async function compcalibration(){  
  Swal.fire({
    position: "top-end",
    icon: "success",
    title: "CALIBRATION COMPLETE...",
    showConfirmButton: false,
    timer: 1500
  });
  
  }

  function deleteCalibration(){
    Swal.fire({
      title: "Are you sure?",
      text: "You won't be able to revert this!",
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Yes, delete it!"
    }).then((result) => {
      if (result.isConfirmed) {
        deletedatacalibration()
        Swal.fire({
          title: "Deleted!",
          text: "Your file has been deleted.",
          icon: "success"
        });
      }
    });
  }

async function inputypes(){
  const { value: fruit } = await Swal.fire({
    title: "Select field validation",
    input: "select",
    inputOptions: {
      Cuadrante1: {
        TA1: "TA1",
        TA2: "TA2",
        TA11: "TA11",
        TA12: "TA12"
      },
      Cuadrante2: {
        TA3: "TA3",
        TA4: "TA4",
        TA9: "TA9",
        TA10: "TA10"
      },
      Cuadrante3: {
        TA5: "TA5",
        TA6: "TA6",
        TA7: "TA7",
        TA8: "TA8"
      },
      Cuadrante4: {
        TA17: "TA17",
        TA18: "TA18"
      },
      Cuadrante5: {
        TA15: "TA15",
        TA16: "TA16",
        TA19: "TA19",
        TA20: "TA20"
      },
      Cuadrante6: {
        TA13: "TA13",
        TA14: "TA14"
      },
      Cuadrante7: {
        TA21: "TA21",
        TA22: "TA22"
      },
      //icecream: "Ice cream"
    },
    inputPlaceholder: "Select a TA",
    showCancelButton: true,
    inputValidator: (value) => {
      return new Promise((resolve) => {
        if (value === "TA1") {localStorage.setItem('TA1',value);resolve();
        } else if(value === 'TA2'){localStorage.setItem('TA2',value);resolve();
          resolve("You need to select oranges :)");
        }else if(value === 'TA11'){localStorage.setItem('TA11',value);resolve();
        }else if(value === 'TA12'){localStorage.setItem('TA12',value);resolve();
        }else if(value === 'TA3'){localStorage.setItem('TA3',value);resolve();
        }else if(value === 'TA4'){localStorage.setItem('TA4',value);resolve();
        }else if(value === 'TA9'){localStorage.setItem('TA9',value);resolve();
        }else if(value === 'TA10'){localStorage.setItem('TA10',value);resolve();
        }else if(value === 'TA5'){localStorage.setItem('TA5',value);resolve();
        }else if(value === 'TA6'){localStorage.setItem('TA6',value);resolve();
        }else if(value === 'TA7'){localStorage.setItem('TA7',value);resolve();
        }else if(value === 'TA8'){localStorage.setItem('TA8',value);resolve();
        }else if(value === 'TA17'){localStorage.setItem('TA17',value);resolve();
        }else if(value === 'TA18'){localStorage.setItem('TA18',value);resolve();
        }else if(value === 'TA15'){localStorage.setItem('TA15',value);resolve();
        }else if(value === 'TA16'){localStorage.setItem('TA16',value);resolve();
        }else if(value === 'TA19'){localStorage.setItem('TA19',value);resolve();
        }else if(value === 'TA20'){localStorage.setItem('TA20',value);resolve();
        }else if(value === 'TA13'){localStorage.setItem('TA13',value);resolve();
        }else if(value === 'TA14'){localStorage.setItem('TA14',value);resolve();
        }else if(value === 'TA21'){localStorage.setItem('TA21',value);resolve();
        }else if(value === 'TA22'){localStorage.setItem('TA2',value);resolve();
        }else{console-log('SELECT TA')}
      });
    }
  });
  if (fruit) {
    Swal.fire(`You selected: ${fruit}`);
  }}

  function idcameras(){
    Swal.fire({

      icon: "info",
      title: "IDs",
      text: `Camera 1 (${Point1ID})`,
      text: `Camera 2 (${Point2ID})`,
      text: `Camera 3 (${Point3ID})`,
    });
  }