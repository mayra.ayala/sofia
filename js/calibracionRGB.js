let mean
let BarTA1
let valorP
let valorMaximoR
let valorminR
let valorMaximoG
let valorMinG
let valorMaximoB
let valorMinB
let valorMaximoRFinal
let valorminRFinal
let valorMaximoGFinal
let valorMinGFinal
let valorMaximoBFinal
let valorMinBFinal
let tax

//---Variables y arrays para primera validacion
let V1arrymaxG =[]
let V1arryminG = []

let V1arrymaxB = []
let V1arryminB = []
//Arrays para localstorage 
//Rojo
let V1miArrayminR =[]
let V1miArraymaxR =[]
//Verde
let V1miArrayminG =[]
let V1miArraymaxG =[]
//Azul
let V1miArrayminB =[]
let V1miArraymaxB =[]

//----------Variables y arrays para segunda validacion
let V2arrymaxG =[]
let V2arryminG = []

let V2arrymaxB = []
let V2arryminB = []
//Arrays para localstorage 
//Rojo
let V2miArrayminR =[]
let V2miArraymaxR =[]
//Verde
let V2miArrayminG =[]
let V2miArraymaxG =[]
//Azul
let V2miArrayminB =[]
let V2miArraymaxB =[]

function enviar(){ 
  let selectElement = document.getElementById("TA1");
  let selectedIndex = selectElement.selectedIndex;
  selectedValue = selectElement.options[selectedIndex].value;
  console.log(selectedValue);
}
async function TA1activado(){
      BarTA1 = 'TA1'
      console.log('TA1 Activado para recoleccion de datos')
      timershow()
}

//Valores que se ingresaran al prompt
function valorprompt(){
// Crear un bucle for que se repita 5 veces
  for (var i = 0; i < 1; i++) {
    // Pedir el valor de la variable
    var valorP = prompt("Ingresa TA para calibracion automatica: " + (i + 1) + ":", "");

    // Guardar el valor en localStorage
    localStorage.setItem("variable" + (i + 1), valorP);
    successfuly()
  }
}

function clearlocalstorage(){
  // Borrar una variable almacenada en localStorage
    localStorage.removeItem('variable1')
    localStorage.removeItem('ValmaxR')
    localStorage.removeItem('ValminR')
    localStorage.removeItem('ValmaxG')
    localStorage.removeItem('ValminG')
    localStorage.removeItem('ValmaxB')
    localStorage.removeItem('ValminB')
    localStorage.removeItem('ValmaxR')
    localStorage.removeItem('ValmaxG')
    localStorage.removeItem('ValminG')
    localStorage.removeItem('ValmaxB')
    localStorage.removeItem('ValminB')

    localStorage.removeItem('valorMaximoRFinal')
    localStorage.removeItem('valorminRFinal')
    localStorage.removeItem('valorMaximoGFinal')
    localStorage.removeItem('valorMinGFinal')
    localStorage.removeItem('valorMaximoBFinal')
    localStorage.removeItem('valorMinBFinal')
    
    deletedata()
}


function deletedatacalibration(){
  localStorage.removeItem('replaceTA1MaxR')
  localStorage.removeItem('replaceTA1MinR')
  localStorage.removeItem('replaceTA1MaxG')
  localStorage.removeItem('replaceTA1MinG')
  localStorage.removeItem('replaceTA1MinB')
  localStorage.removeItem('replaceTA1MaxB')
}



async function rgbauto(canvasx) {
    let contextcanvasx = canvasx.getContext('2d')
    let cdata = contextcanvasx.getImageData(0, 0, canvasx.width, canvasx.height)
    valoresR = []
    valoresG = []
    valoresB = []
    valoresA = []
    for (let i = 0; i < cdata.data.length; i += 4) {
        //este arreglo nos da los valores de cada pixel
        R = cdata.data[i + 0]
        G = cdata.data[i + 1]
        B = cdata.data[i + 2]
        A = cdata.data[i + 3]
        //aqui se guardan los valores de cada pixel en el arreglo correspondiente
        valoresR.push(R)
        valoresG.push(G)
        valoresB.push(B)
        valoresA.push(A)
    }
    //---------- Valores de Rojos
    //identifica numero minimo y maximo del array 
    let ValmaxR = Math.max(...valoresR)
    let ValminR = Math.min(...valoresR)
    //console.log("Valores R:", valoresR)
    //console.log("ValormaximoR:", ValmaxR)
    //console.log("ValorminimoR:", ValminR)

    //------ Guarda valores de rangos Min y Max en localStorage
    
    let miArrayminR = JSON.parse(localStorage.getItem('ValminR')) || [];
    let miArraymaxR = JSON.parse(localStorage.getItem('ValmaxR')) || [];//Agrega el dato en localStorage

    if(miArraymaxR.length <= 9  && miArrayminR.length <= 9){
        //console.log("No has llegado al limite de datos")
        //Agrega valores al localstorage
        miArraymaxR.push(ValmaxR) 
        miArrayminR.push(ValminR)
        localStorage.setItem('ValmaxR', JSON.stringify(miArraymaxR));
        localStorage.setItem('ValminR', JSON.stringify(miArrayminR));
    }else if(miArraymaxR.length >= 9  && miArrayminR.length >= 9){ 
      
      valorMaximoR = Math.max(...miArraymaxR)
      localStorage.setItem('valorMaximoRFinal',valorMaximoR)
      valorminR = Math.min(...miArrayminR)
      localStorage.setItem('valorminRFinal',valorminR)
      //Extraer datos finales de localStorage una vez terminadas 10 muestras
      valorMaximoRFinal = localStorage.getItem('valorMaximoRFinal')
      valorminRFinal = localStorage.getItem('valorminRFinal')
    }
    
//------------------------------------- AQUI SE TERMINA ROJO... -------------------------------------------

    //---------- Valores de Verdes
    let ValmaxG = Math.max(...valoresG)
    let ValminG = Math.min(...valoresG)

    //------ Guarda valores de rangos Min y Max en localStorage
    
     miArrayminG = JSON.parse(localStorage.getItem('ValminG')) || [];
     miArraymaxG = JSON.parse(localStorage.getItem('ValmaxG')) || [];

    if(miArrayminG.length <= 9  && miArraymaxG.length <= 9){
      miArrayminG.push(ValminG) 
      miArraymaxG.push(ValmaxG)
      localStorage.setItem('ValminG', JSON.stringify(miArrayminG));
      localStorage.setItem('ValmaxG', JSON.stringify(miArraymaxG));
      //console.log("miArraymaxG: ",miArraymaxG) //muestra almacenamiento de localstorage en consola
      //console.log("miArrayminG: ",miArrayminG)
  }else if(miArraymaxG.length >= 9  && miArraymaxG.length >= 9){ 
      valorMaximoG = Math.max(...miArraymaxG)
      localStorage.setItem('valorMaximoGFinal', valorMaximoG)
      valorMinG = Math.min(...miArrayminG)
      localStorage.setItem('valorMinGFinal', valorMinG)

      valorMaximoGFinal = localStorage.getItem('valorMaximoGFinal')
      valorMinGFinal =  localStorage.getItem('valorMinGFinal')
    }
  
//--------------------- AQUI TERMINA VERDE  
    //---------- Valores de azules
    let ValmaxB = Math.max(...valoresB)
    let ValminB = Math.min(...valoresB)
 
    let miArrayminB = JSON.parse(localStorage.getItem('ValminB')) || [];
    let miArraymaxB = JSON.parse(localStorage.getItem('ValmaxB')) || [];

    if(miArrayminB.length <= 9  && miArraymaxB.length <= 9){
      //console.log("No has llegado al limite de datos")
      //Agrega valores al localstorage
      miArrayminB.push(ValminB) 
      miArraymaxB.push(ValmaxB)
      localStorage.setItem('ValminB', JSON.stringify(miArrayminB));
      localStorage.setItem('ValmaxB', JSON.stringify(miArraymaxB));
      //console.log("miArraymaxB: ",miArraymaxB) //muestra almacenamiento de localstorage en consola
    }else if(miArraymaxB.length >= 9  && miArrayminB.length >= 9){ 
      console.log("LLegaste al limite de datos")
      valorMaximoB = Math.max(...miArraymaxB)
      localStorage.setItem('valorMaximoBFinal', valorMaximoB)
      //console.log("Valor maximo en B en LocalStorage: ",valorMaximoB)
      valorMinB = Math.min(...miArrayminB)
      localStorage.setItem('valorMinBFinal', valorMinB)

      valorMaximoBFinal = localStorage.getItem('valorMaximoBFinal')
      valorMinBFinal = localStorage.getItem('valorMinBFinal')

      //Extraer variable que identifique TAx
      tax = localStorage.getItem('variable1')     
      insertdatargb(tax,valorminRFinal,valorMaximoRFinal,valorMinGFinal,valorMaximoGFinal,valorMinBFinal,valorMaximoBFinal)
        }
    //--------- Calculo de Media y desviacion estandar 

function desviacion(valores) {
        // Calcular la media
        mean = valores.reduce((acc, curr) => acc + curr, 0) / valores.length;
      
        // Calcular la varianza
        let variance = valores.map((k) => {
          return (k - mean) ** 2;
        }).reduce((acc, curr) => acc + curr, 0) / valores.length;
      

        // Calcular la desviación estándar
        return Math.sqrt(variance);
    }

      desviacionR = desviacion(valoresR)
      desviacionG = desviacion(valoresG)
      desviacionB = desviacion(valoresB)
      desviacionA = desviacion(valoresA)
     
      valormaximoR = mean + desviacionR
      valorminimoR = mean - desviacionR
      valormaximoG = mean + desviacionG
      valorminimoG = mean - desviacionG
      valormaximoB = mean + desviacionB
      valorminimoB = mean - desviacionB
      valormaximoA = mean + desviacionA
      valorminimoA = mean - desviacionA

      let meanR = valoresR.reduce((acc, curr) => acc + curr, 0) / valoresR.length;
      let meanG = valoresG.reduce((acc, curr) => acc + curr, 0) / valoresG.length;
      let meanB = valoresB.reduce((acc, curr) => acc + curr, 0) / valoresB.length;

}


function insertdatargb(tax,valorminRFinal,valorMaximoRFinal,valorMinGFinal,valorMaximoGFinal,valorMinBFinal,valorMaximoBFinal){
  //console.log('Dentro de la funcion insertdatargb',tax,valorminRFinal,valorMaximoRFinal,valorMinGFinal,valorMaximoGFinal,valorMinBFinal,valorMaximoBFinal)
  const socket = io();
  socket.emit('datargb',tax, valorminRFinal,valorMaximoRFinal,valorMinGFinal,valorMaximoGFinal,valorMinBFinal,valorMaximoBFinal)

}

// Función para bloquear los valores
function bloquearValores() {
  valoresBloqueados = true;
}

// Función para desbloquear los valores
function desbloquearValores() {
  valoresBloqueados = false;
}