



let statusx // Resultado de analisis pass(1)/Fail(0) se utiliza en la funcion logresult()
let logsave = [] //Array que utilizamos en la funcion logresult() y  almacena resumen
let cuadranteArray = [] // Array para evaluar pass o fail de cuadrante
let porcentajeArray = [] // Array que guarda el porcentaje en cada punto 
let mtxw = [] //Array que guarda el porcentaje 
let IAdesition = [] //Alamcena el punto que inspeccionara la IA 
let CadenaComplete = [] // Array que se utilizara para almacenar la cadena de logsave
let logta=[]
let tasArry=[]
let valores = []
let valta1 = []
let criterio // Esta variable se utiliza como criterio de pase para % de TIM x cada TA
let criterio2 = .990 //Variable para validacion de un criterio al 90%
let ruido
let latapona
let resultado
let snr //= "soyserial"
let snfile // = //"segundo intento bd"
let turno_pass_qty1
let turno_fail_qty1
let turno_pass_qty2
let turno_fail_qty2
let turno_pass_qtyD
let bar
let mondayPass
let mondayFail
let TuesdayPass
let TuesdayFail
let WednesdayPass
let WednesdayFail
let ThursdayPass
let ThursdayFail
let FridayPass
let FridayFail
let SaturdayPass
let SaturdayFail
let SundayPass
let SundayFail
let Yield
let selectedValue
let Point1ID
let Point2ID
let Point3ID
let valoresBloqueados = false
//Variables guardan valores de calibracion desde BD
let tacalibration
let Rmin
let Rmax
let Gmin
let Gmax 
let Bmin 
let Bmax
//Variables para valores nuevos de calibracion TA1
let replaceTA1MaxR
let replaceTA1MinR
let replaceTA1MaxG
let replaceTA1MinG
let replaceTA1MinB
let replaceTA1MaxB



//let model = new cvstfjs.ClassificationModel(); //Variable que se utiliza en mlinspector para cargar el modelo 
let pasa //Variable utilizada para guardar el resultado pass en la red neuronal
let falla //Variable utilizada para guardar el resultado fail en la red neuronal
let datostas //Variable para datos de TAs
//Variables a utilizar para guardar data en BD
let ta1statusgood;let ta2statusgood;let ta3statusgood;let ta4statusgood;let ta5statusgood;let ta6statusgood;let ta7statusgood;let ta8statusgood
let ta9statusgood;let ta10statusgood;let ta12statusgood;let ta13statusgood;let ta14statusgood;let ta15statusgood;let ta16statusgood;let ta17statusgood
let ta18statusgood;let ta19statusgood;let ta20statusgood;let ta21statusgood;let ta22statusgood;let ta23statusgood;let ta24statusgood;let ta25statusgood
let ta26statusgood;let ta1statusbad;let ta2statusbad;let ta3statusbad;let ta4statusbad;let ta5statusbad;let ta6statusbad;let ta7statusbad;let ta8statusbad
let ta9statusbad;let ta10statusbad;let ta11statusbad;let ta12statusbad;let ta13statusbad;let ta14statusbad;let ta15statusbad;let ta16statusbad
let ta17statusbad;let ta18statusbad;let ta19statusbad;let ta20statusbad;let ta21statusbad;let ta22statusbad;let ta23statusbad;let ta24statusbad
let ta25statusbad;let ta26statusbad
let ta1fail;let ta2fail;let ta3fail; let ta4fail;let ta5fail; let ta6fail;let ta7fail;let ta8fail; let ta9fail; let ta10fail;let ta11fail;let ta12fail
let ta13fail;let ta14fail; let ta15fail; let ta16fail; let ta17fail;let ta18fail;let ta19fail;let ta20fail;let ta21fail;let ta22fail;let ta23fail;let ta24fail
let ta25fail;let ta26fail
let valmax = 100
//-----------Canvas--------------------//
let fullimage = document.getElementById('CanvasFHD')
let contextfullimage = fullimage.getContext('2d')

let canvasCamara = document.getElementById('canvasCamara')
let contextcanvasCamara = canvasCamara.getContext('2d')

let canvasClen1 = document.getElementById('canvasClen1') //canvas 1,3
let contextcanvasClen1 = canvasClen1.getContext('2d')

let canvasMlen1 = document.getElementById('canvasMlen1') //canvas 12
let contextcanvasMlen1 = canvasMlen1.getContext('2d')

let canvasGlen1 = document.getElementById('canvasGlen1') //canvas 2 y 11
let contextcanvasGlen1 = canvasGlen1.getContext('2d')


let canvasClen2 = document.getElementById('canvasClen2')
let contextcanvasClen2 = canvasClen2.getContext('2d')

let canvasTA6len1 = document.getElementById('canvasTA6len1')
let contextcanvasTA6len1 = canvasTA6len1.getContext('2d')

let canvasGlen2 = document.getElementById('canvasGlen2')
let contextcanvasGlen2 = canvasGlen2.getContext('2d')

let canvasTGlen2 = document.getElementById('canvasTGlen2')
let contextcanvasTGlen2 = canvasTGlen2.getContext('2d')

let canvasflalen3 = document.getElementById('canvasflalen3')
let contextcanvasflalen3 = canvasflalen3.getContext('2d')

let canvasgolen3 = document.getElementById('canvasgolen3')
let contextcanvasgolen3 = canvasgolen3.getContext('2d')

let canvasTB = document.getElementById('canvasTB')
let contextcanvasTB = canvasTB.getContext('2d')

let canvasTB4 = document.getElementById('canvasTB4')
let contextcanvasTB4 = canvasTB4.getContext('2d')

//----- canvas de rotacion ----- //
let canvasnuevo = document.getElementById('canvasnuevo')
let contextcanvasnuevo = canvasnuevo.getContext('2d')

//------------------ Cuadrante 1 --------------------------- //
let canvaspaste = document.getElementById('canvaspaste')
let contextcanvaspaste = canvaspaste.getContext('2d')

//------------------ Cuadrante 2 --------------------------- //
let canvaspasteC2 = document.getElementById('canvaspasteC2')
let contextcanvaspasteC2 = canvaspasteC2.getContext('2d')

//------------------ Cuadrante 3 --------------------------- //
let canvaspasteC3 = document.getElementById('canvaspasteC3')
let contextcanvaspasteC3 = canvaspasteC3.getContext('2d')

//------------------ Cuadrante 4 --------------------------- //
let canvasnuevo4 = document.getElementById('canvasnuevo4')
let contextcanvasnuevo4 = canvasnuevo4.getContext('2d')

let canvaspaste4 = document.getElementById('canvaspaste4')
let contextcanvaspaste4 = canvaspaste4.getContext('2d')

//------------------ Cuadrante 5 --------------------------- //
let canvaspasteC5 = document.getElementById('canvaspasteC5')
let contextcanvaspasteC5 = canvaspasteC5.getContext('2d')

//------------------ Cuadrante 5 --------------------------- //
let canvaspasteC6 = document.getElementById('canvaspasteC6')
let contextcanvaspasteC6 = canvaspasteC6.getContext('2d')

let canvasnuevo7 = document.getElementById('canvasnuevo7')
let contextcanvasnuevo7 = canvasnuevo7.getContext('2d')

let canvaspaste7 = document.getElementById('canvaspaste7')
let contextcanvaspaste7 = canvaspaste7.getContext('2d')

let image = new Image() //Carga caricatura

image.src = '/img/caricatura.png'

image.onload = function () { //se usa para ejecutar una función de JavaScript tan pronto como una página haya cargado
    contextcanvasCamara.drawImage(image, 0, 0, image.width, image.height, 0, 0, contextcanvasCamara.canvas.width, contextcanvasCamara.canvas.height);
}
window.onload = async function () { //en cuanto se recargue 
    return new Promise(async resolve => {
        await abrir()
if(day == 'Monday'){
        await agrupapasst2('1', 'pass','Monday', fecha)
        await agrupapasst2('2', 'pass','Monday', fecha)
        await agrupapasst2('3', 'pass','Monday', fecha)
        await agrupapasst2('1', 'fail','Monday', fecha)
        await agrupapasst2('2', 'fail','Monday', fecha)
        await agrupapasst2('3', 'fail','Monday', fecha)
        
} if (day == 'Tuesday'){
        await agrupapasst2('1', 'pass','Tuesday', fecha)
        await agrupapasst2('2', 'pass','Tuesday', fecha)
        await agrupapasst2('3', 'pass','Tuesday', fecha)
        await agrupapasst2('1', 'fail','Tuesday', fecha)
        await agrupapasst2('2', 'fail','Tuesday', fecha)
        await agrupapasst2('3', 'fail','Tuesday', fecha)
} if (day == 'Wednesday'){
        await agrupapasst2('1', 'pass','Wednesday', fecha)
        await agrupapasst2('2', 'pass','Wednesday', fecha)
        await agrupapasst2('3', 'pass','Wednesday', fecha)
        await agrupapasst2('1', 'fail','Wednesday', fecha)
        await agrupapasst2('2', 'fail','Wednesday', fecha)
        await agrupapasst2('3', 'fail','Wednesday', fecha)
} if (day == 'Thursday'){
        await agrupapasst2('1', 'pass','Thursday', fecha)
        await agrupapasst2('2', 'pass','Thursday', fecha)
        await agrupapasst2('3', 'pass','Thursday', fecha)
        await agrupapasst2('1', 'fail','Thursday', fecha)
        await agrupapasst2('2', 'fail','Thursday', fecha)
        await agrupapasst2('3', 'fail','Thursday', fecha)
} if (day == 'Friday'){
        await agrupapasst2('1', 'pass','Friday', fecha)
        await agrupapasst2('2', 'pass','Friday', fecha)
        await agrupapasst2('3', 'pass','Friday', fecha)
        await agrupapasst2('1', 'fail','Friday', fecha)
        await agrupapasst2('2', 'fail','Friday', fecha)
        await agrupapasst2('3', 'fail','Friday', fecha)
        
}if (day == 'Saturday'){
        await agrupapasst2('1', 'pass','Saturday', fecha)
        await agrupapasst2('2', 'pass','Saturday', fecha)
        await agrupapasst2('3', 'pass','Saturday', fecha)
        await agrupapasst2('1', 'fail','Saturday', fecha)
        await agrupapasst2('2', 'fail','Saturday', fecha)
        await agrupapasst2('3', 'fail','Saturday', fecha)
}if (day == 'Sunday'){
        await agrupapasst2('1', 'pass','Sunday', fecha)
        await agrupapasst2('2', 'pass','Sunday', fecha)
        await agrupapasst2('3', 'pass','Sunday', fecha)
        await agrupapasst2('1', 'fail','Sunday', fecha)
        await agrupapasst2('2', 'fail','Sunday', fecha)
        await agrupapasst2('3', 'fail','Sunday', fecha)
}
await numsem() 
resolve('resolved')})}



/**************************************************** Muestra hora local */
const d = new Date();
let hora = d.getHours();
document.getElementById("demo").innerHTML = hora;
//console.log(hora)

/**************************************************** Muestra dia de calendario */
const weekday = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];
const dia = new Date();
let day = weekday[dia.getDay()];
document.getElementById("demo").innerHTML = day;

//console.log(day)

//numero
const dian = new Date();
let num = dian.getDate()
document.getElementById("demo").innerHTML = num;

//console.log(num)
//mes

const months= ["January","February","March","April","May","June","July","August","September","October","November","December"];
const m = new Date();
let month = months[m.getMonth()];

//año
const y = new Date();
let year = y.getFullYear();


let fecha = year+"-"+month+"-"+num
//console.log(typeof(fecha))


function getweek(date){
    var onejan = new Date (date.getFullYear(),0,1)
    return Math.ceil((((date - onejan) / 86400000) + onejan.getDay()+1)/7)
   
}
var date = new Date(fecha);
let semana = getweek(date);
console.log(semana);


const fechaActual = new Date()
const diaSemana = fechaActual.getDay()

async function numsem(){
if((diaSemana == 0 ||diaSemana == 1 ||diaSemana == 2 ||diaSemana == 3 ||diaSemana == 4 ||diaSemana == 5 ||diaSemana == 6 && (getweek(date)) == getweek(date))){
    await agrupardias('pass', 'Monday', semana)
    await agrupardias('fail', 'Monday', semana)
    await agrupardias('pass', 'Tuesday', semana)
    await agrupardias('fail', 'Tuesday', semana)
    await agrupardias('pass', 'Wednesday', semana)
    await agrupardias('fail', 'Wednesday', semana)
    await agrupardias('pass', 'Thursday', semana)
    await agrupardias('fail', 'Thursday', semana)
    await agrupardias('fail', 'Friday', semana)
    await agrupardias('pass', 'Friday', semana)
    //await querytas('pass','Friday',semana)
    await agrupardias('pass', 'Saturday', semana)
    await agrupardias('fail', 'Saturday',  semana)
    await agrupardias('pass', 'Sunday', semana)
    await agrupardias('fail', 'Sunday',  semana)
    await querytas('fail','fail','fail','fail','fail','fail','fail','fail','fail','fail','fail','fail','fail','fail','fail','fail','fail','fail','fail','fail','fail','fail','fail','fail','fail','fail',day,semana)//la posicion representa a cada TAx
    }
}

//************************ GRAFICA LINEAL */ 
async function iniciar() {
    return new Promise(async resolve => {

        const bar_ctx = document.getElementById('bar').getContext('2d');
        let turno1 = 1
        let turno2 = 2
        let turno3 = 3

        bar = new Chart(bar_ctx, {
            type: 'bar',
            data: {

                labels: [turno1, turno2, turno3],
                datasets: [{
                    label: 'PASS',
                    data: [],//[56,28,30,15,36,20,40,45, 34], 
                    backgroundColor: '#66FF66',
                    borderColor: '#66FF66',
                    borderWidth: 1
                }, {
                    label: 'FAIL',
                    data: [],// [12, 19, 3, 5, 4, 6, 2, 4, 2],
                    backgroundColor: '#FF0000',
                    borderColor: '#FF0000',
                    borderWidth: 1
                }]
            },
            options: {
                scales: {
                    x: {
                        grid: {
                            display: false
                        }
                    },
                    y: {
                        beginAtZero: false,
                        grid: {
                            display: false
                        }
                    }
                }
            }

        });
        //console.log(bar)
        resolve('resolved');
    })

}
iniciar()

/***************************** GRAFICA BARRAS */

async function iniciar2() {
    return new Promise(async resolve => {
        //variable = 15
        let Monday = 'Monday'
        let Tuesday = 'Tuesday'
        let Wednesday = 'Wednesday'
        let Thursday = 'Thursday'
        let Friday = 'Friday'
        let Saturday = 'Saturday'
        let Sunday = 'Sunday'
        //let turno2 = 2
        //let turno3 = 3
        line_ctx = document.getElementById('linea').getContext('2d');
        linea = new Chart(line_ctx, {
            type: 'line',
            data: {
                labels: [Monday, Tuesday, Wednesday, Thursday, Friday, Saturday, Sunday],
                datasets: [{
                    label: 'PASS',
                    data: [],//[variable, 19, 3, 5, 2, 3],
                    backgroundColor: '#66FF66',
                    borderColor: '#66FF66',
                    borderWidth: 1
                }, {
                    label: 'FAIL',
                    data: [],//[5, 9, 6, 2, 1,15],
                    backgroundColor: '#FF0000',
                    borderColor: '#FF0000',
                    borderWidth: 1
                }]
            },
            options: {

                scales: {
                    x: {
                        grid: {
                            display: false
                        }
                    },
                    y: {
                        beginAtZero: true,
                        grid: {
                            display: false
                        }
                    }
                }
            }
        });
       // console.log(bar)
        resolve('resolved');
    })
}

iniciar2()

/***************************** GRAFICA BARRAS PARA TA's */

async function grafictas(){
    return new Promise(async resolve => {
        const bardos_ctx = document.getElementById('bardos').getContext('2d');
        let TA1 = 'TA1';let TA2 = 'TA2';let TA3 = 'TA3';let TA4 = 'TA4';let TA5 = 'TA5';let TA6 = 'TA6';let TA7 = 'TA7'
        let TA8 = 'TA8';let TA9 = 'TA9';let TA10 = 'TA10';let TA11 = 'TA11';let TA12 = 'TA12';let TA13 = 'TA13'
        let TA14 = 'TA14';let TA15 = 'TA15';let TA16 = 'TA16';let TA17 = 'TA17';let TA18 = 'TA18';let TA19 = 'TA19'
        let TA20 = 'TA20';let TA21 = 'TA21';let TA22 = 'TA22';let TA23 = 'TA23';let TA24 = 'TA24';let TA25 = 'TA25'
        let TA26 = 'TA26'

        bardos = new Chart(bardos_ctx, {
            type: 'bar',
            data: {
                labels: [null,TA1,TA2,TA3,TA4,TA5,TA6,TA7,TA8,TA9,TA10,TA11,TA12,TA13,TA14,TA15,TA16,TA17,TA18,TA19,TA20,TA21,TA22,TA23,TA24,TA25,TA26],
                datasets: [{
                    label: 'FAIL-SEMANA',
                    data: [tasArry],//[56,28,30,15,36,20,40,45, 34], 
                    backgroundColor: '#FF0000',
                    borderColor: '#FF0000',
                    borderWidth: 1, 
                }, ]
            },
            options: {
                
                   
                        
                        scales: {
                            x: {
                                grid: {
                                    display: false
                                }
                            },
                            y: {
                                beginAtZero: false,
                                grid: {
                                    display: false
                                }
                            }
                        },
                    resposive: true,
                    plugins: {
                        legend: {}
                    },
                    title: {
                        display: true,
                        text: 'FAILED'
                    }
                    
                
            }

        });
        //console.log(bar)
        resolve('resolved');
    })
}

grafictas()

//*************************Socket block */
const socket = io();

socket.on('Timsequence_start', function (infoplc) {//pg migrated
    split(infoplc)
    //console.log(infoplc)
    if (infoplc != 0) {

        station = infoplc.toString().substr(2, 8)
        snr = infoplc.toString().substr(11, 29)
        snfile = infoplc.toString().substr(25, 15)
        pn = infoplc.toString().substr(41, 16)
        console.log("Start test sequence");
        testsequence()//Activa bandera para continuar

        
        // console.log(typeof(data))
        //console.log(infoplc)
        //console.log(pn)
    }
    else {
        console.log("Algo salio mal en el backend");
    }
});
//---------------------------------- Matriz que imprime en log y manda cadena al PLC/
function plc_response(logsave) {
    return new Promise(async resolve => {
        //console.log(logsave)
        porcentajeArray =
            "" + snr + "\n\nCuadrante :1\n" +
            "TA1-:  " + "status :" + logsave[1] + ", " + "percent -->  " + porcentajeArray[1] * 100 + "(%) " + `IA Inspection : ${IAdesition[1] == 0 ? 'Fail' : (IAdesition[1] == 1 ? 'Pass' : 'On hold')} ` + "\n" + //Operador ternario que se interpreta como in else if 
            "TA2-:  " + "status :" + logsave[2] + ", " + "percent -->  " + porcentajeArray[2] * 100 + "(%) " + `IA Inspection : ${IAdesition[2] == 0 ? 'Fail' : (IAdesition[2] == 1 ? 'Pass' : 'On hold')} ` + "\n" + //Operador ternario que se interpreta como in else if 
            "TA11-: " + "status :" + logsave[3] + ", " + "percent -->  " + porcentajeArray[11] * 100 + "(%) " +`IA Inspection : ${IAdesition[11] == 0 ? 'Fail' : (IAdesition[11] == 1 ? 'Pass' : 'On hold')} ` + "\n" + //Operador ternario que se interpreta como in else if 
            "TA12-: " + "status :" + logsave[4] + ", " + "percent -->  " + porcentajeArray[12] * 100 + "(%) " +`IA Inspection : ${IAdesition[12] == 0 ? 'Fail' : (IAdesition[12] == 1 ? 'Pass' : 'On hold')} ` + "\n" + //Operador ternario que se interpreta como in else if 
            "TB1-:  " + "status :" + logsave[5] + ", " + "percent -->  " + porcentajeArray[23] * 100 + "(%)\n\n" +
            //Cuadrante 2 
            "Cuadrante :2\n" +
            "TA3-:  " + "status :" + logsave[6] + ",  " + "percent -->  " + porcentajeArray[3] * 100 + "(%) " + `IA Inspection : ${IAdesition[3] == 0 ? 'Fail' : (IAdesition[3] == 1 ? 'Pass' : 'On hold')} ` + "\n" + //Operador ternario que se interpreta como in else if 
            "TA4-:  " + "status :" + logsave[7] + ",  " + "percent -->  " + porcentajeArray[4] * 100 + "(%) " + `IA Inspection : ${IAdesition[4] == 0 ? 'Fail' : (IAdesition[4] == 1 ? 'Pass' : 'On hold')} ` + "\n" + //Operador ternario que se interpreta como in else if 
            "TA9-:  " + "status :" + logsave[8] + ",  " + "percent -->  " + porcentajeArray[9] * 100 + "(%) " +  `IA Inspection : ${IAdesition[9] == 0 ? 'Fail' : (IAdesition[9] == 1 ? 'Pass' : 'On hold')} ` + "\n" + //Operador ternario que se interpreta como in else if 
            "TA10-: " + "status :" + logsave[9] + ",  " + "percent -->  " + porcentajeArray[10] * 100 + "(%) " +  `IA Inspection : ${IAdesition[10] == 0 ? 'Fail' : (IAdesition[10] == 1 ? 'Pass' : 'On hold')} ` + "\n" + //Operador ternario que se interpreta como in else if 
            "TB2-:  " + "status :" + logsave[10] + ",  " + "percent -->  " + porcentajeArray[24] * 100 + "(%)\n\n" +// `IA Inspection: ${IAdesition[24] == 0 ? 'Fail' : 'Pass'} ` + "\n\n" +
            // Cuadrante3
            "Cuadrante :3\n" +
            "TA5-: " + "status :" + logsave[11] + ", " + "percent -->  " + porcentajeArray[5] * 100 + "(%) " +  `IA Inspection : ${IAdesition[5] == 0 ? 'Fail' : (IAdesition[5] == 1 ? 'Pass' : 'On hold')} ` + "\n" + //Operador ternario que se interpreta como in else if 
            "TA6-: " + "status :" + logsave[12] + ", " + "percent -->  " + porcentajeArray[6] * 100 + "(%) " +  `IA Inspection : ${IAdesition[6] == 0 ? 'Fail' : (IAdesition[6] == 1 ? 'Pass' : 'On hold')} ` + "\n" + //Operador ternario que se interpreta como in else if 
            "TA7-: " + "status :" + logsave[13] + ", " + "percent -->  " + porcentajeArray[7] * 100 + "(%) " +  `IA Inspection : ${IAdesition[7] == 0 ? 'Fail' : (IAdesition[7] == 1 ? 'Pass' : 'On hold')} ` + "\n" + //Operador ternario que se interpreta como in else if 
            "TA8-: " + "status :" + logsave[14] + ", " + "percent -->  " + porcentajeArray[8] * 100 + "(%) " +  `IA Inspection : ${IAdesition[8] == 0 ? 'Fail' : (IAdesition[8] == 1 ? 'Pass' : 'On hold')} ` + "\n" + //Operador ternario que se interpreta como in else if 
            "TB3-: " + "status :" + logsave[15] + ", " + "percent -->  " + porcentajeArray[25] * 100 + "(%)\n\n" + //`IA Inspection : ${IAdesition[25] == 0 ? 'Fail' : 'Pass'} ` + "\n\n" +
            // Cuadrante 4
            "Cuadrante :4\n" +
            "TA17-: " + "status :" + logsave[16] + ", " + "percent -->  " + porcentajeArray[17] * 100 + "(%) " + `IA Inspection : ${IAdesition[17] == 0 ? 'Fail' : (IAdesition[17] == 1 ? 'Pass' : 'On hold')} ` + "\n" + //Operador ternario que se interpreta como in else if 
            "TA18-: " + "status :" + logsave[17] + ", " + "percent -->  " + porcentajeArray[18] * 100 + "(%) " + `IA Inspection : ${IAdesition[18] == 0 ? 'Fail' : (IAdesition[18] == 1 ? 'Pass' : 'On hold')} ` + "\n" + //Operador ternario que se interpreta como in else if 
            "TG1-:  " + "status :" + logsave[18] + ", " + "percent -->  " + porcentajeArray[27] * 100 + "(%)\n\n" + //`IA Inspection: ${IAdesition[27] == 0 ? 'Fail' : 'Pass'} ` + "\n\n" +
            // Cuadrante 5 
            "Cuadrante :5\n" +
            "TA15-: " + "status :" + logsave[19] + ", " + "percent -->  " + porcentajeArray[15] * 100 + "(%) " + `IA Inspection : ${IAdesition[15] == 0 ? 'Fail' : (IAdesition[15] == 1 ? 'Pass' : 'On hold')} ` + "\n" + //Operador ternario que se interpreta como in else if 
            "TA16-: " + "status :" + logsave[20] + ", " + "percent -->  " + porcentajeArray[16] * 100 + "(%) " +  `IA Inspection : ${IAdesition[16] == 0 ? 'Fail' : (IAdesition[16] == 1 ? 'Pass' : 'On hold')} ` + "\n" + //Operador ternario que se interpreta como in else if 
            "TA19-: " + "status :" + logsave[21] + ", " + "percent -->  " + porcentajeArray[19] * 100 + "(%) " + `IA Inspection : ${IAdesition[19] == 0 ? 'Fail' : (IAdesition[19] == 1 ? 'Pass' : 'On hold')} ` + "\n" + //Operador ternario que se interpreta como in else if 
            "TA20-: " + "status :" + logsave[22] + ", " + "percent -->  " + porcentajeArray[20] * 100 + "(%) " + `IA Inspection : ${IAdesition[20] == 0 ? 'Fail' : (IAdesition[20] == 1 ? 'Pass' : 'On hold')} ` + "\n" + //Operador ternario que se interpreta como in else if 
            "TB4-:  " + "status :" + logsave[23] + ", " + "percent -->  " + porcentajeArray[26] * 100 + "(%) " + `IA Inspection : ${IAdesition[26] == 0 ? 'Fail' : (IAdesition[26] == 1 ? 'Pass' : 'On hold')} ` + "\n\n" + //Operador ternario que se interpreta como in else if 
            // Cuadrante 6
            "Cuadrante :6\n" +
            "TA13-: " + "status :" + logsave[24] + ", " + "percent -->  " + porcentajeArray[13] * 100 + "(%) " + `IA Inspection : ${IAdesition[13] == 0 ? 'Fail' : (IAdesition[13] == 1 ? 'Pass' : 'On hold')} ` + "\n" + //Operador ternario que se interpreta como in else if 
            "TA14-: " + "status :" + logsave[25] + ", " + "percent -->  " + porcentajeArray[14] * 100 + "(%) " + `IA Inspection : ${IAdesition[14] == 0 ? 'Fail' : (IAdesition[14] == 1 ? 'Pass' : 'On hold')} ` + "\n\n" + //Operador ternario que se interpreta como in else if 
            // Cuadrante 7
            "Cuadrante :7\n" +
            "TA21-: " + "status :" + logsave[26] + ", " + "percent -->  " + porcentajeArray[21] * 100 + "(%) " + `IA Inspection : ${IAdesition[21] == 0 ? 'Fail' : (IAdesition[21] == 1 ? 'Pass' : 'On hold')} ` + "\n" + //Operador ternario que se interpreta como in else if 
            "TC-:   " + "status :" + logsave[27] + ", " + "percent -->  " + porcentajeArray[22] * 100 + "(%) " +  `IA Inspection : ${IAdesition[22] == 0 ? 'Fail' : (IAdesition[22] == 1 ? 'Pass' : 'On hold')} ` + "\n" + //Operador ternario que se interpreta como in else if 
            "#" 


        logsave =
            "" + snr + "&TA1-" + mtxw[1] * 100 + "%" + "," + logsave[1] +
            "&TA2-" + mtxw[2] * 100 + "%" + "," + logsave[2] +//porcentajeArray[2]+"%"+           
            "&TA11-" + mtxw[11] * 100 + "%" + "," + logsave[3] +//porcentajeArray[3]+"%"+
            "&TA12-" + mtxw[12] * 100 + "%" + "," + logsave[4] +//porcentajeArray[4]+
            "&TB1-" + mtxw[23] * 100 + "%" + "," + logsave[5] +//porcentajeArray[5]+
            //Cuadrante 2 
            "&TA3-" + mtxw[3] * 100 + "%" + "," + logsave[6] +//porcentajeArray[6]+
            "&TA4-" + mtxw[4] * 100 + "%" + "," + logsave[7] +//porcentajeArray[7]+
            "&TA9-" + mtxw[9] * 100 + "%" + "," + logsave[8] +//porcentajeArray[8]+
            "&TA10-" + mtxw[10] * 100 + "%" + "," + logsave[9] +//porcentajeArray[9]+
            "&TB2-" + mtxw[24] * 100 + "%" + "," + logsave[10] +//porcentajeArray[10]+
            // Cuadrante3
            "&TA5-" + mtxw[5] * 100 + "%" + "," + logsave[11] +//porcentajeArray[11]+
            "&TA6-" + mtxw[6] * 100 + "%" + "," + logsave[12] +//porcentajeArray[12]+
            "&TA7-" + mtxw[7] * 100 + "%" + "," + logsave[13] +//porcentajeArray[13]+
            "&TA8-" + mtxw[8] * 100 + "%" + "," + logsave[14] +//porcentajeArray[14]+
            "&TB3-" + mtxw[25] * 100 + "%" + "," + logsave[15] +//porcentajeArray[15]+

            "&TA17-" + mtxw[17] * 100 + "%" + "," + logsave[16] +//porcentajeArray[16]+
            "&TA18-" + mtxw[18] * 100 + "%" + "," + logsave[17] +//porcentajeArray[17]+
            "&TG1-" + mtxw[27] * 100 + "%" + "," + logsave[18] +

            "&TA15-" + mtxw[15] * 100 + "%" + "," + logsave[19] +//porcentajeArray[18]+
            "&TA16-" + mtxw[16] * 100 + "%" + "," + logsave[20] +//porcentajeArray[19]+
            "&TA19-" + mtxw[19] * 100 + "%" + "," + logsave[21] +//porcentajeArray[20]+
            "&TA20-" + mtxw[20] * 100 + "%" + "," + logsave[22] +//porcentajeArray[21]+
            "&TB4-" + mtxw[26] * 100 + "%" + "," + logsave[23] +//porcentajeArray[22]+

            "&TA13-" + mtxw[13] * 100 + "%" + "," + logsave[24] +//porcentajeArray[23]+
            "&TA14-" + mtxw[14] * 100 + "%" + "," + logsave[25] +//porcentajeArray[24]+

            "&TA21-" + mtxw[21] * 100 + "%" + "," + logsave[26] +//porcentajeArray[25]+
            "&TC-" + mtxw[22] * 100 + "%" + "," + logsave[27] +//porcentajeArray[26]+"%"+
            "#"

        //logsave=""+snr+"&TA0,1&TA1,1&TA2,1&TA3,1&TA4,1&TA5,1&TA6,1&TA7,1&TA8,1&TA9,1&TA10,1&TA11,1&TA12,1&TA13,1&TA14,1&TA15,1&TA16,1&TA17,1&TA18,1&TA19,1&TA20,1&TA21,1&TA22,1&TA23,1&TA24,1&TA25,1&TA26,0#"
       //console.log("Logsave--"+logsave)

        /*mtxw= 
        ""+snr+"&TA1,"+mtxw[1]*100+"%"+","+logsave[1]+","+
        "&TA2,       "+mtxw[2]*100+"%"+","+logsave[2]+","+
        "&TA11,      "+mtxw[11]*100+"%"+","+logsave[2]+","+
        "&TA11,      "+mtxw[11]*100+"%"+","+logsave[2]+","+*/

        logsaving(snfile, porcentajeArray,logsave)
        
        //console.log(porcentajeArray) 
        //console.log("logsave",logsave)
        socket.emit('plc_response', logsave)
        resolve('resolved')
    })
}
//funcion onload

//----------------Main test sequence----------//

async function testsequence() {
    cuadranteArray = []// Reinicia valor para retrabajar cuadrante
    porcentajeArray = []// Reinicia valor para retrabajar cuadrante
    canbughide()
    for (point = 1; point < 4; point++) {
        await open_cam(point)
        await captureimage()
        await recorTA(point)
        await stopcam()
    }
    await plc_response(logsave)
    if (resultado == true) {
        renombra(snfile)
    }
setTimeout(function fire(){location.reload()},2000);// temporizador para limpiar pantalla
}

function serialnumber (sn){
    return new Promise(async resolve =>{
        elementsn= document.getElementById('sn')
        //console.log(serial)
        elementsn.innerHTML= "Serial: "+sn+""
    resolve('resolved')})
}
function partnumber (pn){
    return new Promise(async resolve =>{
        elementpn = document.getElementById('pn')
        //console.log(model)
        elementpn.innerHTML= "Model: "+pn+""
    resolve('resolved')})
}
function st(st){
    return new Promise(async resolve =>{
        elementst= document.getElementById('st')
        //console.log(station)
        elementst.innerHTML= "Station: "+st+""
    resolve('resolved')})
}


//-----------------Funciones de procesamiento  ( Coordenadas de areas a inspeccionar )-----------//
let camid
let TA1x = 1576
let TA1y = 70
let TA2x = 1418
let TA2y = 74
let TA11x = 1304
let TA11y = 549
let TA12x = 1562
let TA12y = 636
let TA3x = 1044
let TA3y = 81
let TA4x = 726
let TA4y = 54
let TA9x = 770
let TA9y = 562
let TA10x = 1026
let TA10y = 635
let TA5x = 487
let TA5y = 93
let TA6x = 173
let TA6y = 73
let TA7x = 213
let TA7y = 569
let TA8x = 469
let TA8y = 645
let TA18x = 81
let TA18y = 664
let TA17x = 286
let TA17y = 626
let TA16x = 713
let TA16y = 650
let TA15x = 921
let TA15y = 618
let TA19x = 743
let TA19y = 259
let TA20x = 740
let TA20y = 53
let TA14x = 1451
let TA14y = 645
let TA13x = 1673
let TA13y = 619
let TATGx = 152
let TATGy = 150
let TA21x = 487
let TA21y = 222
let TA22x = 611
let TA22y = 131
let TB1x = 1719
let TB1y = 178
let TB2x = 1187
let TB2y = 108
let TB3x = 631
let TB3y = 121
let TB4x = 984
let TB4y = 455

async function recorTA(point) {
    return new Promise(async resolve => {
        switch (point) {
            //TA1
            case 1:
                /**** INICIO DE CAMARA 1 */
                //Cuadrante 1
                //TA1
                contextcanvasClen1.drawImage(fullimage, TA1x, TA1y, 118, 299, 0, 0, contextcanvasClen1.canvas.width, contextcanvasClen1.canvas.height)                                               
                        for (var i = 0; i < localStorage.length; i++) {
                            var clave = localStorage.key(i);
                            var valor = localStorage.getItem(clave);
                            valores.push({ clave: clave, valor: valor });
                            if(valor === 'ta1' || valor === 'TA1'){console.log('Recolectando datos de TA1');
                                rgbauto(canvasClen1)  }
                                
                        }
                            // Mostrar los valores almacenados
                            //console.log(valores);
                await Analiza(canvasClen1, 1)
                logresult(1, statusx)

                //TA2
                contextcanvasGlen1.drawImage(fullimage, TA2x, TA2y, 118, 330, 0, 0, contextcanvasGlen1.canvas.width, contextcanvasGlen1.canvas.height)
                        for (var i = 0; i < localStorage.length; i++) {
                            var clave = localStorage.key(i);
                            var valor = localStorage.getItem(clave);
                            valores.push({ clave: clave, valor: valor });
                            if(valor === 'ta2' || valor === 'TA2'){console.log('Recolectando datos de TA2');
                                rgbauto(canvasGlen1)}
                        }
                await Analiza(canvasGlen1, 2)
                logresult(2, statusx)

                //TA11
                contextcanvasGlen1.drawImage(fullimage, TA11x, TA11y, 118, 330, 0, 0, contextcanvasGlen1.canvas.width, contextcanvasGlen1.canvas.height)
                        for (var i = 0; i < localStorage.length; i++) {
                            var clave = localStorage.key(i);
                            var valor = localStorage.getItem(clave);
                            valores.push({ clave: clave, valor: valor });
                            if(valor === 'ta11' || valor === 'TA11'){console.log('Recolectando datos de TA11');
                                rgbauto(canvasGlen1)}
                        }
                await Analiza(canvasGlen1, 11)
                logresult(3, statusx)

                //TA12
                contextcanvasMlen1.drawImage(fullimage, TA12x, TA12y, 118, 312, 0, 0, contextcanvasMlen1.canvas.width, contextcanvasMlen1.canvas.height)
                        for (var i = 0; i < localStorage.length; i++) {
                            var clave = localStorage.key(i);
                            var valor = localStorage.getItem(clave);
                            valores.push({ clave: clave, valor: valor });
                            if(valor === 'ta12' || valor === 'TA12'){console.log('Recolectando datos de TA12');
                                rgbauto(canvasMlen1)}
                        }
                await Analiza(canvasMlen1, 12)
                logresult(4, statusx)

                //TB1
                contextcanvasTB.drawImage(fullimage, TB1x, TB1y, 34, 52, 0, 0, contextcanvasTB.canvas.width, contextcanvasTB.canvas.height)
                        for (var i = 0; i < localStorage.length; i++) {
                            var clave = localStorage.key(i);
                            var valor = localStorage.getItem(clave);
                            valores.push({ clave: clave, valor: valor });
                            if(valor === 'tb1' || valor === 'TB1'){console.log('Recolectando datos de TB1');
                                rgbauto(canvasTB)}
                        }
                await Analiza(canvasTB, 23)
                logresult(5, statusx)
                await Evaluacion(1)
                contextcanvasnuevo.drawImage(fullimage, 1243, 35, 519, 934, 0, 0, contextcanvasnuevo.canvas.width, contextcanvasnuevo.canvas.height) // Canvas donde con imagen vertical original 
                contextcanvaspaste.translate(934, 519) //Punto donde va comenzar a realizar la translacion de la imagen
                contextcanvaspaste.rotate(270 * Math.PI / 180) //Formula para convertir el angulo en radianes
                contextcanvaspaste.drawImage(canvasnuevo, 0, 0, 1868, 1038, -400, -519, 1868, 1038) // Canvas donde se coloca la imagen ya rotada 
                //Imagen rotada C1
                contextcanvasCamara.drawImage(canvaspaste, 415, 400, 934, 515, 0, 0, 935, 518) //REcorte de primer cuadrante tomada de fullimag
                await pause()

                //Cuadrante 2 , imagen 1
                //TA3
                contextcanvasClen1.drawImage(fullimage, TA3x, TA3y, 118, 299, 0, 0, contextcanvasClen1.canvas.width, contextcanvasClen1.canvas.height)
                        for (var i = 0; i < localStorage.length; i++) {
                            var clave = localStorage.key(i);
                            var valor = localStorage.getItem(clave);
                            valores.push({ clave: clave, valor: valor });
                            if(valor === 'ta3' || valor === 'TA3'){console.log('Recolectando datos de TA3');
                                rgbauto(canvasClen1)}
                        }
                await Analiza(canvasClen1, 3)
                logresult(6, statusx)

                //TA4
                contextcanvasGlen1.drawImage(fullimage, TA4x, TA4y, 118, 330, 0, 0, contextcanvasGlen1.canvas.width, contextcanvasGlen1.canvas.height)
                        for (var i = 0; i < localStorage.length; i++) {
                            var clave = localStorage.key(i);
                            var valor = localStorage.getItem(clave);
                            valores.push({ clave: clave, valor: valor });
                            if(valor === 'ta4' || valor === 'TA4'){console.log('Recolectando datos de TA4');
                                rgbauto(canvasGlen1)}
                        }
                await Analiza(canvasGlen1, 4)
                logresult(7, statusx)

                //TA9
                contextcanvasGlen1.drawImage(fullimage, TA9x, TA9y, 118, 330, 0, 0, contextcanvasGlen1.canvas.width, contextcanvasGlen1.canvas.height)
                        for (var i = 0; i < localStorage.length; i++) {
                            var clave = localStorage.key(i);
                            var valor = localStorage.getItem(clave);
                            valores.push({ clave: clave, valor: valor });
                            if(valor === 'ta9' || valor === 'TA9'){console.log('Recolectando datos de TA9');
                                rgbauto(canvasGlen1)}
                        }
                await Analiza(canvasGlen1, 9)
                logresult(8, statusx)

                //TA10
                contextcanvasGlen1.drawImage(fullimage, TA10x, TA10y, 118, 330, 0, 0, contextcanvasGlen1.canvas.width, contextcanvasGlen1.canvas.height)
                        for (var i = 0; i < localStorage.length; i++) {
                            var clave = localStorage.key(i);
                            var valor = localStorage.getItem(clave);
                            valores.push({ clave: clave, valor: valor });
                            if(valor === 'ta10' || valor === 'TA10'){console.log('Recolectando datos de TA10');
                                rgbauto(canvasGlen1)}
                        }
                await Analiza(canvasGlen1, 10)
                logresult(9, statusx)

                //TB2
                contextcanvasTB.drawImage(fullimage, TB2x, TB2y, 34, 52, 0, 0, contextcanvasTB.canvas.width, contextcanvasTB.canvas.height)
                        for (var i = 0; i < localStorage.length; i++) {
                            var clave = localStorage.key(i);
                            var valor = localStorage.getItem(clave);
                            valores.push({ clave: clave, valor: valor });
                            if(valor === 'tb2'|| valor === 'TB2'){console.log('Recolectando datos de TB2');
                                rgbauto(canvasTB)}
                        }
                await Analiza(canvasTB, 24)
                logresult(10, statusx)
                await Evaluacion(2)
                // Puntos de rotacion 
                contextcanvasnuevo.drawImage(fullimage, 705, 29, 519, 934, 0, 0, contextcanvasnuevo.canvas.width, contextcanvasnuevo.canvas.height) // Canvas donde con imagen vertical original 
                contextcanvaspasteC2.translate(934, 519) //Punto donde va comenzar a realizar la translacion de la imagen
                contextcanvaspasteC2.rotate(270 * Math.PI / 180) //Formula para convertir el angulo en radianes
                contextcanvaspasteC2.drawImage(canvasnuevo, 0, 0, 1868, 1038, -400, -519, 1868, 1038) // Canvas donde se coloca la imagen ya rotada
                //Imagen rotada C2
                contextcanvasCamara.drawImage(canvaspasteC2, 415, 400, 934, 515, 0, 0, 935, 518) //REcorte de primer cuadrante tomada de fullimag
                setTimeout(function fire() { resolve('resolved'); }, 5000);
                await pause()

                //TA5
                contextcanvasClen1.drawImage(fullimage, TA5x, TA5y, 118, 299, 0, 0, contextcanvasClen1.canvas.width, contextcanvasClen1.canvas.height)
                        for (var i = 0; i < localStorage.length; i++) {
                            var clave = localStorage.key(i);
                            var valor = localStorage.getItem(clave);
                            valores.push({ clave: clave, valor: valor });
                            if(valor === 'ta5'  || valor === 'TA5'){console.log('Recolectando datos de TA5');
                                rgbauto(canvasClen1)}
                        }
                await Analiza(canvasClen1, 5)
                logresult(11, statusx)
                //TA6
                contextcanvasTA6len1.drawImage(fullimage, TA6x, TA6y, 115, 322, 0, 0, contextcanvasTA6len1.canvas.width, contextcanvasTA6len1.canvas.height)
                        for (var i = 0; i < localStorage.length; i++) {
                            var clave = localStorage.key(i);
                            var valor = localStorage.getItem(clave);
                            valores.push({ clave: clave, valor: valor });
                            if(valor === 'ta6'|| valor === 'TA6'){console.log('Recolectando datos de TA6');
                                rgbauto(canvasTA6len1)}
                        }
                await Analiza(canvasTA6len1, 6)
                logresult(12, statusx)
                //TA7
                contextcanvasGlen1.drawImage(fullimage, TA7x, TA7y, 118, 330, 0, 0, contextcanvasGlen1.canvas.width, contextcanvasGlen1.canvas.height)
                        for (var i = 0; i < localStorage.length; i++) {
                            var clave = localStorage.key(i);
                            var valor = localStorage.getItem(clave);
                            valores.push({ clave: clave, valor: valor });
                            if(valor === 'ta7' || valor === 'TA7'){console.log('Recolectando datos de TA7');
                                rgbauto(canvasGlen1)}
                        }
                await Analiza(canvasGlen1,7)
                logresult(13, statusx)
                //TA8
                contextcanvasGlen1.drawImage(fullimage, TA8x, TA8y, 118, 330, 0, 0, contextcanvasGlen1.canvas.width, contextcanvasGlen1.canvas.height)
                        for (var i = 0; i < localStorage.length; i++) {
                            var clave = localStorage.key(i);
                            var valor = localStorage.getItem(clave);
                            valores.push({ clave: clave, valor: valor });
                            if(valor === 'ta8' || valor === 'TA8'){console.log('Recolectando datos de TA8');
                                rgbauto(canvasGlen1)}
                        }
                await Analiza(canvasGlen1, 8)
                logresult(14, statusx)

                //TB3
                contextcanvasTB.drawImage(fullimage, TB3x, TB3y, 34, 52, 0, 0, contextcanvasTB.canvas.width, contextcanvasTB.canvas.height)
                        for (var i = 0; i < localStorage.length; i++) {
                            var clave = localStorage.key(i);
                            var valor = localStorage.getItem(clave);
                            valores.push({ clave: clave, valor: valor });
                            if(valor === 'tb3' || valor === 'TB3'){console.log('Recolectando datos de TB3');
                                rgbauto(canvasTB)}
                        }
                await Analiza(canvasTB, 25)
                logresult(15, statusx)
                await Evaluacion(3)
                // Puntos de rotacion 
                contextcanvasnuevo.drawImage(fullimage, 162, 48, 519, 934, 0, 0, contextcanvasnuevo.canvas.width, contextcanvasnuevo.canvas.height) // Canvas donde con imagen vertical original 
                contextcanvaspasteC3.translate(934, 519) //Punto donde va comenzar a realizar la translacion de la imagen
                contextcanvaspasteC3.rotate(270 * Math.PI / 180) //Formula para convertir el angulo en radianes
                contextcanvaspasteC3.drawImage(canvasnuevo, 0, 0, 1868, 1038, -400, -519, 1868, 1038) // Canvas donde se coloca la imagen ya rotada 
                //Imagen rotada 
                contextcanvasCamara.drawImage(canvaspasteC3, 415, 400, 934, 515, 0, 0, 935, 518) //REcorte de primer cuadrante tomada de fullimag
                resolve('resolved')
                await snapshot(point)
                await pause()
                break
            //***INICIO CUADRANTE 2** */
            case 2:
                //TA18
                contextcanvasClen2.drawImage(fullimage, TA18x, TA18y, 149, 378, 0, 0, contextcanvasClen2.canvas.width, contextcanvasClen2.canvas.height)
                        for (var i = 0; i < localStorage.length; i++) {
                            var clave = localStorage.key(i);
                            var valor = localStorage.getItem(clave);
                            valores.push({ clave: clave, valor: valor });
                            if(valor === 'ta18'|| valor === 'TA18'){console.log('Recolectando datos de TA18');
                                rgbauto(canvasClen2)}
                        }
                await Analiza(canvasClen2, 18)
                logresult(16, statusx)
                 //TA17       
                contextcanvasClen2.drawImage(fullimage, TA17x, TA17y, 149, 378, 0, 0, contextcanvasClen2.canvas.width, contextcanvasClen2.canvas.height)
                        for (var i = 0; i < localStorage.length; i++) {
                            var clave = localStorage.key(i);
                            var valor = localStorage.getItem(clave);
                            valores.push({ clave: clave, valor: valor });
                            if(valor === 'ta17'|| valor === 'TA17'){console.log('Recolectando datos de TA17');
                                rgbauto(canvasClen2)}
                        }
                await Analiza(canvasClen2, 17)
                logresult(17, statusx)

                //contextcanvasTGlen2.drawImage(fullimage, TATGx, TATGy, 298, 99, 0, 0, contextcanvasTGlen2.canvas.width, contextcanvasTGlen2.canvas.height)
                await Analiza(canvasTGlen2, 27)
                logresult(18, statusx)
                await Evaluacion(4)
                contextcanvasnuevo4.drawImage(fullimage, 5, 1, 605, 1069, 0, 0, contextcanvasnuevo4.canvas.width, contextcanvasnuevo4.canvas.height) // Canvas donde con imagen vertical original 
                contextcanvaspaste4.translate(1069, 605) //Punto donde va comenzar a realizar la translacion de la imagen
                contextcanvaspaste4.rotate(270 * Math.PI / 180) //Formula para convertir el angulo en radianes
                contextcanvaspaste4.drawImage(canvasnuevo4, 0, 0, 1677, 2138, -1069, -605, 1677, 2138) // Canvas donde se coloca la imagen ya rotada 
                // Imagen rotada
                contextcanvasCamara.drawImage(canvaspaste4, 464, 1069, 1067, 605, 0, 0, 935, 518) //REcorte de primer cuadrante tomada de fullimag
                await pause()

                //TA16
                contextcanvasClen2.drawImage(fullimage, TA16x, TA16y, 149, 378, 0, 0, contextcanvasClen2.canvas.width, contextcanvasClen2.canvas.height)
                        for (var i = 0; i < localStorage.length; i++) {
                            var clave = localStorage.key(i);
                            var valor = localStorage.getItem(clave);
                            valores.push({ clave: clave, valor: valor });
                            if(valor === 'ta16' || valor === 'TA16'){console.log('Recolectando datos de TA16');
                                rgbauto(canvasClen2)}
                        }
                await Analiza(canvasClen2, 16)
                logresult(19, statusx)

                //TA15
                contextcanvasClen2.drawImage(fullimage, TA15x, TA15y, 149, 378, 0, 0, contextcanvasClen2.canvas.width, contextcanvasClen2.canvas.height)
                        for (var i = 0; i < localStorage.length; i++) {
                            var clave = localStorage.key(i);
                            var valor = localStorage.getItem(clave);
                            valores.push({ clave: clave, valor: valor });
                            if(valor === 'ta15'|| valor === 'TA15'){console.log('Recolectando datos de TA15');
                                rgbauto(canvasClen2)}
                        }
                await Analiza(canvasClen2, 15)
                logresult(20, statusx)

                //TA19
                contextcanvasGlen2.drawImage(fullimage, TA19x, TA19y, 418, 149, 0, 0, contextcanvasGlen2.canvas.width, contextcanvasGlen2.canvas.height)
                        for (var i = 0; i < localStorage.length; i++) {
                            var clave = localStorage.key(i);
                            var valor = localStorage.getItem(clave);
                            valores.push({ clave: clave, valor: valor });
                            if(valor === 'ta19' || valor === 'TA19'){console.log('Recolectando datos de TA19');
                                rgbauto(canvasGlen2)}
                        }
                await Analiza(canvasGlen2, 19)
                logresult(21, statusx)

                //TA20
                contextcanvasGlen2.drawImage(fullimage, TA20x, TA20y, 418, 149, 0, 0, contextcanvasGlen2.canvas.width, contextcanvasGlen2.canvas.height)
                        for (var i = 0; i < localStorage.length; i++) {
                            var clave = localStorage.key(i);
                            var valor = localStorage.getItem(clave);
                            valores.push({ clave: clave, valor: valor });
                            if(valor === 'ta20' || valor === 'TA20'){console.log('Recolectando datos de TA20');
                                rgbauto(canvasGlen2)}
                        }
                await Analiza(canvasGlen2, 20)
                logresult(22, statusx)

                //TB4
                contextcanvasTB4.drawImage(fullimage, TB4x, TB4y, 66, 52, 0, 0, contextcanvasTB4.canvas.width, contextcanvasTB4.canvas.height)
                        for (var i = 0; i < localStorage.length; i++) {
                            var clave = localStorage.key(i);
                            var valor = localStorage.getItem(clave);
                            valores.push({ clave: clave, valor: valor });
                            if(valor === 'tb4' || valor === 'TB4'){console.log('Recolectando datos de TB4');
                                rgbauto(canvasTB4)}
                        }
                await Analiza(canvasTB4, 26)
                logresult(23, statusx)
                await Evaluacion(5)
                contextcanvasnuevo4.drawImage(fullimage, 695, 3, 605, 1069, 0, 0, contextcanvasnuevo4.canvas.width, contextcanvasnuevo4.canvas.height) // Canvas donde con imagen vertical original 
                contextcanvaspasteC5.translate(1069, 605) //Punto donde va comenzar a realizar la translacion de la imagen
                contextcanvaspasteC5.rotate(270 * Math.PI / 180) //Formula para convertir el angulo en radianes
                contextcanvaspasteC5.drawImage(canvasnuevo4, 0, 0, 1677, 2138, -1069, -605, 1677, 2138) // Canvas donde se coloca la imagen ya rotada 
                //Imagen rotada
                contextcanvasCamara.drawImage(canvaspasteC5, 464, 1069, 1067, 605, 0, 0, 935, 518) //REcorte de primer cuadrante tomada de fullimag
                await pause()

                //TA14
                contextcanvasClen2.drawImage(fullimage, TA14x, TA14y, 149, 378, 0, 0, contextcanvasClen2.canvas.width, contextcanvasClen2.canvas.height)
                        for (var i = 0; i < localStorage.length; i++) {
                            var clave = localStorage.key(i);
                            var valor = localStorage.getItem(clave);
                            valores.push({ clave: clave, valor: valor });
                            if(valor === 'ta14' || valor === 'TA14'){console.log('Recolectando datos de TA14');
                                rgbauto(canvasGlen2)}
                        } 
                await Analiza(canvasGlen2, 14)
                logresult(24, statusx)

                //TA13
                contextcanvasClen2.drawImage(fullimage, TA13x, TA13y, 149, 378, 0, 0, contextcanvasClen2.canvas.width, contextcanvasClen2.canvas.height)
                        for (var i = 0; i < localStorage.length; i++) {
                            var clave = localStorage.key(i);
                            var valor = localStorage.getItem(clave);
                            valores.push({ clave: clave, valor: valor });
                            if(valor === 'ta13' || valor === 'TA13'){console.log('Recolectando datos de TA13');
                                rgbauto(canvasClen2)}
                        } 
                await Analiza(canvasClen2, 13)
                logresult(25, statusx)
                await Evaluacion(6)
                contextcanvasnuevo4.drawImage(fullimage, 1315, 7, 605, 1069, 0, 0, contextcanvasnuevo4.canvas.width, contextcanvasnuevo4.canvas.height) // Canvas donde con imagen vertical original 
                contextcanvaspasteC6.translate(1069, 605) //Punto donde va comenzar a realizar la translacion de la imagen
                contextcanvaspasteC6.rotate(270 * Math.PI / 180) //Formula para convertir el angulo en radianes
                contextcanvaspasteC6.drawImage(canvasnuevo4, 0, 0, 1677, 2138, -1069, -605, 1677, 2138) // Canvas donde se coloca la imagen ya rotada 
                //Imagen rotada 
                contextcanvasCamara.drawImage(canvaspasteC6, 464, 1069, 1067, 605, 0, 0, 935, 518) //REcorte de primer cuadrante tomada de fullimag
                resolve('resolved')
                await snapshot(point)
                await pause()
                break
            /** INICIO CUADRANTE 3 */
            case 3:
                contextcanvasflalen3.drawImage(fullimage, TA21x, TA21y, 61, 555, 0, 0, contextcanvasflalen3.canvas.width, contextcanvasflalen3.canvas.height)
                        for (var i = 0; i < localStorage.length; i++) {
                            var clave = localStorage.key(i);
                            var valor = localStorage.getItem(clave);
                            valores.push({ clave: clave, valor: valor });
                            if(valor === 'ta21' || valor === 'TA21'){console.log('Recolectando datos de TA21');
                                rgbauto(canvasflalen3)}
                        } 
                await Analiza(canvasflalen3, 21)
                logresult(26, statusx)

                contextcanvasgolen3.drawImage(fullimage, TA22x, TA22y, 429, 795, 0, 0, contextcanvasgolen3.canvas.width, contextcanvasgolen3.canvas.height)
                        for (var i = 0; i < localStorage.length; i++) {
                            var clave = localStorage.key(i);
                            var valor = localStorage.getItem(clave);
                            valores.push({ clave: clave, valor: valor });
                            if(valor === 'ta22' || valor === 'TA22'){console.log('Recolectando datos de TA22');
                                rgbauto(canvasgolen3)}
                        } 
                await Analiza(canvasgolen3, 22)
                logresult(27, statusx)
                await Evaluacion(7)
                contextcanvasnuevo7.drawImage(fullimage, 297, 8, 986, 1066, 0, 0, contextcanvasnuevo7.canvas.width, contextcanvasnuevo7.canvas.height) // Canvas donde con imagen vertical original 
                contextcanvaspaste7.translate(1066, 986) //Punto donde va comenzar a realizar la translacion de la imagen
                contextcanvaspaste7.rotate(270 * Math.PI / 180) //Formula para convertir el angulo en radianes
                contextcanvaspaste7.drawImage(canvasnuevo7, 0, 0, 1974, 2134, -1067, -987, 1974, 2134) // Canvas donde se coloca la imagen ya rotada 
                //Imagen rotada
                contextcanvasCamara.drawImage(fullimage, 0, 0, 1920, 1080, 0, 0, 935, 518) //REcorte de primer cuadrante tomada de fullimag
                resolve('resolved')
                await snapshot(point)
                break
            default:

        }
    })
}

async function pause() {
    return new Promise(async resolve => {
        setTimeout(function pausea() { resolve('resolved') }, 3000)
    });
}

function Analiza(canvasx, point) {
    return new Promise(async resolve => {

        let contextcanvasx = canvasx.getContext('2d')
        let cdata = contextcanvasx.getImageData(0, 0, canvasx.width, canvasx.height);

        let malo = 0, bueno = 0
        //Valores del rojo
        let rmin
        let rmax
        //Valores del verde
        let gmin
        let gmax
        //Valores del azul
        let bmin
        let bmax
        if (point == 1) {
            if (!localStorage.getItem('TA1')) {
                // El código que se ejecuta si la variable no existe
                console.log('TA1 con datos de ultima calibracion')
                replaceTA1MaxR = localStorage.getItem('replaceTA1MaxR');replaceTA1MinR = localStorage.getItem('replaceTA1MinR')
                replaceTA1MaxG = localStorage.getItem('replaceTA1MaxG'); replaceTA1MinG = localStorage.getItem('replaceTA1MinG')
                replaceTA1MinB = localStorage.getItem('replaceTA1MinB');replaceTA1MaxB = localStorage.getItem('replaceTA1MaxB')
                //rmin = replaceTA1MinR, rmax = replaceTA1MaxR, gmin = replaceTA1MinG, gmax = replaceTA1MaxG, bmin = replaceTA1MinB, bmax = replaceTA1MaxB, criterio = .90, latapona = 0
                rmin = 79, rmax = 149, gmin = 91, gmax = 159, bmin = 107, bmax = 184, criterio = .98, latapona = 15000
                console.log('Calis de RMax',rmax)
                }else if(localStorage.getItem('TA1') === 'ta1' && localStorage.getItem('valorMaximoRFinal') != null) {
                        console.log('datos de localStorage', localStorage.getItem('variable1'))
                        valorMaxRFinal = localStorage.getItem('valorMaximoRFinal');  localStorage.setItem('replaceTA1MaxR',valorMaxRFinal);   
                        valorminRFinal = localStorage.getItem('valorminRFinal');     localStorage.setItem('replaceTA1MinR',valorminRFinal);   
                        valorMaxGFinal = localStorage.getItem('valorMaximoGFinal');  localStorage.setItem('replaceTA1MaxG',valorMaxGFinal);   
                        valorMinGFinal = localStorage.getItem('valorMinGFinal');     localStorage.setItem('replaceTA1MinG',valorMinGFinal);  
                        valorMaXBFinal = localStorage.getItem('valorMaximoBFinal');  localStorage.setItem('replaceTA1MinB',valorMaXBFinal);  
                        valorMinBFinal = localStorage.getItem('valorMinBFinal');     localStorage.setItem('replaceTA1MaxB',valorMinBFinal);   
                        rmin = replaceTA1MinR, rmax = replaceTA1MaxR, gmin = replaceTA1MinG, gmax = replaceTA1MaxG, bmin = replaceTA1MinB, bmax =replaceTA1MaxB, criterio = .950, latapona = 0
                        console.log('Se cumplio la condicion Valores recalibrados',valorminRFinal,valorMaxRFinal,valorMinGFinal,valorMaxGFinal,valorMinBFinal,valorMaXBFinal)
                    }else{
                        console.log('Valores finales aun no estan completados, se usaran valores estandar')
                        rmin = 79, rmax = 143, gmin = 91, gmax = 153, bmin = 107, bmax = 181, criterio = .98, latapona = 10000
                        //rmin = replaceTA1MinR, rmax = replaceTA1MaxR, gmin = replaceTA1MinG, gmax = replaceTA1MaxG, bmin = replaceTA1MinB, bmax = replaceTA1MaxB, criterio = .950, latapona = 100000 
                    }console.log('Valor final de rmin ',rmin)}
        if (point == 2) { 
            if(!localStorage.getItem('TA2')){
                //console.log('La variable "variable1 no existe en localStorage')
                rmin = 96, rmax = 141, gmin = 110, gmax = 153, bmin = 119, bmax = 177, criterio = .90, latapona = 10000
            }else if(localStorage.getItem('TA2') === 'ta2' && localStorage.getItem('valorMaximoRFinal') != null){
                console.log('datos de localStorage', localStorage.getItem('variable1'))
                valorMaxRFinalTA1 = localStorage.getItem('valorMaximoRFinal')                       
                        valorminRFinalTA2 = localStorage.getItem('valorminRFinal')
                        valorMaxGFinalTA2 = localStorage.getItem('valorMaximoGFinal')
                        valorMinGFinalTA2 =  localStorage.getItem('valorMinGFinal')
                        valorMaXBFinalTA2 = localStorage.getItem('valorMaximoBFinal')
                        valorMinBFinalTA2 = localStorage.getItem('valorMinBFinal')
                        rmin = valorminRFinalTA2, rmax = valorMaxRFinalTA2, gmin = valorMinGFinalTA2, gmax = valorMaxGFinalTA2, bmin = valorMinBFinalTA2, bmax =valorMaXBFinalTA2, criterio = .950, latapona = 0
                        console.log('Se cumplio la condicion Valores recalibrados',valorminRFinalTA1,valorMaxRFinalTA1,valorMinGFinalTA1,valorMaxGFinalTA1,valorMinBFinalTA1,valorMaXBFinalTA1)
                }else{
                    rmin = 96, rmax = 141, gmin = 110, gmax = 153, bmin = 119, bmax = 177, criterio = .950, latapona = 15000
                    //console.log('Valores finales aun no estan completados, se usaran valores estandar')
                }}   
        if (point == 11) { rmin = 98, rmax = 194, gmin = 112, gmax = 220, bmin = 135, bmax = 255, criterio = .950, latapona = 2000}  // TA11 Actualizado * 
        if (point == 12) { rmin = 90, rmax = 178, gmin = 103, gmax = 193, bmin = 124, bmax = 231, criterio = .950, latapona = 2000} // TA12 Actualizado
        if (point == 23) { rmin = 100, rmax = 190, gmin = 110, gmax = 200, bmin = 100, bmax = 200, criterio = .950, latapona = 10000} //TB1
        // Cuadrante 2
        if (point == 3) { rmin = 74, rmax = 178, gmin = 88, gmax = 200, bmin = 105, bmax = 235, criterio = .950, latapona = 2500} // TA3 Actualizado
        if (point == 4) { rmin = 89, rmax = 156, gmin = 98, gmax = 165, bmin = 115, bmax = 194, criterio = .950, latapona = 5500}  // TA4 Actualizado
        if (point == 9) { rmin = 88, rmax = 196, gmin = 107, gmax = 220, bmin = 127, bmax = 255, criterio = .950, latapona = 4500}  // TA9 Actuaalizado
        if (point == 10) { rmin = 90, rmax = 203, gmin = 93, gmax = 230, bmin = 110, bmax = 255, criterio = .950, latapona = 2000 } // 8500 TA10 Actualizado *
        if (point == 24) { rmin = 110, rmax = 180, gmin = 125, gmax = 180, bmin = 110, bmax = 170, criterio = .950, latapona = 11400 }  //TB2
        // Cuadrante 3 80, 180
        if (point == 5) { rmin = 81, rmax = 200, gmin = 93, gmax = 227, bmin = 117, bmax = 255, criterio = .950, latapona = 2000 }  // TA5 Actualizado * No tapona
        if (point == 6) { rmin = 96, rmax = 211, gmin = 101, gmax = 231, bmin = 125, bmax = 255, criterio = .950, latapona = 2000   
             }  // 4000 TA6 Actualizado  *
        if (point == 7) { rmin = 106, rmax = 227, gmin = 115, gmax = 255, bmin = 140, bmax = 255, criterio = .950, latapona = 2500 }  // TA7 Actualizado * Canvande
        if (point == 8) { rmin = 83, rmax = 201, gmin = 84, gmax = 218, bmin = 104, bmax = 255, criterio = .950, latapona = 2000 } // TA8 Actualizado  *
        if (point == 25) { rmin = 100, rmax = 190, gmin = 110, gmax = 215, bmin = 100, bmax = 190, criterio = .950, latapona = 10500 } //TB3
        // Cuadrante 4
        if (point == 17) { rmin = 94, rmax = 178, gmin = 118, gmax = 226, bmin = 131, bmax = 249, criterio = .950, latapona = 2000} // TA17 Actualizado 
        if (point == 18) { rmin = 88, rmax = 152, gmin = 121, gmax = 186, bmin = 134, bmax = 217, criterio = .950, latapona = 2000} // TA18 Actualizado
        if (point == 27) { rmin = 90, rmax = 205, gmin = 105, gmax = 235, bmin = 90, bmax = 210, criterio = .950, latapona = 5000000 } // TA27 
        // Cuadrante 5
        if (point == 15) { rmin = 105, rmax = 218, gmin = 130, gmax = 255, bmin = 150, bmax = 255, criterio = .950, latapona = 2100 } // TA15 Actualizado
        if (point == 16) { rmin = 74, rmax = 194, gmin = 104, gmax = 235, bmin = 118, bmax = 255, criterio = .950, latapona = 1500 } // TA16 Actualizado
        if (point == 19) { rmin = 75, rmax = 182, gmin = 115, gmax = 216, bmin = 131, bmax = 246, criterio = .950, latapona = 2000 } // TA19 Actualizado
        if (point == 20) { rmin = 100, rmax = 148, gmin = 119, gmax = 184, bmin = 134, bmax = 210, criterio = .950, latapona = 1300} // TA20 Actualizado
        if (point == 26) { rmin = 110, rmax = 165, gmin = 130, gmax = 205, bmin = 145, bmax = 223, criterio = .950, latapona = 58000 } //TB4
        // Cuadrante 6
        if (point == 13) { rmin = 80, rmax = 216, gmin = 117, gmax = 253, bmin = 131, bmax = 255, criterio = .950, latapona = 10800 } // TA13  Actualizado *
        if (point == 14) { rmin =63, rmax = 226, gmin = 90, gmax = 255, bmin = 102, bmax = 255, criterio = .950, latapona = 5000 } // TA14 Actualizado *
        // Cuadrante 7
        if (point == 21) { rmin = 52, rmax = 183, gmin = 56, gmax = 195, bmin = 68, bmax = 232, criterio = .950, latapona = 10000 } // TA21 Rectangulo ch
        if (point == 22) { rmin = 82, rmax = 210, gmin = 90, gmax = 245, bmin = 95, bmax = 250, criterio = .950, latapona = 200} // TA22 Actualizado

        //Recorrido de asignacion de pixel en el recorte de cada TA asignandoselo a una variable 
        for (let i = 0; i < cdata.data.length; i += 4) { //cdata.data.length
            // Matriz para valores 
            R = cdata.data[i + 0]
            G = cdata.data[i + 1]
            B = cdata.data[i + 2]
            A = cdata.data[i + 3]
           // console.log(cdata)
            //console.log(`Pixn: ${ i / 4 }:-->`, R,G,B,A)

            if (((R > rmin) && (R < rmax)) && ((G > gmin) && (G < gmax)) && ((B > bmin) && (B < bmax))) {// condicion para verificar cada pixel
                bueno++
            } else {
                malo++  // Matriz que pinta de color rojo sino se cumple la condicion anterior 
                cdata.data[i + 0] = 255
                cdata.data[i + 1] = 0
                cdata.data[i + 2] = 0
                cdata.data[i + 3] = 255
            }//End Else
        }// End For

        contextcanvasx.putImageData(cdata, 0, 0)// Dibuja los pixeles rojos encontrados 
        await limpiaR(point, canvasx) // Se manda llamar limpiar ruido
        let pixitotal = (cdata.data.length) / 4 // cuenta todos los pixeles del canvas bajo analisis 
        
       if (point == 20) { //Imprime por cada punto el conteo de pixeles
            console.log("Buenos: " + bueno)
            console.log("******")
            console.log("Malo: " + malo)
            console.log("Ruido:" + ruido)
            console.log("Finetuning:" + latapona)
            console.log("******")
            console.log("Pix Total:"+pixitotal)
            }
        //console.log("Pix Total:"+pixitotal)

        let pixibuenos = bueno + ruido + latapona
        //console.log("Pixibuenos", pixibuenos)
        let porcentajebueno = pixibuenos / pixitotal
        console.log(porcentajebueno)
        /*let percent = pixitotal - ruido
        console.log("cobertura: " + percent + " pixeles restantes de ruido ")
        cobertura = pixitotal * percent
        console.log("cobertura en porcentaje :" + cobertura / 100 + "(%)")*/

        porcentajefinal = porcentajebueno.toFixed(4); //|| porcentajefinal > .90
        if ( porcentajefinal > .950 ) { 
            porcentajefinal = 1 
        }
        // porcentajeArray[point] = porcentajefinal
       // mtxw[point] = porcentajefinal
        //console.log("Porcentajefinal: ",porcentajefinal)
        //porcentajeArray[point]= porcentajefinal
        //console.log("Pix Total:"+pixitotal)
        //console.log(pixibuenos)

        if (porcentajebueno > criterio || porcentajebueno >= criterio2) {
            statusx = "1"
            IAdesition[point]= " On hold "
            
            //Recolector de muestras buenas sin IA
            /*if( point == 1 || point == 2 || point == 3 || point == 4 || point == 5 || point == 6  ||  point == 7 || point == 8  || point == 9 || point == 10  || point == 11 || point == 12 ||  point == 13 || point == 14 || point == 15 ||  point == 16 ||   point == 17 || point == 18 || point == 19 || point == 20 || point == 21 || point == 22 || point == 23 || point == 24 || point == 25|| point == 26 ){
                await recolectabuenos(point)
            }*/
            
            if(((point == 1) && (statusx == '1'))){ta1statusgood = 'pass'; logubications(1,ta1statusgood)} 
            if(((point == 2) && (statusx == '1'))){ta2statusgood = 'pass'; logubications(2,ta2statusgood)} 
            if(((point == 3) && (statusx == '1'))){ta3statusgood = 'pass'; logubications(3,ta3statusgood)} 
            if(((point == 4) && (statusx == '1'))){ta4statusgood = 'pass'; logubications(4,ta4statusgood)} 
            if(((point == 5) && (statusx == '1'))){ta5statusgood = 'pass'; logubications(5,ta5statusgood)} 
            if(((point == 6) && (statusx == '1'))){ta6statusgood = 'pass'; logubications(6,ta6statusgood)} 
            if(((point == 7) && (statusx == '1'))){ta7statusgood = 'pass'; logubications(7,ta7statusgood)} 
            if(((point == 8) && (statusx == '1'))){ta8statusgood = 'pass'; logubications(8,ta8statusgood)} 
            if(((point == 9) && (statusx == '1'))){ta9statusgood = 'pass'; logubications(9,ta9statusgood)} 
            if(((point == 10) && (statusx == '1'))){ta10statusgood = 'pass'; logubications(10,ta10statusgood)} 
            if(((point == 11) && (statusx == '1'))){ta11statusgood = 'pass'; logubications(11,ta11statusgood)} 
            if(((point == 12) && (statusx == '1'))){ta12statusgood = 'pass'; logubications(12,ta12statusgood)} 
            if(((point == 13) && (statusx == '1'))){ta13statusgood = 'pass'; logubications(13,ta13statusgood)} 
            if(((point == 14) && (statusx == '1'))){ta14statusgood = 'pass'; logubications(14,ta14statusgood)} 
            if(((point == 15) && (statusx == '1'))){ta15statusgood = 'pass'; logubications(15,ta15statusgood)} 
            if(((point == 16) && (statusx == '1'))){ta16statusgood = 'pass'; logubications(16,ta16statusgood)} 
            if(((point == 17) && (statusx == '1'))){ta17statusgood = 'pass'; logubications(17,ta17statusgood)} 
            if(((point == 18) && (statusx == '1'))){ta18statusgood = 'pass'; logubications(18,ta18statusgood)} 
            if(((point == 19) && (statusx == '1'))){ta19statusgood = 'pass'; logubications(19,ta19statusgood)} 
            if(((point == 20) && (statusx == '1'))){ta20statusgood = 'pass'; logubications(20,ta20statusgood)} 
            if(((point == 21) && (statusx == '1'))){ta21statusgood = 'pass'; logubications(21,ta21statusgood)} 
            if(((point == 22) && (statusx == '1'))){ta22statusgood = 'pass'; logubications(22,ta22statusgood)} 
            if(((point == 23) && (statusx == '1'))){ta23statusgood = 'pass'; logubications(23,ta23statusgood)} 
            if(((point == 24) && (statusx == '1'))){ta24statusgood = 'pass'; logubications(24,ta24statusgood)} 
            if(((point == 25) && (statusx == '1'))){ta25statusgood = 'pass'; logubications(25,ta25statusgood)} 
            if(((point == 26) && (statusx == '1'))){ta26statusgood = 'pass'; logubications(26,ta26statusgood)} 
        }//fin de if porcentajebueno
        else{
            statusx = "0"

            if(((point == 1) && (statusx == '0'))){ta1statusbad = 'fail'; logubications(1,ta1statusbad); } //console.log(ta1statusbad)
            if(((point == 2) && (statusx == '0'))){ta2statusbad = 'fail'; logubications(2,ta2statusbad); } ////console.log(ta2statusbad)
            if(((point == 3) && (statusx == '0'))){ta3statusbad = 'fail'; logubications(3,ta3statusbad)}
            if(((point == 4) && (statusx == '0'))){ta4statusbad = 'fail'; logubications(4,ta4statusbad)}
            if(((point == 5) && (statusx == '0'))){ta5statusbad = 'fail'; logubications(5,ta5statusbad)}
            if(((point == 6) && (statusx == '0'))){ta6statusbad = 'fail'; logubications(6,ta6statusbad)}
            if(((point == 7) && (statusx == '0'))){ta7statusbad = 'fail'; logubications(7,ta7statusbad)}
            if(((point == 8) && (statusx == '0'))){ta8statusbad = 'fail'; logubications(8,ta8statusbad)}
            if(((point == 9) && (statusx == '0'))){ta9statusbad = 'fail'; logubications(9,ta9statusbad)}
            if(((point == 10) && (statusx == '0'))){ta10statusbad = 'fail'; logubications(10,ta10statusbad)}
            if(((point == 11) && (statusx == '0'))){ta11statusbad = 'fail'; logubications(11,ta11statusbad)}
            if(((point == 12) && (statusx == '0'))){ta12statusbad = 'fail'; logubications(12,ta12statusbad)}
            if(((point == 13) && (statusx == '0'))){ta13statusbad = 'fail'; logubications(13,ta13statusbad)}
            if(((point == 14) && (statusx == '0'))){ta14statusbad = 'fail'; logubications(14,ta14statusbad)}
            if(((point == 15) && (statusx == '0'))){ta15statusbad = 'fail'; logubications(15,ta15statusbad)}
            if(((point == 16) && (statusx == '0'))){ta16statusbad = 'fail'; logubications(16,ta16statusbad)}
            if(((point == 17) && (statusx == '0'))){ta17statusbad = 'fail'; logubications(17,ta17statusbad)}
            if(((point == 18) && (statusx == '0'))){ta18statusbad = 'fail'; logubications(18,ta18statusbad)}
            if(((point == 19) && (statusx == '0'))){ta19statusbad = 'fail'; logubications(19,ta19statusbad)}
            if(((point == 20) && (statusx == '0'))){ta20statusbad = 'fail'; logubications(20,ta20statusbad)}
            if(((point == 21) && (statusx == '0'))){ta21statusbad = 'fail'; logubications(21,ta21statusbad)}
            if(((point == 22) && (statusx == '0'))){ta22statusbad = 'fail'; logubications(22,ta22statusbad);console.log(ta2statusbad)}
            if(((point == 23) && (statusx == '0'))){ta23statusbad = 'fail'; logubications(23,ta23statusbad)}
            if(((point == 24) && (statusx == '0'))){ta24statusbad = 'fail'; logubications(24,ta24statusbad)}
            if(((point == 25) && (statusx == '0'))){ta25statusbad = 'fail'; logubications(25,ta25statusbad)}
            if(((point == 26) && (statusx == '0'))){ta26statusbad = 'fail'; logubications(26,ta26statusbad)}
        
            if(((point == 23) && (statusx == '0'))){ta23statusbad = 'fail'; logubications(23,ta23statusbad)}
            if(((point == 24) && (statusx == '0'))){ta24statusbad = 'fail'; logubications(24,ta24statusbad)}
            if(((point == 25) && (statusx == '0'))){ta25statusbad = 'fail'; logubications(25,ta25statusbad)}
            if(((point == 26) && (statusx == '0'))){ta26statusbad = 'fail'; logubications(26,ta26statusbad)}
            //Puntos que inspecciona la IA 
        /*if(point == 5 || point == 6  ||  point == 7 || point == 8  || point == 9  || point == 12  || point == 15 ||  point == 16 ||  point == 17 || point == 18 || point == 19 || point == 20  || point == 21 || point == 22 || point == 26 ){ // se cumple condicion cuando el TA falle y llama a inspectora 
            await mlinspection(point)}
            */
        //Recolector de muestras malas sin IA 
        /*if( point == 1 || point == 2 || point == 3 || point == 4 || point == 5 || point == 6  ||  point == 7 || point == 8  || point == 9 || point == 10  || point == 11 || point == 12 ||  point == 13 || point == 14 || point == 15 ||  point == 16 ||   point == 17 || point == 18 || point == 19 || point == 20 || point == 21 || point == 22 || point == 23 || point == 24 || point == 25 || point == 26 ){ // se cumple condicion cuando el TA falle y llama a inspectora 
           await recolectamalos(point)}*/

    }//fin else 
        console.log("TA-" + point + ":" + porcentajefinal * 100 + "(%) ")//+" Status: "+status
        porcentajeArray[point] = porcentajefinal
        mtxw[point] = porcentajefinal
        resolve('resolved')})
}

function cuentarojos(canvasx, x, y, w, h) { //cuenta puntos rojos de la coordenada con el tamaño especificado

    let contextcanvasx = canvasx.getContext('2d')
    let cdata = contextcanvasx.getImageData(x, y, w, h);
    let rojos = 0
    let otros = 0

    contextcanvasx.strokeStyle = "#3333ff"
    contextcanvasx.lineWidth = 1
    contextcanvasx.strokeRect(x, y, w, h)

    for (let i = 0; i < cdata.data.length; i += 4) { //cdata.data.length
        // Matriz para valores 
        R = cdata.data[i + 0]
        if (R == 255) { rojos++ }
        else { otros++ }
    }// end for
    console.log("Red :" + rojos)
    console.log("Others :" + otros)
}
function limpiaR(p, canvasx) { //Funcion que se utiliza para dibujar un rectangulo azul en el canvas del TA seleccionado y pinta azules parte de los pixeles para convertirlos a pixeles buenos
    return new Promise(async resolve => {
        let contextcanvasx = canvasx.getContext('2d')

        let azul = 0
        let otros = 0
        let x = 0
        let y = 0
        let w = 0
        let h = 0

        //Cuadrante 1 
        if (p == 1) { x = 95, y = 0, w = 200, h = 100 } //TA1  Coordenadas de rectangulos donde pinta azules 
        if (p == 2) { x = 385, y = 28, w = 153, h = 143 } //TA2
        if (p == 11) { x = 36, y = 15, w = 495, h = 157 } //TA11
        if (p == 12) { x = 15, y = 6, w = 500, h = 91 } //TA12
        if (p == 23) { x = 1, y = 25, w = 47, h = 50 } //TB1
        //Cuadrante 2
        if (p == 3) { x = 2, y = 0, w = 143, h = 78 } // TA3
        if (p == 4) { x = 252, y = 12, w = 116, h = 107 } //TA4
        if (p == 9) { x = 7, y = 77, w = 357, h = 40 } //TA9
        if (p == 10) { x = 12, y = 9, w = 345, h = 112 } //TA10
        if (p == 24) { x = 8, y = 9, w = 36, h = 34 } //TB2
        //Cuadrante 3
        if (p == 5) { x = 39, y = 35, w = 439, h = 160 } //TA5
        if (p == 6) { x = 9, y = 1, w = 1264, h = 741 } //TA6
        if (p == 7) { x = 12, y = 6, w = 506, h = 89 } //TA7
        if (p == 8) { x = 15, y = 115, w = 507, h = 73 } //TA8 
        if (p == 25) { x = 7, y = 5, w = 63, h = 65 } // TB3
        //Cuadrante4
        if (p == 17) { x = 33, y = 7, w = 417, h = 72 } //TA17
        if (p == 18) { x = 32, y = 6, w = 438, h = 50 } //TA18
        if (p == 27) { x = 32, y = 6, w = 438, h = 50 } //TA27 // 
        //Cuadrante 5
        if (p == 15) { x = 27, y = 9, w = 356, h = 180 } //TA15
        if (p == 16) { x = 34, y = 12, w = 341, h = 197 } // TA16 
        if (p == 19) { x = 17, y = 0, w = 124, h = 382 } //TA19
        if (p == 20) { x = 18, y = 211, w = 122, h = 239 } //TA20
        if (p == 26) { x = 6, y = 5, w = 52, h = 54 } // TB4
        //Cuadrante 6 
        if (p == 13) { x = 2, y = 3, w = 478, h = 85 } //TA13
        if (p == 14) { x = 20, y = 10, w = 460, h = 75 } //TA14
        //Cuadrante 7 
        if (p == 21) { x = 0, y = 6, w = 25, h = 450 } //TA21
        if (p == 22) { x = 91, y = 1, w = 447, h = 75 } //TA22

        let cdata = contextcanvasx.getImageData(x, y, w, h) // linea para pintar 

        for (let i = 0; i < cdata.data.length; i += 4) { // ciclo de matriz /camina la matriz las veces que tiene el ciclo
            // matriz para pintar color azul si encuentra un rojo
            R = cdata.data[i + 0]
            if (R == 255) {
                cdata.data[i + 0] = 0
                cdata.data[i + 1] = 0
                cdata.data[i + 2] = 255
                cdata.data[i + 3] = 255
                B = cdata.data[i + 2]
                if (B == 255) { azul++ }
                //else {otros++}
            }
        }// end for
        //console.log(cdata.data.length)
        //console.log("Blue :"+azul)
        ruido = azul
        contextcanvasx.putImageData(cdata, x, y)// Dibuja los pixeles rojos encontrados
        contextcanvasx.strokeStyle = "#0000FF"
        contextcanvasx.lineWidth = 1
        contextcanvasx.strokeRect(x, y, w, h)
        resolve('resolved')
    })
}
//------------------------------------------- Funciones de evaluacion ----------//
function logresult(pointemp, statusl) {// Guarda valor de cada punto analizado
    logsave[pointemp] = statusl//"TA"+pointemp+","+statusl+"&";
    
}

function pointstatus(TAx, statusx) {
    if ((TAx == 1) && (statusx == '1')) {
        cuadroVerde1();
    } else if ((TAx == 1) && (statusx == '0')) {
        cuadroRojo1()
    }//Fin de if para TA1

    if ((TAx == 2) && (statusx == '1')) {
        cuadroVerde2()
    } else if ((TAx == 2) && (statusx == '0')) {
        cuadroRojo2()
    }//Fin de if para TA2

    //TA11
    if ((TAx == 11) && (statusx == '1')) {
        cuadroVerde11()
    } else if ((TAx == 11) && (statusx == '0')) {
        cuadroRojo11()
    }//Fin de if para TA11

    //TA12
    if ((TAx == 12) && (statusx == '1')) {
        cuadroVerde12()
    } else if ((TAx == 12) && (statusx == '0')) {
        cuadroRojo12()
    }//Fin de if para TA12

    if ((TAx == 23) && (statusx == '1')) {
        cuadroVerde23()
    } else if ((TAx == 23) && (statusx == '0')) {
        cuadroRojo23()
    }//Fin de if para TB1

    //CUADRANTE 2
    if ((TAx == 3) && (statusx == '1')) {
        cuadroVerde3()
    } else if ((TAx == 3) && (statusx == '0')) {
        cuadroRojo3()
    }//Fin de if para TB1

    if ((TAx == 4) && (statusx == '1')) {
        cuadroVerde4()
    } else if ((TAx == 4) && (statusx == '0')) {
        cuadroRojo4()
    }//Fin de if para TB1

    if ((TAx == 9) && (statusx == '1')) {
        cuadroVerde9()
    } else if ((TAx == 9) && (statusx == '0')) {
        cuadroRojo9()
    }//Fin de if para TB1

    if ((TAx == 10) && (statusx == '1')) {
        cuadroVerde10()
    } else if ((TAx == 10) && (statusx == '0')) {
        cuadroRojo10()
    }//Fin de if para TB1

    if ((TAx == 24) && (statusx == '1')) {
        cuadroVerde24()
    } else if ((TAx == 24) && (statusx == '0')) {
        cuadroRojo24()
    }//Fin de if para TB1

    //Cuadrante 3
    if ((TAx == 5) && (statusx == '1')) {
        cuadroVerde5()
    } else if ((TAx == 5) && (statusx == '0')) {
        cuadroRojo5()
    }

    if ((TAx == 6) && (statusx == '1')) {
        cuadroVerde6()
    } else if ((TAx == 6) && (statusx == '0')) {
        cuadroRojo6()
    }

    if ((TAx == 7) && (statusx == '1')) {
        cuadroVerde7()
    } else if ((TAx == 7) && (statusx == '0')) {
        cuadroRojo7()
    }

    if ((TAx == 8) && (statusx == '1')) {
        cuadroVerde8()
    } else if ((TAx == 8) && (statusx == '0')) {
        cuadroRojo8()
    }

    if ((TAx == 25) && (statusx == '1')) {
        cuadroVerde25()
    } else if ((TAx == 25) && (statusx == '0')) {
        cuadroRojo25()
    }

    if ((TAx == 18) && (statusx == '1')) {
        cuadroVerde18()
    } else if ((TAx == 18) && (statusx == '0')) {
        cuadroRojo18()
    }

    if ((TAx == 17) && (statusx == '1')) {
        cuadroVerde17()
    } else if ((TAx == 17) && (statusx == '0')) {
        cuadroRojo17()
    }

    if ((TAx == 27) && (statusx == '1')) {
        cuadroVerde27()
    } else if ((TAx == 27) && (statusx == '0')) {
        cuadroRojo27()
    }

    if ((TAx == 16) && (statusx == '1')) {
        cuadroVerde16()
    } else if ((TAx == 16) && (statusx == '0')) {
        cuadroRojo16()
    }

    if ((TAx == 15) && (statusx == '1')) {
        cuadroVerde15()
    } else if ((TAx == 15) && (statusx == '0')) {
        cuadroRojo15()
    }

    if ((TAx == 19) && (statusx == '1')) {
        cuadroVerde19()
    } else if ((TAx == 19) && (statusx == '0')) {
        cuadroRojo19()
    }

    if ((TAx == 20) && (statusx == '1')) {
        cuadroVerde20()
    } else if ((TAx == 20) && (statusx == '0')) {
        cuadroRojo20()
    }

    if ((TAx == 26) && (statusx == '1')) {
        cuadroVerde26()
    } else if ((TAx == 26) && (statusx == '0')) {
        cuadroRojo26()
    }

    if ((TAx == 14) && (statusx == '1')) {
        cuadroVerde14()
    } else if ((TAx == 14) && (statusx == '0')) {
        cuadroRojo14()
    }

    if ((TAx == 13) && (statusx == '1')) {
        cuadroVerde13()
    } else if ((TAx == 13) && (statusx == '0')) {
        cuadroRojo13()
    }

    if ((TAx == 21) && (statusx == '1')) {
        cuadroVerde21()
    } else if ((TAx == 21) && (statusx == '0')) {
        cuadroRojo21()
    }

    if ((TAx == 22) && (statusx == '1')) {
        cuadroVerde22()
    } else if ((TAx == 22) && (statusx == '0')) {
        cuadroRojo22()
    }
}//Fin de if principal
//Cuadrante 1
function cuadroVerde1() {
    let fullimage = document.getElementById('CanvasFHD')
    let contextfullimage = fullimage.getContext('2d')

    contextfullimage.strokeStyle = "#76FF03"
    contextfullimage.lineWidth = 2
    contextfullimage.strokeRect(TA1x, TA1y, 118, 299)

}
function cuadroVerde2() {
    let fullimage = document.getElementById('CanvasFHD')
    let contextfullimage = fullimage.getContext('2d')

    contextfullimage.strokeStyle = "#76FF03"
    contextfullimage.lineWidth = 2
    contextfullimage.strokeRect(TA2x, TA2y, 118, 330)
}
function cuadroVerde11() {
    let fullimage = document.getElementById('CanvasFHD')
    let contextfullimage = fullimage.getContext('2d')

    contextfullimage.strokeStyle = "#76FF03"
    contextfullimage.lineWidth = 2
    contextfullimage.strokeRect(TA11x, TA11y, 118, 330)
}
function cuadroVerde12() {
    let fullimage = document.getElementById('CanvasFHD')
    let contextfullimage = fullimage.getContext('2d')

    contextfullimage.strokeStyle = "#76FF03"
    contextfullimage.lineWidth = 2
    contextfullimage.strokeRect(TA12x, TA12y, 118, 312)
}
function cuadroVerde23() {
    let fullimage = document.getElementById('CanvasFHD')
    let contextfullimage = fullimage.getContext('2d')

    contextfullimage.strokeStyle = "#76FF03"
    contextfullimage.lineWidth = 2
    contextfullimage.strokeRect(TB1x, TB1y, 34, 52)
}
//Cuadrante 2
function cuadroVerde3() {
    let fullimage = document.getElementById('CanvasFHD')
    let contextfullimage = fullimage.getContext('2d')

    contextfullimage.strokeStyle = "#76FF03"
    contextfullimage.lineWidth = 2
    contextfullimage.strokeRect(TA3x, TA3y, 118, 299)
}
function cuadroVerde4() {
    let fullimage = document.getElementById('CanvasFHD')
    let contextfullimage = fullimage.getContext('2d')

    contextfullimage.strokeStyle = "#76FF03"
    contextfullimage.lineWidth = 2
    contextfullimage.strokeRect(TA4x, TA4y, 118, 330)
}
function cuadroVerde9() {
    let fullimage = document.getElementById('CanvasFHD')
    let contextfullimage = fullimage.getContext('2d')

    contextfullimage.strokeStyle = "#76FF03"
    contextfullimage.lineWidth = 2
    contextfullimage.strokeRect(TA9x, TA9y, 118, 330)
}
function cuadroVerde10() {
    let fullimage = document.getElementById('CanvasFHD')
    let contextfullimage = fullimage.getContext('2d')

    contextfullimage.strokeStyle = "#76FF03"
    contextfullimage.lineWidth = 2
    contextfullimage.strokeRect(TA10x, TA10y, 118, 330)
}
function cuadroVerde24() {
    let fullimage = document.getElementById('CanvasFHD')
    let contextfullimage = fullimage.getContext('2d')

    contextfullimage.strokeStyle = "#76FF03"
    contextfullimage.lineWidth = 2
    contextfullimage.strokeRect(TB2x, TB2y, 34, 52)
}
//Cuadrante 3
function cuadroVerde5() {
    let fullimage = document.getElementById('CanvasFHD')
    let contextfullimage = fullimage.getContext('2d')

    contextfullimage.strokeStyle = "#76FF03"
    contextfullimage.lineWidth = 2
    contextfullimage.strokeRect(TA5x, TA5y, 118, 299)
}
function cuadroVerde6() {
    let fullimage = document.getElementById('CanvasFHD')
    let contextfullimage = fullimage.getContext('2d')

    contextfullimage.strokeStyle = "#76FF03"
    contextfullimage.lineWidth = 2
    contextfullimage.strokeRect(TA6x, TA6y, 115, 322)
}
function cuadroVerde7() {
    let fullimage = document.getElementById('CanvasFHD')
    let contextfullimage = fullimage.getContext('2d')

    contextfullimage.strokeStyle = "#76FF03"
    contextfullimage.lineWidth = 2
    contextfullimage.strokeRect(TA7x, TA7y, 118, 330)
}
function cuadroVerde8() {
    let fullimage = document.getElementById('CanvasFHD')
    let contextfullimage = fullimage.getContext('2d')

    contextfullimage.strokeStyle = "#76FF03"
    contextfullimage.lineWidth = 2
    contextfullimage.strokeRect(TA8x, TA8y, 118, 330)
}
function cuadroVerde25() {
    let fullimage = document.getElementById('CanvasFHD')
    let contextfullimage = fullimage.getContext('2d')

    contextfullimage.strokeStyle = "#76FF03"
    contextfullimage.lineWidth = 2
    contextfullimage.strokeRect(TB3x, TB3y, 34, 52)
}
//Cuadrante 4 
function cuadroVerde18() {
    let fullimage = document.getElementById('CanvasFHD')
    let contextfullimage = fullimage.getContext('2d')

    contextfullimage.strokeStyle = "#76FF03"
    contextfullimage.lineWidth = 2
    contextfullimage.strokeRect(TA18x, TA18y, 149, 378)
}
function cuadroVerde17() {
    let fullimage = document.getElementById('CanvasFHD')
    let contextfullimage = fullimage.getContext('2d')

    contextfullimage.strokeStyle = "#76FF03"
    contextfullimage.lineWidth = 2
    contextfullimage.strokeRect(TA17x, TA17y, 149, 378)
}
function cuadroVerde27() {
    let fullimage = document.getElementById('CanvasFHD')
    let contextfullimage = fullimage.getContext('2d')

    contextfullimage.strokeStyle = "#76FF03"
    contextfullimage.lineWidth = 2
   // contextfullimage.strokeRect(TATGx, TATGy, 298, 99)
}
//Cuarante 5
function cuadroVerde16() {
    let fullimage = document.getElementById('CanvasFHD')
    let contextfullimage = fullimage.getContext('2d')

    contextfullimage.strokeStyle = "#76FF03"
    contextfullimage.lineWidth = 2
    contextfullimage.strokeRect(TA16x, TA16y, 149, 378)
}
function cuadroVerde15() {
    let fullimage = document.getElementById('CanvasFHD')
    let contextfullimage = fullimage.getContext('2d')

    contextfullimage.strokeStyle = "#76FF03"
    contextfullimage.lineWidth = 2
    contextfullimage.strokeRect(TA15x, TA15y, 149, 378)
}
function cuadroVerde19() {
    let fullimage = document.getElementById('CanvasFHD')
    let contextfullimage = fullimage.getContext('2d')

    contextfullimage.strokeStyle = "#76FF03"
    contextfullimage.lineWidth = 2
    contextfullimage.strokeRect(TA19x, TA19y, 418, 149)
}
function cuadroVerde20() {
    let fullimage = document.getElementById('CanvasFHD')
    let contextfullimage = fullimage.getContext('2d')

    contextfullimage.strokeStyle = "#76FF03"
    contextfullimage.lineWidth = 2
    contextfullimage.strokeRect(TA20x, TA20y, 418, 149)
}
function cuadroVerde26() {
    let fullimage = document.getElementById('CanvasFHD')
    let contextfullimage = fullimage.getContext('2d')

    contextfullimage.strokeStyle = "#76FF03"
    contextfullimage.lineWidth = 2
    contextfullimage.strokeRect(TB4x, TB4y, 66, 52)
}
//Cuadrante 6
function cuadroVerde14() {
    let fullimage = document.getElementById('CanvasFHD')
    let contextfullimage = fullimage.getContext('2d')

    contextfullimage.strokeStyle = "#76FF03"
    contextfullimage.lineWidth = 2
    contextfullimage.strokeRect(TA14x, TA14y, 149, 378)
}
function cuadroVerde13() {
    let fullimage = document.getElementById('CanvasFHD')
    let contextfullimage = fullimage.getContext('2d')

    contextfullimage.strokeStyle = "#76FF03"
    contextfullimage.lineWidth = 2
    contextfullimage.strokeRect(TA13x, TA13y, 149, 378)
}
//Cuadrante 7
function cuadroVerde21() {
    let fullimage = document.getElementById('CanvasFHD')
    let contextfullimage = fullimage.getContext('2d')

    contextfullimage.strokeStyle = "#76FF03"
    contextfullimage.lineWidth = 2
    contextfullimage.strokeRect(TA21x, TA21y, 61, 555)
}
function cuadroVerde22() {
    let fullimage = document.getElementById('CanvasFHD')
    let contextfullimage = fullimage.getContext('2d')

    contextfullimage.strokeStyle = "#76FF03"
    contextfullimage.lineWidth = 2
    contextfullimage.strokeRect(TA22x, TA22y, 429, 795)
}


//Cuadrante 1
function cuadroRojo1() {
    let fullimage = document.getElementById('CanvasFHD')
    let contextfullimage = fullimage.getContext('2d')

    contextfullimage.strokeStyle = "#FF0000"
    contextfullimage.lineWidth = 2
    contextfullimage.strokeRect(TA1x, TA1y, 118, 299)

}
function cuadroRojo2() {
    let fullimage = document.getElementById('CanvasFHD')
    let contextfullimage = fullimage.getContext('2d')

    contextfullimage.strokeStyle = "#FF0000"
    contextfullimage.lineWidth = 2
    contextfullimage.strokeRect(TA2x, TA2y, 118, 330)
}
function cuadroRojo11() {
    let fullimage = document.getElementById('CanvasFHD')
    let contextfullimage = fullimage.getContext('2d')

    contextfullimage.strokeStyle = "#FF0000"
    contextfullimage.lineWidth = 2
    contextfullimage.strokeRect(TA11x, TA11y, 118, 330)
}
function cuadroRojo12() {
    let fullimage = document.getElementById('CanvasFHD')
    let contextfullimage = fullimage.getContext('2d')

    contextfullimage.strokeStyle = "#FF0000"
    contextfullimage.lineWidth = 2
    contextfullimage.strokeRect(TA12x, TA12y, 118, 312)
}
//Cuadrante 2
function cuadroRojo23() {
    let fullimage = document.getElementById('CanvasFHD')
    let contextfullimage = fullimage.getContext('2d')

    contextfullimage.strokeStyle = "#FF0000"
    contextfullimage.lineWidth = 2
    contextfullimage.strokeRect(TB1x, TB1y, 118, 312)
}
function cuadroRojo3() {
    let fullimage = document.getElementById('CanvasFHD')
    let contextfullimage = fullimage.getContext('2d')

    contextfullimage.strokeStyle = "#FF0000"
    contextfullimage.lineWidth = 2
    contextfullimage.strokeRect(TA3x, TA3y, 118, 299)
}
function cuadroRojo4() {
    let fullimage = document.getElementById('CanvasFHD')
    let contextfullimage = fullimage.getContext('2d')

    contextfullimage.strokeStyle = "#FF0000"
    contextfullimage.lineWidth = 2
    contextfullimage.strokeRect(TA4x, TA4y, 118, 330)
}
function cuadroRojo9() {
    let fullimage = document.getElementById('CanvasFHD')
    let contextfullimage = fullimage.getContext('2d')

    contextfullimage.strokeStyle = "#FF0000"
    contextfullimage.lineWidth = 2
    contextfullimage.strokeRect(TA9x, TA9y, 118, 330)
}
function cuadroRojo10() {
    let fullimage = document.getElementById('CanvasFHD')
    let contextfullimage = fullimage.getContext('2d')

    contextfullimage.strokeStyle = "#FF0000"
    contextfullimage.lineWidth = 2
    contextfullimage.strokeRect(TA10x, TA10y, 118, 330)
}
function cuadroRojo24() {
    let fullimage = document.getElementById('CanvasFHD')
    let contextfullimage = fullimage.getContext('2d')

    contextfullimage.strokeStyle = "#FF0000"
    contextfullimage.lineWidth = 2
    contextfullimage.strokeRect(TB2x, TB2y, 34, 52)
}
//Cuadrante 3
function cuadroRojo5() {
    let fullimage = document.getElementById('CanvasFHD')
    let contextfullimage = fullimage.getContext('2d')

    contextfullimage.strokeStyle = "#FF0000"
    contextfullimage.lineWidth = 2
    contextfullimage.strokeRect(TA5x, TA5y, 118, 299)
}
function cuadroRojo6() {
    let fullimage = document.getElementById('CanvasFHD')
    let contextfullimage = fullimage.getContext('2d')

    contextfullimage.strokeStyle = "#FF0000"
    contextfullimage.lineWidth = 2
    contextfullimage.strokeRect(TA6x, TA6y, 115, 322)
}
function cuadroRojo7() {
    let fullimage = document.getElementById('CanvasFHD')
    let contextfullimage = fullimage.getContext('2d')

    contextfullimage.strokeStyle = "#FF0000"
    contextfullimage.lineWidth = 2
    contextfullimage.strokeRect(TA7x, TA7y, 118, 330)
}
function cuadroRojo8() {
    let fullimage = document.getElementById('CanvasFHD')
    let contextfullimage = fullimage.getContext('2d')

    contextfullimage.strokeStyle = "#FF0000"
    contextfullimage.lineWidth = 2
    contextfullimage.strokeRect(TA8x, TA8y, 118, 330)
}
function cuadroRojo25() {
    let fullimage = document.getElementById('CanvasFHD')
    let contextfullimage = fullimage.getContext('2d')

    contextfullimage.strokeStyle = "#00FFFF"
    contextfullimage.lineWidth = 2
    contextfullimage.strokeRect(TB3x, TB3y, 34, 52)
}
//Cuadrante 4
function cuadroRojo18() {
    let fullimage = document.getElementById('CanvasFHD')
    let contextfullimage = fullimage.getContext('2d')

    contextfullimage.strokeStyle = "#FF0000"
    contextfullimage.lineWidth = 2
    contextfullimage.strokeRect(TA18x, TA18y, 149, 378)
}
function cuadroRojo17() {
    let fullimage = document.getElementById('CanvasFHD')
    let contextfullimage = fullimage.getContext('2d')

    contextfullimage.strokeStyle = "#FF0000"
    contextfullimage.lineWidth = 2
    contextfullimage.strokeRect(TA17x, TA17y, 149, 378)
}
function cuadroRojo27() {
    let fullimage = document.getElementById('CanvasFHD')
    let contextfullimage = fullimage.getContext('2d')

    contextfullimage.strokeStyle = "#FF0000"
    contextfullimage.lineWidth = 2
    contextfullimage.strokeRect(TATGx, TATGy, 298, 99)
}
//Cuadrante 5
function cuadroRojo16() {
    let fullimage = document.getElementById('CanvasFHD')
    let contextfullimage = fullimage.getContext('2d')

    contextfullimage.strokeStyle = "#FF0000"
    contextfullimage.lineWidth = 2
    contextfullimage.strokeRect(TA16x, TA16y, 149, 378)
}
function cuadroRojo15() {
    let fullimage = document.getElementById('CanvasFHD')
    let contextfullimage = fullimage.getContext('2d')

    contextfullimage.strokeStyle = "#FF0000"
    contextfullimage.lineWidth = 2
    contextfullimage.strokeRect(TA15x, TA15y, 149, 378)
}
function cuadroRojo19() {
    let fullimage = document.getElementById('CanvasFHD')
    let contextfullimage = fullimage.getContext('2d')

    contextfullimage.strokeStyle = "#FF0000"
    contextfullimage.lineWidth = 2
    contextfullimage.strokeRect(TA19x, TA19y, 418, 149)
}
function cuadroRojo20() {
    let fullimage = document.getElementById('CanvasFHD')
    let contextfullimage = fullimage.getContext('2d')

    contextfullimage.strokeStyle = "#FF0000"
    contextfullimage.lineWidth = 2
    contextfullimage.strokeRect(TA20x, TA20y, 418, 149)
}
function cuadroRojo26() {
    let fullimage = document.getElementById('CanvasFHD')
    let contextfullimage = fullimage.getContext('2d')

    contextfullimage.strokeStyle = "#FF0000"
    contextfullimage.lineWidth = 2
    contextfullimage.strokeRect(TB4x, TB4y, 66, 52)
}

function cuadroRojo14() {
    let fullimage = document.getElementById('CanvasFHD')
    let contextfullimage = fullimage.getContext('2d')

    contextfullimage.strokeStyle = "#FF0000"
    contextfullimage.lineWidth = 2
    contextfullimage.strokeRect(TA14x, TA14y, 149, 378)
}
function cuadroRojo13() {
    let fullimage = document.getElementById('CanvasFHD')
    let contextfullimage = fullimage.getContext('2d')

    contextfullimage.strokeStyle = "#FF0000"
    contextfullimage.lineWidth = 2
    contextfullimage.strokeRect(TA13x, TA13y, 149, 378)
}
function cuadroRojo21() {
    let fullimage = document.getElementById('CanvasFHD')
    let contextfullimage = fullimage.getContext('2d')

    contextfullimage.strokeStyle = "#FF0000"
    contextfullimage.lineWidth = 2
    contextfullimage.strokeRect(TA21x, TA21y, 61, 555)
}
function cuadroRojo22() {
    let fullimage = document.getElementById('CanvasFHD')
    let contextfullimage = fullimage.getContext('2d')

    contextfullimage.strokeStyle = "#FF0000"
    contextfullimage.lineWidth = 2
    contextfullimage.strokeRect(TA22x, TA22y, 429, 795)
}
function Evaluacion(point) { //Evalua la matriz de logsave 
    let valtus  //Variable que guarda el valor que se almacena en el Array
    return new Promise(async resolve => {
        switch (point) {
            case 1:
                //Cuadrante 1 
                cuadranteArray[0] = logsave[1]
                valtus = cuadranteArray.some((e) => e == "0") // funcion para buscar dentro de un array solo un valor 
                if (valtus == true) { valtus = "0" } else { valtus = "1" }
                pointstatus(1, valtus)
                //await bypass(1)
                cuadranteArray = [] //Limpia matriz de trabajo
                

                cuadranteArray[1] = logsave[2]
                valtus = cuadranteArray.some((e) => e == "0") // funcion para buscar dentro de un array solo un valor 
                if (valtus == true) { valtus = "0" } else { valtus = "1" }
                pointstatus(2, valtus)
                //await bypass(2)
                cuadranteArray = [] //Limpia matriz de trabajo

                cuadranteArray[2] = logsave[3]
                valtus = cuadranteArray.some((e) => e == "0") // funcion para buscar dentro de un array solo un valor 
                if (valtus == true) { valtus = "0" } else { valtus = "1" }
                pointstatus(11, valtus)
                //await bypass(11)
                cuadranteArray = [] //Limpia matriz de trabajo

                cuadranteArray[3] = logsave[4]
                valtus = cuadranteArray.some((e) => e == "0") // funcion para buscar dentro de un array solo un valor 
                if (valtus == true) { valtus = "0" } else { valtus = "1" }
                pointstatus(12, valtus)
                //await bypass(12)
                cuadranteArray = [] //Limpia matriz de trabajo

                cuadranteArray[4] = logsave[5]
                valtus = cuadranteArray.some((e) => e == "0") // funcion para buscar dentro de un array solo un valor 
                if (valtus == true) { valtus = "0" } else { valtus = "1" }
                pointstatus(23, valtus)
                //await bypass(23)
                cuadranteArray = [] //Limpia matriz de trabajo
                break

            case 2:
                //Cuadrante 2
                cuadranteArray[0] = logsave[6]
                valtus = cuadranteArray.some((e) => e == "0") // funcion para buscar dentro de un array solo un valor 
                if (valtus == true) { valtus = "0" } else { valtus = "1" }
                pointstatus(3, valtus)
                //await bypass(3)
                cuadranteArray = [] //Limpia matriz de trabajo

                cuadranteArray[1] = logsave[7]
                valtus = cuadranteArray.some((e) => e == "0") // funcion para buscar dentro de un array solo un valor 
                if (valtus == true) { valtus = "0" } else { valtus = "1" }
                pointstatus(4, valtus)
                //await bypass(4)
                cuadranteArray = [] //Limpia matriz de trabajo

                cuadranteArray[2] = logsave[8]
                valtus = cuadranteArray.some((e) => e == "0") // funcion para buscar dentro de un array solo un valor 
                if (valtus == true) { valtus = "0" } else { valtus = "1" }
                pointstatus(9, valtus)
                //await bypass(9)
                cuadranteArray = [] //Limpia matriz de trabajo

                cuadranteArray[3] = logsave[9]
                valtus = cuadranteArray.some((e) => e == "0") // funcion para buscar dentro de un array solo un valor 
                if (valtus == true) { valtus = "0" } else { valtus = "1" }
                pointstatus(10, valtus)
                //await bypass(10)
                cuadranteArray = [] //Limpia matriz de trabajo

                cuadranteArray[4] = logsave[10]
                valtus = cuadranteArray.some((e) => e == "0")
                if (valtus == true) { valtus = "0" } else { valtus = "1" }
                pointstatus(24, valtus)
                //await bypass(24)
                cuadranteArray = [] //Limpia matriz de trabajo
                break

            case 3:
                //Cuadrante 3
                cuadranteArray[0] = logsave[11]
                valtus = cuadranteArray.some((e) => e == "0")
                if (valtus == true) { valtus = "0" } else { valtus = "1" }
                pointstatus(5, valtus)
               // await bypass(5)
                cuadranteArray = [] //Limpia matriz de trabajo

                cuadranteArray[1] = logsave[12]
                valtus = cuadranteArray.some((e) => e == "0")
                if (valtus == true) { valtus = "0" } else { valtus = "1" }
                pointstatus(6, valtus)
                //await bypass(6)
                cuadranteArray = [] //Limpia matriz de trabajo

                cuadranteArray[2] = logsave[13]
                valtus = cuadranteArray.some((e) => e == "0")
                if (valtus == true) { valtus = "0" } else { valtus = "1" }
                pointstatus(7, valtus)
                //await bypass(7)
                cuadranteArray = [] //Limpia matriz de trabajo

                cuadranteArray[3] = logsave[14]
                valtus = cuadranteArray.some((e) => e == "0")
                if (valtus == true) { valtus = "0" } else { valtus = "1" }
                pointstatus(8, valtus)
                //await bypass(8)
                cuadranteArray = [] //Limpia matriz de trabajo

                cuadranteArray[4] = logsave[15]
                valtus = cuadranteArray.some((e) => e == "0")
                if (valtus == true) { valtus = "0" } else { valtus = "1" }
                pointstatus(25, valtus)
                //await bypass(25)
                cuadranteArray = [] //Limpia matriz de trabajo
                break
            case 4:
                //Cuadrante 4
                cuadranteArray[0] = logsave[16]
                valtus = cuadranteArray.some((e) => e == "0")
                if (valtus == true) { valtus = "0" } else { valtus = "1" }
                pointstatus(18, valtus)
                //await bypass(18)
                cuadranteArray = [] //Limpia matriz de trabajo

                cuadranteArray[1] = logsave[17]
                valtus = cuadranteArray.some((e) => e == "0")
                if (valtus == true) { valtus = "0" } else { valtus = "1" }
                pointstatus(17, valtus)
                //await bypass(17)
                cuadranteArray = [] //Limpia matriz de trabajo

                cuadranteArray[2] = logsave[18]
                valtus = cuadranteArray.some((e) => e == "0")
                if (valtus == true) { valtus = "0" } else { valtus = "1" }
                pointstatus(27, valtus)
               // await bypass(27)
                cuadranteArray = [] //Limpia matriz de trabajo
                break
            case 5:
                //Cuadrante 5
                cuadranteArray[0] = logsave[19]
                valtus = cuadranteArray.some((e) => e == "0")
                if (valtus == true) { valtus = "0" } else { valtus = "1" }
                pointstatus(16, valtus)
                //await bypass(16)
                cuadranteArray = [] //Limpia matriz de trabajo

                cuadranteArray[1] = logsave[20]
                valtus = cuadranteArray.some((e) => e == "0")
                if (valtus == true) { valtus = "0" } else { valtus = "1" }
                pointstatus(15, valtus)
                //await bypass(15)
                cuadranteArray = [] //Limpia matriz de trabajo

                cuadranteArray[2] = logsave[21]
                valtus = cuadranteArray.some((e) => e == "0")
                if (valtus == true) { valtus = "0" } else { valtus = "1" }
                pointstatus(19, valtus)
                //await bypass(19)
                cuadranteArray = [] //Limpia matriz de trabajo

                cuadranteArray[3] = logsave[22]
                valtus = cuadranteArray.some((e) => e == "0")
                if (valtus == true) { valtus = "0" } else { valtus = "1" }
                pointstatus(20, valtus)
                //await bypass(20)
                cuadranteArray = [] //Limpia matriz de trabajo

                cuadranteArray[4] = logsave[23]
                valtus = cuadranteArray.some((e) => e == "0")
                if (valtus == true) { valtus = "0" } else { valtus = "1" }
                pointstatus(26, valtus)
                //await bypass(26)
                cuadranteArray = [] //Limpia matriz de trabajo
                break
            case 6:
                //Cuadrante 6
                cuadranteArray[0] = logsave[24]
                valtus = cuadranteArray.some((e) => e == "0")
                if (valtus == true) { valtus = "0" } else { valtus = "1" }
                pointstatus(14, valtus)
                //await bypass(14)
                cuadranteArray = [] //Limpia matriz de trabajo

                cuadranteArray[1] = logsave[25]
                valtus = cuadranteArray.some((e) => e == "0")
                if (valtus == true) { valtus = "0" } else { valtus = "1" }
                pointstatus(13, valtus)
                 //await bypass(13)
                cuadranteArray = [] //Limpia matriz de trabajo
                break
            case 7:
                //Cuadrante 7 
                cuadranteArray[0] = logsave[26]
                valtus = cuadranteArray.some((e) => e == "0")
                if (valtus == true) { valtus = "0" } else { valtus = "1" }
                pointstatus(21, valtus)
                //await bypass(21)
                cuadranteArray = [] //Limpia matriz de trabajo

                cuadranteArray[1] = logsave[27]
                valtus = cuadranteArray.some((e) => e == "0")
                if (valtus == true) { valtus = "0" } else { valtus = "1" }
                pointstatus(22, valtus)
                //await bypass(22)
                cuadranteArray = [] //Limpia matriz de trabajo
            
                //logsave.fill('1') 
                alltas() // FUNCION QUE ALMACENA CADA STATUS DE TA'S
                //La evaluacion de todo el array se hara hasta el final del ultimo case
                //logsave[27] = ('1') //Dispensado pendiente de revisar 
                //turno 1
                let resultadofinal = logsave.some((e) => e == "0")
                //console.log(logsave)
                //let hora = 23
                if (resultadofinal == false) {
                    pass()
                    passturno() //tiene status
                    console.log("Unit---> Pass")
                } else {
                    fail()
                    failturno()
                    console.log("Unit---> Fail")
                }
                resultado = resultadofinal
                break
            default:
        }//fin de switch

        resolve('resolved')
    })
}
function agrupapasst2(turno, status, day,fecha) {
    return new Promise(async resolve => {
        const socket = io();
        socket.emit('agrupapasst2', { turno, status, day,fecha })
        resolve('resolved');
    })
}
function agrupardias(status,day,semana) {
    return new Promise(async resolve => {
        const socket = io();
        socket.emit('agrupardias', { status, day,semana })
        resolve('resolved');
    })
}



function abrir() {
    return new Promise(async resolve => {
        const socket = io();
        socket.emit('abrir')
        setTimeout(function fire() { resolve('resolved'); }, 1000) //tiempo para el opencam
    })//cierra la promesa
}
function cerrar() {
    const socket = io();
    socket.emit('cerrar')
}
function exp() {
    const socket = io();
    socket.emit('exp')
}
//funciones abrir y cerrar conexion
socket.on('qtyP2', function (resulcons) {
    let datosp2 = resulcons.result
    turno_pass_qty2 = parseInt(datosp2.rows[0].count, 10)
    //console.log(resulcons.result)
   // console.log(parseInt(datosp2.rows[0].count, 10))

    //UnidadesPLunesT1(resulcons)
    bar.data.datasets[resulcons.status === 'pass' ? 0 : 1].data.push(turno_pass_qty2)
   // console.log(bar.data.datasets[resulcons.status === 'pass' ? 0 : 1].data)
    bar.update()
})
//console.log(turno_pass_qty2)
//console.log(resulcons.status)
//qtyD segunda grafica, por dias

socket.on('qtyD', function (resulday) {
    let datosday = resulday.result
    //console.log(datosday)
    turno_pass_qtyD = parseInt(datosday.rows[0].count, 10)
    //console.log(datosday)
    //console.log(parseInt(datosday.rows[0].count, 10))
    //se mandan llamar las funciones de "funcionyield"
    yieldMonday(resulday)
    yieldTuesday(resulday)
    yieldWednesday(resulday)
    yieldThursday(resulday)
    yieldFriday(resulday)
    yieldSaturday(resulday)
    yieldSunday(resulday)

    linea.data.datasets[resulday.day === 'day', resulday.status == 'pass' ? 0 : 1].data.push(turno_pass_qtyD)
    linea.update()
})

socket.on('rgbdataselect', function(results){
    let datacalibration = results.result
    //console.log(datacalibration)
    tacalibration = datacalibration.rows[0]
    //console.log('REsultado de calibracion',tacalibration)
    let newta = datacalibration.rows[0].ta
    //console.log('TA: ',newta)
    Rmin = datacalibration.rows[0].rmin
    //console.log('Valor Rmin',Rmin)
    Rmax = datacalibration.rows[0].rmax
    Gmin = datacalibration.rows[0].gmin
    Gmax = datacalibration.rows[0].gmax
    Bmin = datacalibration.rows[0].bmin
    Bmax = datacalibration.rows[0].bmax
    //console.log('Datos de calibracion:')
})

socket.on('qtytas', function (resultas){
    datostas= resultas.result
    //console.log(datostas)
    ta1fail=parseInt(datostas.rows[0].conteota1, 10)//analiza una cadena para determinar si contiene un valor entero
    ta2fail=parseInt(datostas.rows[0].conteota2, 10)
    ta3fail=parseInt(datostas.rows[0].conteota3, 10)
    ta4fail=parseInt(datostas.rows[0].conteota4, 10)
    ta5fail=parseInt(datostas.rows[0].conteota5, 10)
    ta6fail=parseInt(datostas.rows[0].conteota6, 10)
    ta7fail=parseInt(datostas.rows[0].conteota7, 10)
    ta8fail=parseInt(datostas.rows[0].conteota8, 10)
    ta9fail=parseInt(datostas.rows[0].conteota9, 10)
    ta10fail=parseInt(datostas.rows[0].conteota10, 10)
    ta11fail=parseInt(datostas.rows[0].conteota11, 10)
    ta12fail=parseInt(datostas.rows[0].conteota12, 10)
    ta13fail=parseInt(datostas.rows[0].conteota13, 10)
    ta14fail=parseInt(datostas.rows[0].conteota14, 10)
    ta15fail=parseInt(datostas.rows[0].conteota15, 10)
    ta16fail=parseInt(datostas.rows[0].conteota16, 10)
    ta17fail=parseInt(datostas.rows[0].conteota17, 10)
    ta18fail=parseInt(datostas.rows[0].conteota18, 10)
    ta19fail=parseInt(datostas.rows[0].conteota19, 10)
    ta20fail=parseInt(datostas.rows[0].conteota20, 10)
    ta21fail=parseInt(datostas.rows[0].conteota21, 10)
    ta22fail=parseInt(datostas.rows[0].conteota22, 10)
    ta23fail=parseInt(datostas.rows[0].conteota23, 10)
    ta24fail=parseInt(datostas.rows[0].conteota24, 10)
    ta25fail=parseInt(datostas.rows[0].conteota25, 10)
    ta26fail=parseInt(datostas.rows[0].conteota26, 10)
    tasArry =[null,ta1fail,ta2fail,ta3fail,ta4fail,ta5fail,ta6fail,ta7fail,ta8fail,ta9fail,ta10fail,ta11fail,ta12fail,ta13fail,ta14fail,ta15fail,ta16fail,ta17fail,ta18fail,ta19fail,ta20fail,ta21fail,ta22fail,ta23fail,ta24fail,ta25fail,ta26fail]
    //console.log('Datos de ta17fail',ta17fail)
    //console.log(ta1fail,ta2fail,ta3fail,ta4fail,ta5fail,ta6fail,ta7fail,ta8fail,ta9fail,ta10fail,ta11fail,ta12fail,ta13fail,ta14fail,ta15fail,ta16fail,ta17fail,ta18fail,ta19fail,ta20fail,ta21fail,ta22fail,ta23fail,ta24fail,ta25fail,ta26fail)

    bardos.data.datasets[resultas.ta1== 'fail', resultas.ta2=='fail', resultas.ta3=='fail', resultas.ta4=='fail',resultas.ta5=='fail',resultas.ta6=='fail' ? 0 : 1].data.push(ta1fail,ta2fail,ta3fail,ta4fail,ta5fail,ta6fail,ta7fail,ta8fail,ta9fail,ta10fail,ta11fail,ta12fail,ta13fail,ta14fail,ta15fail,ta16fail,ta17fail,ta18fail,ta19fail,ta20fail,ta21fail,ta22fail,ta23fail,ta24fail,ta25fail,ta26fail)//.data
    bardos.update()

    /*if(ta1fail <= valmax ){
        console.log("TA sin errores")
    }*/
    if(ta1fail >= valmax){
        console.log("Alerta Revisar TA1 -->", ta1fail)
    }
    if(ta2fail >= valmax){
        console.log("Alerta Revisar TA2 -->", ta2fail)
    }
    if(ta3fail >= valmax){
        console.log("Alerta Revisar TA3 -->", ta3fail)
    } 
    if(ta4fail >= valmax){
        console.log("Alerta Revisar TA4 -->", ta4fail)
    }
    if(ta5fail >= valmax){
        console.log("Alerta Revisar TA5 -->", ta5fail)
    }
    if(ta6fail >= valmax){
        console.log("Alerta Revisar TA6 -->", ta6fail)
    }
    if(ta7fail >= valmax){
        console.log("Alerta Revisar TA7 -->", ta7fail)
    }
    if(ta8fail >= valmax){
        console.log("Alerta Revisar TA8 -->", ta8fail)
    }
    if(ta9fail >= valmax){
        console.log("Alerta Revisar TA9 -->", ta9fail)
    }
    if(ta10fail >= valmax){
        console.log("Alerta Revisar TA10 -->", ta10fail)
    }
    if(ta11fail >= valmax){
        console.log("Alerta Revisar TA11 -->", ta11fail)
    }
    if(ta12fail >= valmax){
        console.log("Alerta Revisar TA12 -->", ta12fail)
    }
    if(ta13fail >= valmax){
        console.log("Alerta Revisar TA13 -->", ta13fail)
    }
    if(ta14fail >= valmax){
        console.log("Alerta Revisar TA14 -->", ta14fail)
    }
    if(ta15fail >= valmax){
        console.log("Alerta Revisar TA15 -->", ta15fail)
    }
    if(ta16fail >= valmax){
        console.log("Alerta Revisar TA16 -->",ta16fail)
    }
    if(ta17fail >= valmax){
        console.log("Alerta Revisar TA17 -->",ta17fail)
    }
    if(ta18fail >= valmax){
        console.log("Alerta Revisar TA18 -->", ta18fail)
    }
    if(ta19fail >= valmax){
        console.log("Alerta Revisar TA19 -->", ta19fail)
    }
    if(ta20fail >= valmax){
        console.log("Alerta Revisar TA20 -->", ta20fail)
    }
    if(ta21fail >= valmax){
        console.log("Alerta Revisar TA21 -->", ta21fail)
    }
    if(ta22fail >= valmax){
        console.log("Alerta Revisar TA22 -->", ta22fail)
    }
    if(ta23fail >= valmax){
        console.log("Alerta Revisar TA23 -->", ta23fail)
    }
    if(ta24fail >= valmax){
        console.log("Alerta Revisar TA24 -->", ta24fail)
    }
    if(ta25fail >= valmax){
        console.log("Alerta Revisar TA25 -->", ta25fail)
    }
    if(ta26fail >= valmax){
        console.log("Alerta Revisar TA26 -->", ta26fail)
    }
})


//console.log(turno_pass_qtyD)
//operacion

/*let contador = 0
    for(let i = 0; i<datosday.length; i++){
        if(datosday[i].status === 'fail')
        contador++;
    
    }
    console.log(contador)*/

//---------------------------Respuesta backend--------------------------------//
socket.on('respuestabd', function (conteototal) {
    //console.log(conteototal)
    
})


//--------------------------------------------Funciones de la camara-----------//
async function open_cam(point) {// Resolve de 2 segundos

    return new Promise(async resolve => {
        
        if (point == 1) { camid = "ca77f42ad533ed070c2572debe799cecfb6176d2b51e38c51f692e5d1d0d9ef6" }
        if (point == 2) { camid = "2a1661fd03d75e8f75da4916a0fc38232cbb974756d14253f45633887f1107dc" }
        if (point == 3) { camid = "90e598aa84b34634093880c339289bede2a68eb9f30f00dc27ca89cb8729e9c0" } //Camera 3
        // camara UR

        const video = document.querySelector('video')
        const vgaConstraints = {
            video:
            {
                width: { ideal: 1920 },
                height: { ideal: 1080 },
                deviceId: camid
            }
        }
        await navigator.mediaDevices.getUserMedia(vgaConstraints).then((stream) => { video.srcObject = stream }).catch(function (err) { 
            console.log(err.name)
            location.reload()
         })
        setTimeout(function fire() { resolve('resolved'); }, 3500) //tiempo para el opencam
    })//Cierra Pro}mise principal
}
function captureimage() {// Resolve de 2 segundos
    return new Promise(async resolve => {

        let image = document.getElementById('CanvasFHD');
        let contexim2 = image.getContext('2d');

        var video = document.getElementById("video");

        w = image.width;
        h = image.height;

        contexim2.drawImage(video, 0, 0, image.width, image.height);
        //var dataURI = canvas.toDataURL('image/jpeg');
        //setTimeout(function fire(){resolve('resolved');},2000);//Temporal para programacion de secuencia
        resolve('resolved')
    });
}
function mapcams() {
    navigator.mediaDevices.enumerateDevices()
        .then(devices => {
            let filtered = devices.filter(device => device.kind === 'videoinput');
            //console.log('Cameras found', filtered);
            let count = Object.keys(filtered).length
            let countcomplete = 'Camera enabled: ' + count
            camedetect(countcomplete)
        });
}
async function IDcam(){
   return new Promise(async resolve =>{
    navigator.mediaDevices.enumerateDevices()
        .then(devices => {
            let filtered = devices.filter(device => device.kind === 'videoinput');
            console.log('Cameras found', filtered);
             Point3ID = filtered[0].deviceId
             Point1ID = filtered[1].deviceId
             Point2ID = filtered[2].deviceId
             console.log('Camera 1',Point1ID);console.log('Camera 2',Point2ID);console.log('Camera 3',Point3ID)
        });
resolve('resolved')})
}

function stopcam() {
    return new Promise(async resolve => {
        const video = document.querySelector('video');
        // A video's MediaStream object is available through its srcObject attribute
        const mediaStream = video.srcObject;
        // Through the MediaStream, you can get the MediaStreamTracks with getTracks():
        const tracks = mediaStream.getTracks();
        tracks.forEach(track => { track.stop() })//;console.log(track);
        setTimeout(function fire() { resolve('resolved'); }, 1000);
    });//Cierra Promise principal
}
function snapshot() {//Back end sent URI,SN? & point?
    return new Promise(async resolve => {
        //let image1 = document.getElementById( 'fullimage' );
        //let contexim1 = image1.getContext( '2d' );        

        //var video = document.getElementById("webcam_conveyor");

        //w = image1.width;
        //h= image1.height;

        //contextfullimage.drawImage(fullimage,0,0,fullimage.width,fullimage.height);
        var dataURI = fullimage.toDataURL('image/jpeg');
        savepic(dataURI, snfile, point); //savepic(dataURI,point);
        //console.log("Pic Sent--"+sn+"--"+point);
        //setTimeout(function fire(){resolve('resolved');},2000);//Temporal para programacion de secuencia
        resolve('resolved')
    });
}
function renombra(snfile) {
    const socket = io();
    socket.emit('renombrasnr', snfile);
}
function savepic(uri, snfile, point) {
    const socket = io();
    socket.emit('picsaving', uri, snfile, point);
}
function logsaving(snr, logdata,logsave) {
   // console.log("Entre a logsaving",snr, logdata,logsave)
    socket.emit('logsaving', snr, logdata,logsave);
    
}
//------------------------Funciones de debug---------------//

let calis = new Image()// Variable utilizada por switchpic

function loadcalis(fotox) {//Funcion Carga la imagen del modelo 
    switchpic(fotox)
    setTimeout(function dibuja() {
        contextfullimage.drawImage(calis, 0, 0, calis.width, calis.height, 0, 0, contextfullimage.canvas.width, contextfullimage.canvas.height)
        canbughide()
    }, 300)
}
function switchpic(name) {
    calis.src = "/sofia/img/" + name + ".png"
}

function canbughide() { // funcion para esconder los canvas 
    return new Promise(async resolve => {
        document.getElementById('CanvasFHD').style.visibility = "hidden"
        document.getElementById('canvasClen1').style.visibility = "hidden"
        document.getElementById('canvasMlen1').style.visibility = "hidden"
        document.getElementById('canvasGlen1').style.visibility = "hidden"
        document.getElementById('canvasClen2').style.visibility = "hidden"
        document.getElementById('canvasTA6len1').style.visibility = "hidden"
        document.getElementById('canvasGlen2').style.visibility = "hidden"
        document.getElementById('canvasflalen3').style.visibility = "hidden"
        document.getElementById('canvasgolen3').style.visibility = "hidden"
        resolve('resolved')
    });
}

function canbugshow() {
    return new Promise(async resolve => {
        document.getElementById('CanvasFHD').style.visibility = "visible"
        document.getElementById('canvasClen1').style.visibility = "visible"
        document.getElementById('canvasMlen1').style.visibility = "visible"
        document.getElementById('canvasGlen1').style.visibility = "visible"
        document.getElementById('canvasClen2').style.visibility = "visible"
        document.getElementById('canvasTA6len1').style.visibility = "visible"
        document.getElementById('canvasGlen2').style.visibility = "visible"
        document.getElementById('canvasflalen3').style.visibility = "visible"
        document.getElementById('canvasgolen3').style.visibility = "visible"
        resolve('resolved')
    });
}


//---------------------------------- Seccion cadena de plc ---------------//

async function split(infoplc) { // S&IDM-2007&P1093219-00-G:SBNJ19194020602&LFTM1135558-04-A&START#
    //console.log(infoplc)
    station = infoplc.toString().substr(2, 8); //console.log(station)
    sn = infoplc.toString().substr(11, 29); //console.log(sn)
    pn = infoplc.toString().substr(41, 16); //console.log(pn)
    await serialnumber(sn)
    await partnumber(pn)
    await st(station)

}
//console.log(model)
//------------------------------------------------- IA ----------------------------------------------------------
async function mlinspection(point) {
    return new Promise(async resolve => {
        switch(point){
            case 1: //TA1
                                model = new cvstfjs.ClassificationModel();
                                await model.loadModelAsync('../ml/TA1/model.json');
                                result = await model.executeAsync(canvasClen1)
                            
                                pasa = result[0][0]
                                falla = result[0][1]
                                if(pasa >= falla){ //Evalua el valor en la posicion 0 que da la redneuronal
                                    statusx = "1" 
                                    console.log("AI Inspection: " + `${statusx == 1 ? 'Pass' :'Fail' }`)
                                    porcentajefinal = 1
                                    if(((point == 1) && (statusx == '1'))){ta1statusgood = 'pass'; logubications(1,ta1statusgood)} 
                                    await recolectabuenos(point)
                                }else{
                                    statusx = "0"
                                    if(((point == 1) && (statusx == '0'))){ta1statusbad = 'fail'; logubications(1,ta1statusbad)} 
                                    console.log("AI Inspection: " + `${statusx == 0 ? 'Fail' : 'Pass'}`)
                                    await  recolectamalos(point)
                                }
                                    IAdesition[1]= statusx; model = null
                                break
            case 2://TA2
                                model = new cvstfjs.ClassificationModel();
                                await model.loadModelAsync('../ml/TA2/model.json');
                                result = await model.executeAsync(canvasGlen1)
                                setTimeout(function fire(){console.log("Entrenamiento cargado"),2000})
                                pasa = result[0][0]
                                falla = result[0][1]
                                if(pasa >= falla){ //Evalua el valor en la posicion 0 que da la redneuronal
                                    statusx = "1" 
                                    console.log("AI Inspection: " + `${statusx == 1 ? 'Pass' :'Fail' }`)
                                    porcentajefinal = 1
                                    if(((point == 2) && (statusx == '1'))){ta2statusgood = 'pass';logubications(2,ta2statusgood)} 
                                    await recolectabuenos(point) 
                                }else{
                                    statusx = "0"
                                    if(((point == 2) && (statusx == '0'))){ta2statusbad = 'fail'; logubications(2,ta2statusbad)} 
                                    console.log("AI Inspection: " + `${statusx == 0 ? 'Fail' : 'Pass'}`)
                                    await  recolectamalos(point)//Funcion recolectora de muestras para entrenamientos
                                }                                
                                    IAdesition[2]= statusx;model = null
                                break
            case 3: //TA3 
                                model = new cvstfjs.ClassificationModel();
                                await model.loadModelAsync('../ml/TA3/model.json');
                                result = await model.executeAsync(canvasClen1)
                                console.log(model)
                                //console.log(result)
                                //console.log(result[0][0]) //Accede al elemento 0 del array en un objeto
                                pasa = result[0][0]
                                falla = result[0][1]
                                console.log("Unit pass" , pasa)
                                console.log("Unit fail", falla)
                                if(pasa >= falla){ //Evalua el valor en la posicion 0 que da la redneuronal
                                    //console.log("Unit pass" , pass)
                                    statusx = "1" 
                                    console.log("AI Inspection: " + `${statusx == 1 ? 'Pass' :'Fail' }`)
                                    porcentajefinal = 1
                                    if(((point == 3) && (statusx == '1'))){ta3statusgood = 'pass'; logubications(3,ta3statusgood)} 
                                    await recolectabuenos(point)
                                }else{
                                    statusx = "0"
                                    if(((point == 3) && (statusx == '0'))){ta2statusbad = 'fail'; logubications(3,ta3statusbad)}
                                    console.log("AI Inspection: " + `${statusx == 0 ? 'Fail' : 'Pass'}`)
                                    await  recolectamalos(point)
                                }
                                    IAdesition[3]= statusx;model = null
                                break
             case 4: //TA4
                                model = new cvstfjs.ClassificationModel();
                                await model.loadModelAsync('../ml/TA4/model.json');
                                //const image = document.getElementById('canvasClen1');
                                result = await model.executeAsync(canvasGlen1)
                                console.log(model)
                                // console.log(result)
                                //console.log(result[0][0]) //Accede al elemento 0 del array en un objeto
                                pasa = result[0][0]
                                falla = result[0][1]
                                // console.log("Unit pass" , pasa)
                                //console.log("Unit fail", falla)
                                if(pasa >= falla){ //Evalua el valor en la posicion 0 que da la redneuronal
                                    //console.log("Unit pass" , pass)
                                    statusx = "1" 
                                    console.log("AI Inspection: " + `${statusx == 1 ? 'Pass' :'Fail' }`)
                                    porcentajefinal = 1
                                    if(((point == 4) && (statusx == '1'))){ta4statusgood = 'pass'; logubications(4,ta4statusgood)}
                                    await recolectabuenos(point)
                                }else{
                                    statusx = "0"
                                    if(((point == 4) && (statusx == '0'))){ta4statusbad = 'fail'; logubications(4,ta4statusbad)}
                                    console.log("AI Inspection: " + `${statusx == 0 ? 'Fail' : 'Pass'}`)
                                    await  recolectamalos(point)
                                }
                                    IAdesition[4]= statusx;model = null
                                break
             case 5: //TA5
                                model = new cvstfjs.ClassificationModel();
                                await model.loadModelAsync('../ml/TA5/model.json');
                                //const image = document.getElementById('canvasClen1');
                                result = await model.executeAsync(canvasClen1)
                                console.log(model)
                                // console.log(result)
                                //console.log(result[0][0]) //Accede al elemento 0 del array en un objeto
                                pasa = result[0][0]
                                falla = result[0][1]
                                console.log("Unit pass" , pasa)
                                console.log("Unit fail", falla)
                                if(pasa >= falla){ //Evalua el valor en la posicion 0 que da la redneuronal
                                    //console.log("Unit pass" , pass)
                                    statusx = "1" 
                                    console.log("AI Inspection: " + `${statusx == 1 ? 'Pass' :'Fail' }`)
                                    porcentajefinal = 1
                                    if(((point == 5) && (statusx == '1'))){ta5statusgood = 'pass'; logubications(5,ta5statusgood)} 
                                    await recolectabuenos(point)
                                }else{
                                    statusx = "0"
                                    if(((point == 5) && (statusx == '0'))){ta5statusbad = 'fail'; logubications(5,ta5statusbad)}
                                    console.log("AI Inspection: " + `${statusx == 0 ? 'Fail' : 'Pass'}`)
                                    await  recolectamalos(point)
                                }
                                    IAdesition[5]= statusx;model = null
                                break
             case 6: //TA6
                                model = new cvstfjs.ClassificationModel();
                                await model.loadModelAsync('../ml/TA6/model.json');
                                //const image = document.getElementById('canvasClen1');
                                result = await model.executeAsync(canvasTA6len1)
                                console.log(model)
                                // console.log(result)
                                //console.log(result[0][0]) //Accede al elemento 0 del array en un objeto
                                pasa = result[0][0]
                                falla = result[0][1]
                                console.log("Unit pass" , pasa)
                                console.log("Unit fail", falla)
                                if(pasa >= falla){ //Evalua el valor en la posicion 0 que da la redneuronal
                                //console.log("Unit pass" , pass)
                                statusx = "1" 
                                console.log("AI Inspection: " + `${statusx == 1 ? 'Pass' :'Fail' }`)
                                porcentajefinal = 1
                                if(((point == 6) && (statusx == '1'))){ta6statusgood = 'pass'; logubications(6,ta6statusgood)} 
                               await recolectabuenos(point)
                                }else{
                               statusx = "0"
                               if(((point == 6) && (statusx == '0'))){ta6statusbad = 'fail'; logubications(6,ta6statusbad)}
                               console.log("AI Inspection: " + `${statusx == 0 ? 'Fail' : 'Pass'}`)
                               await  recolectamalos(point)
                            }
                               IAdesition[6]= statusx;model = null
                            break       
             case 7: //TA7
                            model = new cvstfjs.ClassificationModel();
                            await model.loadModelAsync('../ml/TA7/model.json');
                            //const image = document.getElementById('canvasClen1');
                            result = await model.executeAsync(canvasGlen1)
                            console.log(model)
                            // console.log(result)
                            //console.log(result[0][0]) //Accede al elemento 0 del array en un objeto
                            pasa = result[0][0]
                            falla = result[0][1]
                            console.log("Unit pass" , pasa)
                            console.log("Unit fail", falla)
                            if(pasa >= falla){ //Evalua el valor en la posicion 0 que da la redneuronal
                            //console.log("Unit pass" , pass)
                            statusx = "1" 
                            console.log("AI Inspection: " + `${statusx == 1 ? 'Pass' :'Fail' }`)
                            porcentajefinal = 1
                            if(((point == 7) && (statusx == '1'))){ta7statusgood = 'pass'; logubications(7,ta7statusgood)} 
                            await recolectabuenos(point)
                              }else{
                           statusx = "0"
                           if(((point == 7) && (statusx == '0'))){ta7statusbad = 'fail'; logubications(7,ta7statusbad)}
                           console.log("AI Inspection: " + `${statusx == 0 ? 'Fail' : 'Pass'}`)
                           await  recolectamalos(point)
                           }
                           IAdesition[7]= statusx;model = null
                        break       
             case 8: //TA8
                            model = new cvstfjs.ClassificationModel();
                            await model.loadModelAsync('../ml/TA8/model.json');
                            //const image = document.getElementById('canvasClen1');
                            result = await model.executeAsync(canvasGlen1)
                            console.log(model)
                            // console.log(result)
                            //console.log(result[0][0]) //Accede al elemento 0 del array en un objeto
                            pasa = result[0][0]
                            falla = result[0][1]
                            console.log("Unit pass" , pasa)
                            console.log("Unit fail", falla)
                           if(pasa >= falla){ //Evalua el valor en la posicion 0 que da la redneuronal
                               //console.log("Unit pass" , pass)
                               statusx = "1" 
                               console.log("AI Inspection: " + `${statusx == 1 ? 'Pass' :'Fail' }`)
                               porcentajefinal = 1
                               if(((point == 8) && (statusx == '1'))){ta8statusgood = 'pass'; logubications(8,ta8statusgood)} 
                               await recolectabuenos(point)
                           }else{
                               statusx = "0"
                               if(((point == 8) && (statusx == '0'))){ta8statusbad = 'fail'; logubications(8,ta8statusbad)}
                               console.log("AI Inspection: " + `${statusx == 0 ? 'Fail' : 'Pass'}`)
                               await  recolectamalos(point)
                            }
                               IAdesition[8]= statusx;model = null
                            break
            case 9: //TA9
                            model = new cvstfjs.ClassificationModel();
                            await model.loadModelAsync('../ml/TA9/model.json');
                            //const image = document.getElementById('canvasClen1');
                            result = await model.executeAsync(canvasGlen1);
                            // console.log(result)
                            //console.log(result[0][0]) //Accede al elemento 0 del array en un objeto
                            console.log(model)
                            pasa = result[0][0]
                            falla = result[0][1]
                             console.log("Unit pass" , pasa)
                             console.log("Unit fail", falla)
                            if(pasa >= falla){ //Evalua el valor en la posicion 0 que da la redneuronal
                                //console.log("Unit pass" , pass)
                                statusx = "1" 
                                console.log("AI Inspection: " + `${statusx == 1 ? 'Pass' :'Fail' }`)
                                porcentajefinal = 1
                                if(((point == 9) && (statusx == '1'))){ta9statusgood = 'pass'; logubications(9,ta9statusgood)} 
                                await recolectabuenos(point)
                            }else{
                                statusx = "0"
                                if(((point == 9) && (statusx == '0'))){ta9statusbad = 'fail'; logubications(9,ta9statusbad)}
                                console.log("AI Inspection: " + `${statusx == 0 ? 'Fail' : 'Pass'}`)
                                await  recolectamalos(point)
                             }
                                IAdesition[9]= statusx;model = null
                             break
            case 10: //TA10
                                // model = new cvstfjs.ClassificationModel();
                                /*await model.loadModelAsync('../ml/TA10/model.json');
                                //const image = document.getElementById('canvasClen1');
                                result = await model.executeAsync(canvasGlen1);
                                console.log(model)
                                console.log(result)*/
                                //console.log(result[0][0]) //Accede al elemento 0 del array en un objeto
                                await loadneuralTA10()
                                pasa = result[0][0]
                                falla = result[0][1]
                                console.log("Unit pass" , pasa)
                                console.log("Unit fail", falla)
                                if(pasa >= falla){ //Evalua el valor en la posicion 0 que da la redneuronal
                                    //console.log("Unit pass" , pass)
                                    statusx = "1" 
                                    console.log("AI Inspection: " + `${statusx == 1 ? 'Pass' :'Fail' }`)
                                    porcentajefinal = 1
                                    if(((point == 10) && (statusx == '1'))){ta10statusgood = 'pass'; logubications(10,ta10statusgood)} 
                                   await recolectabuenos(point)
                                }else{
                                    statusx = "0"
                                    if(((point == 10) && (statusx == '0'))){ta10statusbad = 'fail'; logubications(10,ta10statusbad)}
                                    console.log("AI Inspection: " + `${statusx == 0 ? 'Fail' : 'Pass'}`)
                                    await recolectamalos(point)
                                }
                                IAdesition[10]= statusx;model = null
                        break
            case 11: //TA11
                                model = new cvstfjs.ClassificationModel();
                                await model.loadModelAsync('../ml/TA11/model.json');
                                //const image = document.getElementById('canvasClen1');
                                result = await model.executeAsync(canvasGlen1)
                                console.log(model)
                                //console.log(result)
                                //console.log(result[0][0]) //Accede al elemento 0 del array en un objeto
                                pasa = result[0][0]
                                falla = result[0][1]
                                // console.log("Unit pass" , pasa)
                                // console.log("Unit fail", falla)
                                if(pasa >= falla){ //Evalua el valor en la posicion 0 que da la redneuronal
                                    //console.log("Unit pass" , pass)
                                    statusx = "1" 
                                    console.log("AI Inspection: " + `${statusx == 1 ? 'Pass' :'Fail' }`)
                                    porcentajefinal = 1
                                    if(((point == 11) && (statusx == '1'))){ta11statusgood = 'pass'; logubications(11,ta11statusgood)} 
                                    await recolectabuenos(point)
                                }else{
                                    statusx = "0"
                                    if(((point == 11) && (statusx == '0'))){ta11statusbad = 'fail'; logubications(11,ta11statusbad)}
                                    console.log("AI Inspection: " + `${statusx == 0 ? 'Fail' : 'Pass'}`)
                                    await recolectamalos(point)
                                } 
                                IAdesition[11]= statusx;model = null
                                break
            case 12://TA12
                                model = new cvstfjs.ClassificationModel();
                                await model.loadModelAsync('../ml/TA12/model.json');
                                //const image = document.getElementById('canvasClen1');
                                result = await model.executeAsync(canvasMlen1)
                                console.log(model)
                                //console.log(result)
                                //console.log(result[0][0]) //Accede al elemento 0 del array en un objeto
                                pasa = result[0][0]
                                falla = result[0][1]
                                // console.log("Unit pass" , pasa)
                                // console.log("Unit fail", falla)
                                if(pasa >= falla){ //Evalua el valor en la posicion 0 que da la redneuronal
                                    statusx = "1" 
                                    console.log("AI Inspection: " + `${statusx == 1 ? 'Pass' :'Fail' }`)
                                    porcentajefinal = 1
                                    if(((point == 12) && (statusx == '1'))){ta12statusgood = 'pass'; logubications(12,ta12statusgood)} 
                                    await recolectabuenos(point)
                                }else{
                                    statusx = "0"
                                    if(((point == 12) && (statusx == '0'))){ta12statusbad = 'fail'; logubications(12,ta12statusbad)}
                                    console.log("AI Inspection: " + `${statusx == 0 ? 'Fail' : 'Pass'}`)
                                    await recolectamalos(point)
                                }
                                IAdesition[12]= statusx;model = null
                                break
            case 13://TA13
                                model = new cvstfjs.ClassificationModel();
                                await model.loadModelAsync('../ml/TA13/model.json');
                                //const image = document.getElementById('canvasClen1');
                                result = await model.executeAsync(canvasClen2)
                                console.log(model)
                                //console.log(result)
                                //console.log(result[0][0]) //Accede al elemento 0 del array en un objeto
                                pasa = result[0][0]
                                falla = result[0][1]
                                //console.log("Unit pass" , pasa)
                                //console.log("Unit fail", falla)
                                if(pasa >= falla){ //Evalua el valor en la posicion 0 que da la redneuronal
                                    //console.log("Unit pass" , pass)
                                    statusx = "1" 
                                    console.log("AI Inspection: " + `${statusx == 1 ? 'Pass' :'Fail' }`)
                                    porcentajefinal = 1
                                    if(((point == 13) && (statusx == '1'))){ta13statusgood = 'pass'; logubications(13,ta13statusgood)} 
                                    await recolectabuenos(point)
                                    //console.log("Guarde imagen buena")
                                }else{
                                    statusx = "0"
                                    if(((point == 13) && (statusx == '0'))){ta13statusbad = 'fail'; logubications(13,ta13statusbad)}
                                    console.log("AI Inspection: " + `${statusx == 0 ? 'Fail' : 'Pass'}`)
                                    await recolectamalos(point)
                                }
                                IAdesition[13]= statusx;model = null
                                break
            case 14://TA14
                                model = new cvstfjs.ClassificationModel();
                                await model.loadModelAsync('../ml/TA14/model.json');
                                //const image = document.getElementById('canvasClen1');
                                result = await model.executeAsync(canvasClen2)
                                console.log(model)
                                //console.log(result)
                                //console.log(result[0][0]) //Accede al elemento 0 del array en un objeto
                                pasa = result[0][0]
                                falla = result[0][1]
                                console.log("Unit pass" , pasa)
                                console.log("Unit fail", falla)
                                if(pasa >= falla){ //Evalua el valor en la posicion 0 que da la redneuronal
                                    //console.log("Unit pass" , pass)
                                    statusx = "1" 
                                    console.log("AI Inspection: " + `${statusx == 1 ? 'Pass' :'Fail' }`)
                                    porcentajefinal = 1
                                    if(((point == 14) && (statusx == '1'))){ta14statusgood = 'pass'; logubications(14,ta14statusgood)} 
                                    await recolectabuenos(point)
                                }else{
                                    statusx = "0"
                                    if(((point == 14) && (statusx == '0'))){ta14statusbad = 'fail'; logubications(14,ta14statusbad)}
                                    console.log("AI Inspection: " + `${statusx == 0 ? 'Fail' : 'Pass'}`)
                                    await recolectamalos(point)
                                }
                                IAdesition[14]= statusx;model = null
                                break
            case 15://TA15
                                model = new cvstfjs.ClassificationModel();
                                await model.loadModelAsync('../ml/TA15/model.json');
                                //const image = document.getElementById('canvasClen1');
                                result = await model.executeAsync(canvasClen2)
                                console.log(model)
                                //console.log(result)
                                //console.log(result[0][0]) //Accede al elemento 0 del array en un objeto
                                pasa = result[0][0]
                                falla = result[0][1]
                                console.log("Unit pass" , pasa)
                                console.log("Unit fail", falla)
                                if(pasa >= falla){ //Evalua el valor en la posicion 0 que da la redneuronal
                                    //console.log("Unit pass" , pass)
                                    statusx = "1" 
                                    console.log("AI Inspection: " + `${statusx == 1 ? 'Pass' :'Fail' }`)
                                    porcentajefinal = 1
                                    if(((point == 15) && (statusx == '1'))){ta15statusgood = 'pass'; logubications(15,ta15statusgood)} 
                                    await recolectabuenos(point)
                                }else{
                                    statusx = "0"
                                    if(((point == 15) && (statusx == '0'))){ta15statusbad = 'fail'; logubications(15,ta15statusbad)}
                                    console.log("AI Inspection: " + `${statusx == 0 ? 'Fail' : 'Pass'}`)
                                    await recolectamalos(point)
                                }
                                IAdesition[15]= statusx; model = null
                                break
            case 16://TA16
                                model = new cvstfjs.ClassificationModel();
                                await model.loadModelAsync('../ml/TA16/model.json');
                                //const image = document.getElementById('canvasClen1');
                                result = await model.executeAsync(canvasClen2);
                                console.log(model)
                                //console.log(result[0][0]) //Accede al elemento 0 del array en un objeto
                                pasa = result[0][0]
                                falla = result[0][1]
                                console.log("Unit pass" , pasa)
                                console.log("Unit fail", falla)
                                if(pasa >= falla){ //Evalua el valor en la posicion 0 que da la redneuronal
                                    //console.log("Unit pass" , pass)
                                    statusx = "1" 
                                    console.log("AI Inspection: " + `${statusx == 1 ? 'Pass' :'Fail' }`)
                                    porcentajefinal = 1
                                    if(((point == 16) && (statusx == '1'))){ta16statusgood = 'pass'; logubications(16,ta16statusgood)} 
                                    await recolectabuenos(point)
                                }else{
                                    statusx = "0"
                                    if(((point == 16) && (statusx == '0'))){ta16statusbad = 'fail'; logubications(16,ta16statusbad)}
                                    console.log("AI Inspection: " + `${statusx == 0 ? 'Fail' : 'Pass'}`)
                                    await recolectamalos(point)
                                }
                                IAdesition[16]= statusx;model = null
                                break
            case 17://TA17
                               /* model = new cvstfjs.ClassificationModel();
                                await model.loadModelAsync('../ml/TA17/model.json');
                                //const image = document.getElementById('canvasClen1');
                                result = await model.executeAsync(canvasClen2);
                                setTimeout(function fire(){console.log("Entrenamiento cargado"),3000})
                                console.log(result)
                                console.log(model)*/
                                //console.log(result[0][0]) //Accede al elemento 0 del array en un objeto
                                
                                await loadneuralTA17()
                                pasa = result[0][0]
                                falla = result[0][1]
                                console.log("Unit pass" , pasa)
                                console.log("Unit fail", falla)
                                if(pasa >= falla){ //Evalua el valor en la posicion 0 que da la redneuronal
                                    //console.log("Unit pass" , pass)
                                    statusx = "1" 
                                    console.log("AI Inspection: " + `${statusx == 1 ? 'Pass' :'Fail' }`)
                                    porcentajefinal = 1
                                    if(((point == 17) && (statusx == '1'))){ta17statusgood = 'pass'; logubications(17,ta17statusgood)} 
                                    await recolectabuenos(point)
                                }else{
                                    statusx = "0"
                                    if(((point == 17) && (statusx == '0'))){ta17statusbad = 'fail'; logubications(17,ta17statusbad)}
                                    console.log("AI Inspection: " + `${statusx == 0 ? 'Fail' : 'Pass'}`)
                                    await recolectamalos(point)
                                }
                                IAdesition[17]= statusx;model = null
                                break
            case 18://TA18
                                model = new cvstfjs.ClassificationModel();
                                await model.loadModelAsync('../ml/TA18/model.json');
                                //const image = document.getElementById('canvasClen1');
                                result = await model.executeAsync(canvasClen2);
                                console.log(model)
                                //console.log(result[0][0]) //Accede al elemento 0 del array en un objeto
                                pasa = result[0][0]
                                falla = result[0][1]
                                console.log("Unit pass" , pasa)
                                console.log("Unit fail", falla)
                                if(pasa >= falla){ //Evalua el valor en la posicion 0 que da la redneuronal
                                    //console.log("Unit pass" , pass)
                                    statusx = "1" 
                                    console.log("AI Inspection: " + `${statusx == 1 ? 'Pass' :'Fail' }`)
                                    porcentajefinal = 1
                                    if(((point == 18) && (statusx == '1'))){ta18statusgood = 'pass'; logubications(18,ta18statusgood)} 
                                    await recolectabuenos(point)
                                }else{
                                    statusx = "0"
                                    if(((point == 18) && (statusx == '0'))){ta18statusbad = 'fail'; logubications(18,ta18statusbad)}
                                    console.log("AI Inspection: " + `${statusx == 0 ? 'Fail' : 'Pass'}`)
                                    await recolectamalos(point)
                                }
                                IAdesition[18]= statusx;model = null
                                break
            case 19://TA19
                                model = new cvstfjs.ClassificationModel();
                                await model.loadModelAsync('../ml/TA19/model.json');
                                //const image = document.getElementById('canvasClen1');
                                result = await model.executeAsync(canvasGlen2);
                                console.log(model)
                                //console.log(result)
                                //console.log(result[0][0]) //Accede al elemento 0 del array en un objeto
                                pasa = result[0][0]
                                falla = result[0][1]
                                console.log("Unit pass" , pasa)
                                console.log("Unit fail", falla)
                                if(pasa >= falla){ //Evalua el valor en la posicion 0 que da la redneuronal
                                    //console.log("Unit pass" , pass)
                                    statusx = "1" 
                                    console.log("AI Inspection: " + `${statusx == 1 ? 'Pass' :'Fail' }`)
                                    porcentajefinal = 1
                                    if(((point == 19) && (statusx == '1'))){ta19statusgood = 'pass'; logubications(19,ta19statusgood)} 
                                    await recolectabuenos(point)
                                }else{
                                    statusx = "0"
                                    if(((point == 19) && (statusx == '0'))){ta19statusbad = 'fail'; logubications(19,ta19statusbad)}
                                    console.log("AI Inspection: " + `${statusx == 0 ? 'Fail' : 'Pass'}`)
                                    await recolectamalos(point)
                                }
                                IAdesition[19]= statusx;model = null
                                break
            case 20://TA20
                                model = new cvstfjs.ClassificationModel();
                                await model.loadModelAsync('../ml/TA20/model.json');
                                //const image = document.getElementById('canvasClen1');
                                result = await model.executeAsync(canvasGlen2)
                                console.log(model)
                                //console.log(result)
                                //console.log(result[0][0]) //Accede al elemento 0 del array en un objeto
                                pasa = result[0][0]
                                falla = result[0][1]
                                console.log("Unit pass" , pasa)
                                console.log("Unit fail", falla)
                                if(pasa >= falla){ //Evalua el valor en la posicion 0 que da la redneuronal
                                    //console.log("Unit pass" , pass)
                                    statusx = "1" 
                                    console.log("AI Inspection: " + `${statusx == 1 ? 'Pass' :'Fail' }`)
                                    porcentajefinal = 1
                                    if(((point == 20) && (statusx == '1'))){ta20statusgood = 'pass'; logubications(20,ta20statusgood)} 
                                    await recolectabuenos(point)
                                }else{
                                    statusx = "0"
                                    if(((point == 20) && (statusx == '0'))){ta20statusbad = 'fail'; logubications(20,ta20statusbad)}
                                    console.log("AI Inspection: " + `${statusx == 0 ? 'Fail' : 'Pass'}`)
                                    await recolectamalos(point)
                                }
                                IAdesition[20]= statusx;model = null //Limpiar variable que carga el modelo
                                break
            case 21://TA21
                                model = new cvstfjs.ClassificationModel();
                                await model.loadModelAsync('../ml/TA21/model.json');
                                //const image = document.getElementById('canvasClen1');
                                result = await model.executeAsync(canvasflalen3)
                                console.log(model)
                                //console.log(result)
                                //console.log(result[0][0]) //Accede al elemento 0 del array en un objeto
                                pasa = result[0][0]
                                falla = result[0][1]
                                console.log("Unit pass" , pasa)
                                console.log("Unit fail", falla)
                                if(pasa >= falla){ //Evalua el valor en la posicion 0 que da la redneuronal
                                    //console.log("Unit pass" , pass)
                                    statusx = "1" 
                                    console.log("AI Inspection: " + `${statusx == 1 ? 'Pass' :'Fail' }`)
                                    porcentajefinal = 1
                                    if(((point == 21) && (statusx == '1'))){ta21statusgood = 'pass'; logubications(21,ta21statusgood)} 
                                    await recolectabuenos(point)
                                }else{
                                    statusx = "0"
                                    if(((point == 21) && (statusx == '0'))){ta21statusbad = 'fail'; logubications(21,ta21statusbad)}
                                    console.log("AI Inspection: " + `${statusx == 0 ? 'Fail' : 'Pass'}`)
                                    await recolectamalos(point)
                                }
                                IAdesition[21]= statusx;model = null
                                break
            case 22://TA22
                                model = new cvstfjs.ClassificationModel();
                                await model.loadModelAsync('../ml/TA22/model.json');
                                //const image = document.getElementById('canvasClen1');
                                result = await model.executeAsync(canvasgolen3)
                                console.log(model)
                                //console.log(result)
                                //console.log(result[0][0]) //Accede al elemento 0 del array en un objeto
                                pasa = result[0][0]
                                falla = result[0][1]
                                console.log("Unit pass" , pasa)
                                console.log("Unit fail", falla)
                                if(pasa >= falla){ //Evalua el valor en la posicion 0 que da la redneuronal
                                    //console.log("Unit pass" , pass)
                                    statusx = "1" 
                                    console.log("AI Inspection: " + `${statusx == 1 ? 'Pass' :'Fail' }`)
                                    porcentajefinal = 1
                                    if(((point == 22) && (statusx == '1'))){ta22statusgood = 'pass'; logubications(22,ta22statusgood)} 
                                    await recolectabuenos(point)
                                }else{
                                    statusx = "0"
                                    if(((point == 22) && (statusx == '0'))){ta22statusbad = 'fail'; logubications(22,ta22statusbad)}
                                    console.log("AI Inspection: " + `${statusx == 0 ? 'Fail' : 'Pass'}`)
                                    await recolectamalos(point)
                                }
                                IAdesition[22]= statusx;model = null
                                break
                case 23://TA23
                                model = new cvstfjs.ClassificationModel();
                                await model.loadModelAsync('../ml/TA23/model.json');
                                //const image = document.getElementById('canvasClen1');
                                result = await model.executeAsync(canvasgolen3)
                                console.log(model)
                                //console.log(result)
                                //console.log(result[0][0]) //Accede al elemento 0 del array en un objeto
                                pasa = result[0][0]
                                falla = result[0][1]
                                console.log("Unit pass" , pasa)
                                console.log("Unit fail", falla)
                                if(pasa >= falla){ //Evalua el valor en la posicion 0 que da la redneuronal
                                    //console.log("Unit pass" , pass)
                                    statusx = "1" 
                                    console.log("AI Inspection: " + `${statusx == 1 ? 'Pass' :'Fail' }`)
                                    porcentajefinal = 1
                                    if(((point == 23) && (statusx == '1'))){ta23statusgood = 'pass'; logubications(23,ta23statusgood)} 
                                    await recolectabuenos(point)
                                }else{
                                    statusx = "0"
                                    if(((point == 23) && (statusx == '0'))){ta23statusbad = 'fail'; logubications(23,ta23statusbad)}
                                    console.log("AI Inspection: " + `${statusx == 0 ? 'Fail' : 'Pass'}`)
                                    await recolectamalos(point)
                                }
                                IAdesition[23]= statusx;model = null
                                break
                case 24://TA24
                                model = new cvstfjs.ClassificationModel();
                                await model.loadModelAsync('../ml/TA24/model.json');
                                //const image = document.getElementById('canvasClen1');
                                result = await model.executeAsync(canvasgolen3)
                                console.log(model)
                                pasa = result[0][0]
                                falla = result[0][1]
                                console.log("Unit pass" , pasa)
                                console.log("Unit fail", falla)
                                if(pasa >= falla){ //Evalua el valor en la posicion 0 que da la redneuronal
                                    //console.log("Unit pass" , pass)
                                    statusx = "1" 
                                    console.log("AI Inspection: " + `${statusx == 1 ? 'Pass' :'Fail' }`)
                                    porcentajefinal = 1
                                    if(((point == 24) && (statusx == '1'))){ta24statusgood = 'pass'; logubications(24,ta24statusgood)} 
                                    await recolectabuenos(point)
                                }else{
                                    statusx = "0"
                                    if(((point == 24) && (statusx == '0'))){ta24statusbad = 'fail'; logubications(24,ta24statusbad)}
                                    console.log("AI Inspection: " + `${statusx == 0 ? 'Fail' : 'Pass'}`)
                                    await recolectamalos(point)
                                }
                                IAdesition[24]= statusx;model = null
                                break
                case 25://TA25
                                model = new cvstfjs.ClassificationModel();
                                await model.loadModelAsync('../ml/TA25/model.json');
                                //const image = document.getElementById('canvasClen1');
                                result = await model.executeAsync(canvasgolen3)
                                console.log(model)
                                //console.log(result)
                                //console.log(result[0][0]) //Accede al elemento 0 del array en un objeto
                                pasa = result[0][0]
                                falla = result[0][1]
                                console.log("Unit pass" , pasa)
                                console.log("Unit fail", falla)
                                if(pasa >= falla){ //Evalua el valor en la posicion 0 que da la redneuronal
                                    //console.log("Unit pass" , pass)
                                    statusx = "1" 
                                    console.log("AI Inspection: " + `${statusx == 1 ? 'Pass' :'Fail' }`)
                                    porcentajefinal = 1
                                    if(((point == 25) && (statusx == '1'))){ta25statusgood = 'pass'; logubications(25,ta25statusgood)} 
                                    await recolectabuenos(point)
                                }else{
                                    statusx = "0"
                                    if(((point == 25) && (statusx == '0'))){ta25statusbad = 'fail'; logubications(25,ta25statusbad)}
                                    console.log("AI Inspection: " + `${statusx == 0 ? 'Fail' : 'Pass'}`)
                                    await recolectamalos(point)
                                }
                                IAdesition[25]= statusx;model = null
                                break
                case 26://TA26
                                model = new cvstfjs.ClassificationModel();
                                await model.loadModelAsync('../ml/TB4/model.json');
                                //const image = document.getElementById('canvasClen1');
                                result = await model.executeAsync(canvasgolen3)
                                console.log(model)
                                //console.log(result)
                                //console.log(result[0][0]) //Accede al elemento 0 del array en un objeto
                                pasa = result[0][0]
                                falla = result[0][1]
                                console.log("Unit pass" , pasa)
                                console.log("Unit fail", falla)
                                if(pasa >= falla){ //Evalua el valor en la posicion 0 que da la redneuronal
                                    //console.log("Unit pass" , pass)
                                    statusx = "1" 
                                    console.log("AI Inspection: " + `${statusx == 1 ? 'Pass' :'Fail' }`)
                                    porcentajefinal = 1
                                    if(((point == 26) && (statusx == '1'))){ta26statusgood = 'pass'; logubications(26,ta26statusgood)} 
                                    await recolectabuenos(point)
                                }else{
                                    statusx = "0"
                                    if(((point == 26) && (statusx == '0'))){ta26statusbad = 'fail'; logubications(26,ta26statusbad)}
                                    console.log("AI Inspection: " + `${statusx == 0 ? 'Fail' : 'Pass'}`)
                                    await recolectamalos(point)
                                }
                                IAdesition[26]= statusx
                               // model = null
                                break
        }
     resolve('resolved')})
}

//-------------------------------------------------------------- Funcion byPass --------------------------------------------------

async function bypass(posicion){
    return new Promise(async resolve => {
   /* switch(posicion){
        case 1:
            logsave[1] = ('1')
              console.log('posicion 1 en valor 1')
        break
        case 2:
            logsave[2] = ('1')
        break
        case 3:
            logsave[3] = ('1')
        break
        case 4:
            logsave[4] = ('1')
        break
        case 5:
            logsave[5] = ('1')
        break
        case 6:
            logsave[6] = ('1')
        break
        case 7:
            logsave[7] = ('1')
        break
        case 8:
            logsave[8] = ('1')
        break
        case 9:
            logsave[9] = ('1')
        break
        case 10:
            logsave[10] = ('1')
        break
        case 11:
            logsave[11] = ('1')
        break
        case 12:
            logsave[12] = ('1')
        break
        case 13:
            logsave[13] = ('1')
        break
        case 14:
            logsave[14] = ('1')
        break
        case 15:
            logsave[15] = ('1')
        break
        case 16:
            logsave[16] = ('1')
            //console.log('entre a punto 16')
        break
        case 17:
            logsave[17] = ('1')
        break
        case 18:
            logsave[18] = ('1')
        break
        case 19:
            logsave[19] = ('1')
        break
        case 20:
            logsave[20] = ('1')
        break
        case 21:
            logsave[21] = ('1')
            break
        case 22:
            logsave[22] = ('1')
            break
    }*/
    resolve('resolved')
})

}
async function loadneuralTA17(){
    return new Promise(async resolve => {
        model = new cvstfjs.ClassificationModel();
        await model.loadModelAsync('../ml/TA17/model.json')
        //const image = document.getElementById('canvasClen1')
        console.log(model)
        result = await model.executeAsync(canvasClen2)
        setTimeout(function fire(){console.log("Entrenamiento cargado",result,model);resolve('resolved')},3000); 
    })
}
async function loadneuralTA10(){
    return new Promise(async resolve => {
        model = new cvstfjs.ClassificationModel();
        await model.loadModelAsync('../ml/TA10/model.json')
        //const image = document.getElementById('canvasClen1')
        console.log(model)
        result = await model.executeAsync(canvasGlen1);
        setTimeout(function fire(){console.log("Entrenamiento cargado",result,model);resolve('resolved')},3000); 
    })
}

//****************************************************** IA ****************************************
/*
const classifier = knnClassifier.create()
ml()

async function mlinspector(pot) {
    return new Promise(async resolve => { // inicio de promesa 
        let x = 0
        let y = 0
        let w = 0
        let h = 0
        // Cuadrante 1
        if (pot == 1) { x = 81, y = 240, w = 299, h = 118 }
        if (pot == 2) { x = 85, y = 395, w = 330, h = 118 }
        if (pot == 11) { x = 549, y = 512, w = 330, h = 118 }
        if (pot == 12) { x = 629, y = 262, w = 312, h = 118 }
        if (pot == 23) { x = 188, y = 187, w = 52, h = 34 }

        // Cuadrante 2
        if (pot == 3) { x = 90, y = 758, w = 299, h = 118 }
        if (pot == 4) { x = 57, y = 1070, w = 330, h = 118 }
        if (pot == 9) { x = 557, y = 1031, w = 330, h = 118 }
        if (pot == 10) { x = 627, y = 780, w = 330, h = 118 }
        if (pot == 24) { x = 117, y = 187, w = 52, h = 34 }

        //cuadrante 3
        if (pot == 5) { x = 95, y = 1307, w = 299, h = 118 }
        if (pot == 6) { x = 67, y = 1622, w = 322, h = 115 } //hacer el 7
        if (pot == 7) { x = 562, y = 1580, w = 562, h = 1580 }
        if (pot == 8) { x = 629, y = 1329, w = 330, h = 118 }
        if (pot == 25) { x = 119, y = 1248, w = 52, h = 34 }

        //Cuadrante 4
        if (pot == 17) { x = 99, y = 312, w = 378, h = 149 }
        if (pot == 18) { x = 50, y = 105, w = 378, h = 149 }
        if (pot == 27) { x = 822, y = 153, w = 99, h = 298 } //TG

        //Cuadrante 5
        if (pot == 15) { x = 100, y = 961, w = 378, h = 149 }
        if (pot == 16) { x = 55, y = 751, w = 378, h = 149 }
        if (pot == 19) { x = 692, y = 771, w = 418, h = 149 }
        if (pot == 20) { x = 893, y = 766, w = 418, h = 149 }
        if (pot == 26) { x = 594, y = 1031, w = 52, h = 66 }

        //Cuadrante 6
        if (pot == 13) { x = 98, y = 1715, w = 378, h = 149 }
        if (pot == 14) { x = 61, y = 1504, w = 378, h = 149 }

        //Cuadrante 7
        if (pot == 21) { x = 549, y = 221, w = 61, h = 555 }
        if (pot == 22) { x = 619, y = 194, w = 364, h = 634 }


        switch (pot) {
            case 1:
                //TA1
                contextcanvasClen1.drawImage(fullimage, x, y, w, h, 0, 0, contextcanvasClen1.canvas.width, contextcanvasClen1.canvas.height)
                // contextcanvande.drawImage(fullimage,355,317,538,192,0,0,contextcanvande.canvas.width,contextcanvande.canvas.height);   
                await call("TA1-75", 75)
                await predict(canvasClen1)
                IAdesition[1] = statusx
                break;
            case 2:
                //TA2
                contextcanvasGlen1.drawImage(fullimage, x, y, w, h, 0, 0, contextcanvasGlen1.canvas.width, contextcanvasGlen1.canvas.height);
                await call("TA1-75", 75)
                await predict(canvasGlen1)
                break;
            case 3:
                //TA3
                contextcanvasClen1.drawImage(fullimage, x, y, w, h, 0, 0, contextcanvasClen1.canvas.width, contextcanvasClen1.canvas.height);
                await call("TA3-75", 75)
                await predict(canvasClen1)
                IAdesition[3] = statusx
                break;
            case 4:
                //TA4
                contextcanvasGlen1.drawImage(fullimage, x, y, w, h, 0, 0, contextcanvasGlen1.canvas.width, contextcanvasGlen1.canvas.height);
                await call("TA4-75", 75)
                await predict(canvasGlen1)
                IAdesition[4] = statusx
                break;
            case 5:
                //TA5
                contextcanvasClen1.drawImage(fullimage, x, y, w, h, 0, 0, contextcanvasClen1.canvas.width, contextcanvasClen1.canvas.height)
                // contextcanvande.drawImage(fullimage,355,317,538,192,0,0,contextcanvande.canvas.width,contextcanvande.canvas.height);   
                await call("TA5-75", 75)
                await predict(canvasClen1)
                IAdesition[5] = statusx
                break;
            case 6:
                //TA6
                contextcanvasTA6len1.drawImage(fullimage, x, y, w, h, 0, 0, contextcanvasTA6len1.canvas.width, contextcanvasTA6len1.canvas.height);
                await call("TA6-75", 75)
                await predict(canvasTA6len1)
                IAdesition[6] = statusx
                break;
            case 7:
                // TA7
                contextcanvasGlen1.drawImage(fullimage, x, y, w, h, 0, 0, contextcanvasGlen1.canvas.width, contextcanvasGlen1.canvas.height);
                await call("TA7-75", 75)
                await predict(canvasGlen1)
                IAdesition[7] = statusx
                break;
            case 8:
                // TA8
                contextcanvasGlen1.drawImage(fullimage, x, y, w, h, 0, 0, contextcanvasGlen1.canvas.width, contextcanvasGlen1.canvas.height);
                await call("TA8-75", 75)
                await predict(canvasGlen1)
                IAdesition[8] = statusx
                break;
            case 9:
                //TA9
                contextcanvasGlen1.drawImage(fullimage, x, y, w, h, 0, 0, contextcanvasGlen1.canvas.width, contextcanvasGlen1.canvas.height);
                await call("TA9", 118)
                await predict(canvasGlen1)
                IAdesition[9] = statusx
                break;
            //TA10
            case 10:
                contextcanvasGlen1.drawImage(fullimage, x, y, w, h, 0, 0, contextcanvasGlen1.canvas.width, contextcanvasGlen1.canvas.height);
                await call("TA3-75", 75)
                await predict(canvasGlen1)
                IAdesition[10] = statusx
                break;
            //TA11
            case 11:
                contextcanvasGlen1.drawImage(fullimage, x, y, w, h, 0, 0, contextcanvasGlen1.canvas.width, contextcanvasGlen1.canvas.height);
                await call("Canvande", 26)
                await predict(canvasGlen1)
                break;
            case 12:
                contextcanvasMlen1.drawImage(fullimage, x, y, w, h, 0, 0, contextcanvasMlen1.canvas.width, contextcanvasMlen1.canvas.height);
                await call("TA12-75", 75)
                await predict(canvasMlen1)
                IAdesition[12] = statusx
                break;
            case 13:
                contextcanvasClen2.drawImage(fullimage, x, y, w, h, 0, 0, contextcanvasClen2.canvas.width, contextcanvasClen2.canvas.height)
                //contextcanvande.drawImage(fullimage,355,317,538,192,0,0,contextcanvande.canvas.width,contextcanvande.canvas.height);   
                await call("TA13-75", 75)
                await predict(canvasClen2)
                IAdesition[13] = statusx
                break;
            case 14:
                contextcanvasClen2.drawImage(fullimage, x, y, w, h, 0, 0, contextcanvasClen2.canvas.width, contextcanvasClen2.canvas.height)
                //contextcanvande.drawImage(fullimage,355,317,538,192,0,0,contextcanvande.canvas.width,contextcanvande.canvas.height);   
                await call("TA14", 22)
                await predict(canvasClen2)
                IAdesition[14] = statusx
                break;
            case 15:
                contextcanvasClen2.drawImage(fullimage, x, y, w, h, 0, 0, contextcanvasClen2.canvas.width, contextcanvasClen2.canvas.height);
                await call("TA15", 27)
                await predict(canvasClen2)
                IAdesition[15] = statusx
                break;
            case 16:
                contextcanvasClen2.drawImage(fullimage, x, y, w, h, 0, 0, contextcanvasClen2.canvas.width, contextcanvasClen2.canvas.height);
                await call("TA16", 30)
                await predict(canvasClen2)
                IAdesition[16] = statusx
                break;
            case 17:
                contextcanvasClen2.drawImage(fullimage, x, y, w, h, 0, 0, contextcanvasClen2.canvas.width, contextcanvasClen2.canvas.height)
                // contextcanvande.drawImage(fullimage,355,317,538,192,0,0,contextcanvande.canvas.width,contextcanvande.canvas.height);   
                await call("TA17-75", 75)
                await predict(canvasClen2)
                IAdesition[17] = statusx
                break;
            case 18:
                contextcanvasClen2.drawImage(fullimage, x, y, w, h, 0, 0, contextcanvasClen2.canvas.width, contextcanvasClen2.canvas.height)
                //contextcanvande.drawImage(fullimage,355,317,538,192,0,0,contextcanvande.canvas.width,contextcanvande.canvas.height);   
                await call("TA18-102", 102)
                await predict(canvasClen2)
                IAdesition[18] = statusx
                break;
            case 19:
                contextcanvasGlen2.drawImage(fullimage, x, y, w, h, 0, 0, contextcanvasGlen2.canvas.width, contextcanvasGlen2.canvas.height);
                await call("TA19", 48)
                await predict(canvasGlen2)
                IAdesition[19] = statusx
                break;
            case 20:
                contextcanvasGlen2.drawImage(fullimage, x, y, w, h, 0, 0, contextcanvasGlen2.canvas.width, contextcanvasGlen2.canvas.height);
                await call("canvica")
                await predict(canvasGlen2)
                break;
            case 21:
                contextcanvasflalen3.drawImage(fullimage, x, y, w, h, 0, 0, contextcanvasflalen3.canvas.width, contextcanvasflalen3.canvas.height);
                await call("TA21-50", 50)
                await predict(canvasflalen3)
                IAdesition[21] = statusx
                break;
            case 22:
                contextcanvasgolen3.drawImage(fullimage, x, y, w, h, 0, 0, contextcanvasgolen3.canvas.width, contextcanvasgolen3.canvas.height);
                await call("TA22-40", 40)
                await predict(canvasgolen3)
                IAdesition[22] = statusx
                break;
            //TG
            case 23:
                contextcanvasTGlen2.drawImage(fullimage, x, y, w, h, 0, 0, contextcanvasTGlen2.canvas.width, contextcanvasTGlen2.canvas.height);
                await call("TA1-75", 75)
                await predict(canvasTGlen2)
                IAdesition[23] = statusx
                break;
            //TB1
            case 24:
                contextcanvasTB.drawImage(fullimage, x, y, w, h, 0, 0, contextcanvasTB.canvas.width, contextcanvasTB.canvas.height);
                await call("TA3-75", 75)
                await predict(canvasTB)
                IAdesition[24] = statusx
                break;
            //TB2
            case 25:
                contextcanvasTB.drawImage(fullimage, x, y, w, h, 0, 0, contextcanvasTB.canvas.width, contextcanvasTB.canvas.height);
                await call("TA7-75", 75)
                await predict(canvasTB)
                IAdesition[25] = statusx
                break;
            //TB3
            case 26:
                contextcanvasTB.drawImage(fullimage, x, y, w, h, 0, 0, contextcanvasTB.canvas.width, contextcanvasTB.canvas.height);
                await call("canvica2")
                await predict(canvasTB)
                break;
            //TB4
            case 27:
                contextcanvasTB4.drawImage(fullimage, x, y, w, h, 0, 0, contextcanvasTB4.canvas.width, contextcanvasTB4.canvas.height);
                await call("TA18-102", 102)
                await predict(canvasTB4)
                IAdesition[27] = statusx
                break;ft
            /*case 27: 
    
            break;
            default:
        }
        resolve('resolved')
    })
}
async function ml() {
    // Load the model.
    net = await mobilenet.load(); // red neuronal echa, ejemplo, ya esta entrenada  
    console.log('Neural network load success...');
}

function load(file, callback) {  // funcion que se encarga de llamar Json con el nuevo entrenamiento 
    //Can be change to other source	
    var rawFile = new XMLHttpRequest();
    rawFile.overrideMimeType("application/json");
    rawFile.open("GET", file, true);
    rawFile.onreadystatechange = function () {
        if (rawFile.readyState === 4 && rawFile.status == "200") {
            callback(rawFile.responseText);
        }
    }
    rawFile.send(null)
}

function call(nuca, vari) { // vari representa nuero de muestras de la red neuronal 
    return new Promise(async resolve => {
        let data;
        load("/ml/neuralnetworks/" + nuca + ".json", function (text) { //C:/Users/juan_moreno/myapp/public/Nokia Vision
            data = JSON.parse(text);
            //*****************************COnvertir a tensor
            //Set classifier
            Object.keys(data).forEach((key) => {
                data[key] = tf.tensor2d(data[key], [vari, 1024]); //Shape es [# imagenes,dimension pix] ,[19,1024]
            });
            //Set classifier
            //console.log(data);
            classifier.setClassifierDataset(data)
        }) // fin de load 
        setTimeout(function fire() { resolve('resolved') }, 500);
    }) // fin de promesa 
}

async function predict(wonka) { // wonka es variable
    return new Promise(async resolve => {
        // Get the activation from mobilenet from the webcam.
        const activation = net.infer(wonka, 'conv_preds');
        // Get the most likely class and confidences from the classifier module.
        const result = await classifier.predictClass(activation); // Clasifica, decide depende de la imagen lo que le va a poner 
        const classes = ['Pass', 'Fail'];
        if (classes[result.label] == "Pass") {
            statusx = "1"
            console.log("AI Inspection: " + `${statusx == 1 ? 'Pass' : 'Fail'}`)
        } else {
            statusx = "0"
            console.log("AI Inspection: " + `${statusx == 0 ? 'Fail' : 'Pass'}`)
        }
        //console.log(classes[result.label])
        // console.log(result.confidences[result.label]*100)
        resolve('resolved')
    });//Cierra Promise						
}//if logic end */